
#include <utils/draw2d/src/draw2d_priv.h>


#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <iconv.h>

#define COLOR_00    (0)    /* background color */
#define COLOR_01    (1)    /* border color     */
#define COLOR_02    (2)    /* font color       */


#define CHINESE_UTF8_BYTES 3    //UTF8编码中文字符占用三个字节



uint16_t gDraw2D_fontChinese_color_bg = DRAW2D_TRANSPARENT_COLOR;
uint16_t gDraw2D_fontChinese_color_border = ((uint16_t)(RGB888_TO_RGB565(16,16,16)))    /* border color     */;
uint16_t gDraw2D_fontChinese_color_text = (0xE71C)    /* font color       */;


int32_t Draw2D_getChineseFontProperty(Draw2D_FontPrm *pPrm, Draw2D_FontProperty *pProp)
{

    if(pProp==NULL)
        return VX_FAILURE;

    if(pPrm!=NULL)
    {
        switch(pPrm->fontIdx)
        {
            case 24:

                Draw2D_getFontPropertyChina24x24(pProp); /* default */   

                break;
            default:

                Draw2D_getFontPropertyChina24x24(pProp); /* default */

                break;
        }
    }

    return VX_SUCCESS;
}



uint8_t * Draw2D_getChineseFontCharAddr(Draw2D_FontProperty *font, char* c)
{
    if (font == NULL)
        return 0;
    uint16_t i = 0;

    for (i = 0; i < font->num;i++)      
    {
        if(0 == strcmp(font->addrChinese[i].Index , c))
        {//匹配字符串，如果匹配，则退出
            break;
        };
    }
    return ((uint8_t *)font->addrChinese + i * sizeof(typFNT_GB24));
}

uint16_t Draw2D_getChineseFontColor(uint16_t key)
{
    uint16_t color = gDraw2D_fontChinese_color_bg;

    if(key==COLOR_01)
        color = gDraw2D_fontChinese_color_border;
    else
    if(key==COLOR_02)
        color = gDraw2D_fontChinese_color_text;

    return color;
}

int32_t Draw2D_drawChineseString_rot(Draw2D_Handle pCtx,
                        uint32_t startX,
                        uint32_t startY,
                        char *str,
                        Draw2D_FontPrm *pPrm,
                        uint32_t rotate)
{
    int32_t status = VX_SUCCESS;
    Draw2D_Obj *pObj = (Draw2D_Obj *)pCtx;
    uint32_t len, width, height, h, i, w, px, py;
    uint8_t *fontAddr;
    uint16_t  color;
    Draw2D_FontProperty font;

    uint8_t byteLen;

    char strTmp[4];
    if(pObj==NULL || str==NULL)
        return VX_FAILURE;

    Draw2D_getChineseFontProperty(pPrm, &font); //获取需要绘制的字体的属性

    len = strlen(str) / CHINESE_UTF8_BYTES; //汉字字符，占用两个字节，UTF-8 编码需要除以3

    width = font.width * len;   //计算整体宽度
    height = font.height;

    if(startX >= pObj->bufInfo.bufWidth)    //检查是否超出整个显示的边界
        return 0;

    if(startY >= pObj->bufInfo.bufHeight)   //检查是否超出整个显示的边界
        return 0;


    if((startX + width)> pObj->bufInfo.bufWidth)
    {
        width = pObj->bufInfo.bufWidth - startX;
    }

    if((startY + height)> pObj->bufInfo.bufHeight)
    {
        height = pObj->bufInfo.bufHeight - startY;
    }


    for (i = 0; i < len; i++)   //根据中文字符创长度，决定循环次数
    {
        memset(strTmp, 0, sizeof(strTmp));                            //将缓冲数组清空
        memcpy(strTmp, str + CHINESE_UTF8_BYTES * i, CHINESE_UTF8_BYTES); //将单个的汉字字符复制出来

        fontAddr = Draw2D_getChineseFontCharAddr(&font, strTmp);    //获取当前字符在汉字表内的相对地址
        fontAddr += sizeof(font.addrChinese->Index);                //将汉字本身的占用字节数地址进行偏移

        px = startX + i * font.width;
        py = startY;

        /* draw font char */
        for (h = 0; h < height; h++)
        {
            for (w = 0; w < (font.width / 8); w++) //每个点被压缩成字节的一位
            {
                for (byteLen = 0; byteLen < 8; byteLen++) //按每一位的值，需要
                {
                    if (((*(fontAddr + w + (font.width / 8)*h)) << byteLen) & 0x80) //检查每一位是否为有效
                    {
                        color = gDraw2D_fontChinese_color_text; //设置当前像素点的颜色属性
                    }
                    else
                    {
                        color = gDraw2D_fontChinese_color_bg;
                    }
                    Draw2D_drawPixel(
                        pCtx,
                        px + 8 * w + byteLen,
                        py + h,
                        color,
                        font.colorFormat);
                }
            }
        }
    }
    return status;
}

int32_t Draw2D_drawChineseString(Draw2D_Handle pCtx,
                        uint32_t startX,
                        uint32_t startY,
                        char *str,
                        Draw2D_FontPrm *pPrm)
{
    return Draw2D_drawChineseString_rot(pCtx,
                                        startX,
                                        startY,
                                        str,
                                        pPrm,
                                        0);
}

int32_t Draw2D_clearChineseString(Draw2D_Handle pCtx,
                                  uint32_t startX,
                                  uint32_t startY,
                                  uint32_t length,
                                  uint32_t width,
                                  uint32_t height,
                                  uint32_t color)
{
    int32_t status = VX_SUCCESS;
    Draw2D_RegionPrm regionPrm;
    Draw2D_Obj *pObj = (Draw2D_Obj *)pCtx;

    if(pObj==NULL)
        return VX_FAILURE;

    regionPrm.startX = startX;
    regionPrm.startY = startY;
    regionPrm.width  = width*length;
    regionPrm.height = height;
    regionPrm.color  = color;
    regionPrm.colorFormat  = pObj->bufInfo.transperentColorFormat;

    status =  Draw2D_fillRegion(pCtx, &regionPrm);

    return status;
}

void Draw2D_setChineseFontColor(uint16_t colorText, uint16_t colorBorder, uint16_t colorBg )
{
    gDraw2D_fontChinese_color_text = colorText;
    gDraw2D_fontChinese_color_border = colorBorder;
    gDraw2D_fontChinese_color_bg = colorBg;
}

void Draw2D_resetChineseFontColor(void)
{
    Draw2D_setChineseFontColor(
        (0xE71C),    /* font color       */
        ((uint16_t)(RGB888_TO_RGB565(16,16,16))),    /* border color     */
        DRAW2D_TRANSPARENT_COLOR
            );
}

