/******************************************************************************
Copyright (c) [2012 - 2017] Texas Instruments Incorporated

All rights reserved not granted herein.

Limited License.

 Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 license under copyrights and patents it now or hereafter owns or controls to
 make,  have made, use, import, offer to sell and sell ("Utilize") this software
 subject to the terms herein.  With respect to the foregoing patent license,
 such license is granted  solely to the extent that any such patent is necessary
 to Utilize the software alone.  The patent license shall not apply to any
 combinations which include this software, other than combinations with devices
 manufactured by or for TI ("TI Devices").  No hardware patent is licensed
 hereunder.

 Redistributions must preserve existing copyright notices and reproduce this
 license (including the above copyright notice and the disclaimer and
 (if applicable) source code license limitations below) in the documentation
 and/or other materials provided with the distribution

 Redistribution and use in binary form, without modification, are permitted
 provided that the following conditions are met:

 * No reverse engineering, decompilation, or disassembly of this software
   is permitted with respect to any software provided in binary form.

 * Any redistribution and use are licensed by TI for use only with TI Devices.

 * Nothing shall obligate TI to provide you with source code for the software
   licensed and provided to you in object code.

 If software source code is provided to you, modification and redistribution of
 the source code are permitted provided that the following conditions are met:

 * Any redistribution and use of the source code, including any resulting
   derivative works, are licensed by TI for use only with TI Devices.

 * Any redistribution and use of any object code compiled from the source code
   and any resulting derivative works, are licensed by TI for use only with TI
   Devices.

 Neither the name of Texas Instruments Incorporated nor the names of its
 suppliers may be used to endorse or promote products derived from this software
 without specific prior written permission.

 DISCLAIMER.

 THIS SOFTWARE IS PROVIDED BY TI AND TI�S LICENSORS "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL TI AND TI�S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/

/**
 *******************************************************************************
 *
 * \defgroup DRAW_2D_API Font and 2D Drawing API
 *
 * \brief  This module has the interface for drawing fonts and 2D primitives
 *         like lines
 *
 *         NOTE: This is limited demo API and not a comprehensive 2D drawing
 *               library
 *
 * @{
 *
 *******************************************************************************
 */

/**
 *******************************************************************************
 *
 * \file draw2d.h
 *
 * \brief Font and 2D Drawing API
 *
 * \version 0.0 (Oct 2013) : [KC] First version
 *
 *******************************************************************************
 */

#ifndef _DRAW_2D_PRIV_H_
#define _DRAW_2D_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 *  INCLUDE FILES
 *******************************************************************************
 */
#include <utils/draw2d/include/draw2d.h>

/*******************************************************************************
 *  Defines
 *******************************************************************************
 */

typedef struct {

    Draw2D_BufInfo bufInfo;

} Draw2D_Obj;


int32_t Draw2D_getFontProperty00(Draw2D_FontProperty *pProp);
int32_t Draw2D_getFontProperty01(Draw2D_FontProperty *pProp);
int32_t Draw2D_getFontProperty02(Draw2D_FontProperty *pProp);
int32_t Draw2D_getFontProperty03(Draw2D_FontProperty *pProp);
int32_t Draw2D_getFontProperty04(Draw2D_FontProperty *pProp);
int32_t Draw2D_getFontProperty10(Draw2D_FontProperty *pProp);
int32_t Draw2D_getFontProperty11(Draw2D_FontProperty *pProp);
int32_t Draw2D_getFontProperty12(Draw2D_FontProperty *pProp);
int32_t Draw2D_getFontProperty13(Draw2D_FontProperty *pProp);

int32_t Draw2D_getFontPropertyChina24x24(Draw2D_FontProperty *pProp);

int32_t Draw2D_getBmpProperty00(Draw2D_BmpProperty *pProp);
int32_t Draw2D_getBmpProperty01(Draw2D_BmpProperty *pProp);
int32_t Draw2D_getBmpProperty02(Draw2D_BmpProperty *pProp);
int32_t Draw2D_getBmpProperty03(Draw2D_BmpProperty *pProp);
int32_t Draw2D_getBmpProperty04(Draw2D_BmpProperty *pProp);
int32_t Draw2D_getBmpProperty05(Draw2D_BmpProperty *pProp);


//assets

int32_t Draw2D_gImage_air_icon_setting_normal(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_air_icon_setting_normalBackup(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_air_icon_setting_normalBackup2(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_air_icon_setting_normalBackup3(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_air_icon_setting_normalBackup4(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_air_icon_setting_normalBackup5(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_air_icon_setting_normalBackup6(Draw2D_BmpProperty *pProp);

int32_t Draw2D_gImage_icons_general_close_enableds(Draw2D_BmpProperty *pProp);


//alert


int32_t Draw2D_gImage_icon_bcdlca_alert1(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_bcdlca_alert2(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_cameraerror_alert(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_dow_alert1(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_dow_alert2(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_rtca_alert1(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_rtca_alert2(Draw2D_BmpProperty *pProp);

//screen
int32_t Draw2D_gImage_add(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_bg_push(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_left_nomal(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_left_select(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_light(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_right_nomal(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_right_select(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_setting1(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_plus(Draw2D_BmpProperty *pProp);

//setting
int32_t Draw2D_gImage_bg248x66(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_cameraheat_active(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_cameraheat_normal(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_cameraheat_select(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_rainfog_active(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_rainfog_normal(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_rainfog_select(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_screenheat_active(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_screenheat_normal(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_icon_screenheat_select(Draw2D_BmpProperty *pProp);

//    ./cat 
int32_t Draw2D_gImage_bg_left(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_bg_right(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_date(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_warning(Draw2D_BmpProperty *pProp);

// UI
int32_t Draw2D_gImage_gray_80x80(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_gray_100x100(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_gray_160x120(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_gray_480x72(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_gray_540x720(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_white_80x80(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_white_100x100(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_white_156x64(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_white_160x120(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_white_236x64(Draw2D_BmpProperty *pProp);
int32_t Draw2D_gImage_white_435x120(Draw2D_BmpProperty *pProp);















int32_t Draw2D_getBmpProperty_DofColourMap(Draw2D_BmpProperty *pProp);
int32_t Draw2D_getBmpProperty_SdeColourMap(Draw2D_BmpProperty *pProp);

uint8_t * Draw2D_getFontCharAddr(Draw2D_FontProperty *font, char c);

void Draw2D_drawCharYuv420SP(Draw2D_Handle pCtx,
                            uint32_t px,
                            uint32_t py,
                            char value,
                            Draw2D_FontProperty *pProp);

uint32_t Draw2D_rgb565ToYuv444(uint16_t rgb565);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif  /* _DRAW_2D_H_ */

/* @} */

/* Nothing beyond this point */
