from tiovx import *
code = KernelExportCode(Module.DEHAZE_FLOAT, Core.C66, "VISION_APPS_PATH")
code.setCoreDirectory("c66")
kernel = Kernel("dehaze_RGB_channels_extract_float")
kernel.setParameter(Type.IMAGE, Direction.INPUT, ParamState.REQUIRED, "IN", ['VX_DF_IMAGE_RGB'])
kernel.setParameter(Type.IMAGE, Direction.OUTPUT, ParamState.REQUIRED, "CHANNELR", ['VX_DF_IMAGE_U8'])
kernel.setParameter(Type.IMAGE, Direction.OUTPUT, ParamState.REQUIRED, "CHANNELG", ['VX_DF_IMAGE_U8'])
kernel.setParameter(Type.IMAGE, Direction.OUTPUT, ParamState.REQUIRED, "CHANNELB", ['VX_DF_IMAGE_U8'])

kernel.setParameter(Type.MATRIX, Direction.OUTPUT, ParamState.REQUIRED, "MATRIX_GRAY", ['VX_TYPE_FLOAT32'])

#设置kernel运行的目标核
kernel.setTarget(Target.DSP1)
kernel.setTarget(Target.DSP2)
code.export(kernel)
