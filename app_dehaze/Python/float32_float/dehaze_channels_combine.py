from tiovx import *
code = KernelExportCode(Module.DEHAZE_FLOAT, Core.C66, "VISION_APPS_PATH")
code.setCoreDirectory("c66")
kernel = Kernel("dehaze_channels_combine_float")

kernel.setParameter(Type.IMAGE, Direction.INPUT, ParamState.REQUIRED, "CHANNELR", ['VX_DF_IMAGE_U8'])
kernel.setParameter(Type.IMAGE, Direction.INPUT, ParamState.REQUIRED, "CHANNELG", ['VX_DF_IMAGE_U8'])
kernel.setParameter(Type.IMAGE, Direction.INPUT, ParamState.REQUIRED, "CHANNELB", ['VX_DF_IMAGE_U8'])
kernel.setParameter(Type.MATRIX, Direction.INPUT, ParamState.REQUIRED, "TRANS_GUIDE", ['VX_TYPE_FLOAT32'])
#大气光值作为输入
kernel.setParameter(Type.FLOAT32, Direction.INPUT, ParamState.REQUIRED, "Atom")
kernel.setParameter(Type.IMAGE, Direction.OUTPUT, ParamState.REQUIRED, "out", ['VX_DF_IMAGE_RGB'])



#设置kernel运行的目标核
kernel.setTarget(Target.DSP1)
kernel.setTarget(Target.DSP2)
code.export(kernel)
