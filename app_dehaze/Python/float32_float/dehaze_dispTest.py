from tiovx import *
code = KernelExportCode(Module.DEHAZE_FLOAT, Core.C66, "VISION_APPS_PATH")
code.setCoreDirectory("c66")
kernel = Kernel("dehaze_dispTest_float")
kernel.setParameter(Type.MATRIX, Direction.INPUT, ParamState.REQUIRED, "MATRIX_TRANS", ['VX_TYPE_FLOAT32'])
kernel.setParameter(Type.MATRIX, Direction.INPUT, ParamState.REQUIRED, "MATRIX_GRAY", ['VX_TYPE_FLOAT32'])
kernel.setParameter(Type.IMAGE, Direction.OUTPUT, ParamState.REQUIRED, "TRANS", ['VX_DF_IMAGE_U8'])
kernel.setParameter(Type.IMAGE, Direction.OUTPUT, ParamState.REQUIRED, "GRAY", ['VX_DF_IMAGE_U8'])

#设置kernel运行的目标核
kernel.setTarget(Target.DSP1)
kernel.setTarget(Target.DSP2)
code.export(kernel)
