from tiovx import *
code = KernelExportCode(Module.DEHAZE_FLOAT, Core.C66, "VISION_APPS_PATH")
code.setCoreDirectory("c66")
kernel = Kernel("dehaze_get_atmo_float")
kernel.setParameter(Type.IMAGE, Direction.INPUT, ParamState.REQUIRED, "CHANNELR", ['VX_DF_IMAGE_U8'])
kernel.setParameter(Type.IMAGE, Direction.INPUT, ParamState.REQUIRED, "CHANNELG", ['VX_DF_IMAGE_U8'])
kernel.setParameter(Type.IMAGE, Direction.INPUT, ParamState.REQUIRED, "CHANNELB", ['VX_DF_IMAGE_U8'])

kernel.setParameter(Type.USER_DATA_OBJECT, Direction.INPUT, ParamState.REQUIRED, "CONFIGURATION")
kernel.setParameter(Type.FLOAT32, Direction.OUTPUT, ParamState.REQUIRED, "Atom")

#设置kernel运行的目标核
kernel.setTarget(Target.DSP1)
kernel.setTarget(Target.DSP2)
code.export(kernel)
