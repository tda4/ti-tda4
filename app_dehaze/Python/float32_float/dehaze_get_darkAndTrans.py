#获取暗通道和透射图
from tiovx import *
code = KernelExportCode(Module.DEHAZE_FLOAT, Core.C66, "VISION_APPS_PATH")
code.setCoreDirectory("c66")
kernel = Kernel("dehaze_darkAndTrans_float")

#输入的3路分离通道图像
kernel.setParameter(Type.IMAGE, Direction.INPUT, ParamState.REQUIRED, "CHANNELR", ['VX_DF_IMAGE_U8'])
kernel.setParameter(Type.IMAGE, Direction.INPUT, ParamState.REQUIRED, "CHANNELG", ['VX_DF_IMAGE_U8'])
kernel.setParameter(Type.IMAGE, Direction.INPUT, ParamState.REQUIRED, "CHANNELB", ['VX_DF_IMAGE_U8'])

#大气光值作为输入
kernel.setParameter(Type.FLOAT32, Direction.INPUT, ParamState.REQUIRED, "Atom")
#用于腐蚀图像的核
kernel.setParameter(Type.MATRIX, Direction.INPUT, ParamState.REQUIRED, "MASK", ['VX_TYPE_UINT8'])
#传入w,除雾程度的参数
kernel.setParameter(Type.USER_DATA_OBJECT, Direction.INPUT, ParamState.REQUIRED, "CONFIGURATION")
#输出透射图
kernel.setParameter(Type.MATRIX, Direction.OUTPUT, ParamState.REQUIRED, "MATRIX_TRANS", ['VX_TYPE_FLOAT32'])

#设置kernel运行的目标核
kernel.setTarget(Target.DSP1)
kernel.setTarget(Target.DSP2)
code.export(kernel)
