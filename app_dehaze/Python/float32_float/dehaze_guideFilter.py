from tiovx import *
code = KernelExportCode(Module.DEHAZE_FLOAT, Core.C66, "VISION_APPS_PATH")
code.setCoreDirectory("c66")
kernel = Kernel("dehaze_get_guideFilter_float")
kernel.setParameter(Type.MATRIX, Direction.INPUT, ParamState.REQUIRED, "MATRIX_TRANS", ['VX_TYPE_FLOAT32'])
kernel.setParameter(Type.MATRIX, Direction.INPUT, ParamState.REQUIRED, "MATRIX_GRAY", ['VX_TYPE_FLOAT32'])
kernel.setParameter(Type.USER_DATA_OBJECT, Direction.INPUT, ParamState.REQUIRED, "CONFIGURATION")
kernel.setParameter(Type.MATRIX, Direction.OUTPUT, ParamState.REQUIRED, "TRANS_GUIDE", ['VX_TYPE_FLOAT32'])


#设置kernel运行的目标核
kernel.setTarget(Target.DSP1)
kernel.setTarget(Target.DSP2)
code.export(kernel)
