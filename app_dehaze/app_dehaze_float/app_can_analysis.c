
#include "app_can_analysis.h"

vx_status app_can_analysis_init(CANObj *canObj)
{
    vx_status status = VX_SUCCESS;

    memset(canObj, 0, sizeof(CANObj));

    //缺少初始化步骤，需要补充！！！

    return status;
}

vx_status app_can_analysis(char *src, void *can)
{
    vx_status status = VX_SUCCESS;

    CANObj *canObj = (CANObj *)can;
    //指针检测
    if ((src == NULL) || (canObj == NULL))
    {
        //指针为空，直接跳出
        status = VX_FAILURE;
        // return status;
    }

    memcpy(&canObj->cmsUdpCanObj.canID, src, sizeof(canObj->cmsUdpCanObj.canID)); //拷贝CAN ID
    canObj->cmsUdpCanObj.length = *(src + sizeof(canObj->cmsUdpCanObj.canID));    //获取长度信息
    if (canObj->cmsUdpCanObj.length > 0)
    {
        memcpy(canObj->cmsUdpCanObj.buff, (src + 1 + sizeof(canObj->cmsUdpCanObj.canID)), canObj->cmsUdpCanObj.length); //跳过CANID和长度占位
    }
    else
    { //如果数据长度为0，则直接返回报错
        status = VX_FAILURE;
        return status;
    }

    //解析数据信息，等待补充
    switch (canObj->cmsUdpCanObj.canID)
    {
    case CAN_VehiclePwrSts_MESSAGE_ID:
        canObj->cmsCanSignalObj.SysPwrMdV = 0;
        canObj->cmsCanSignalObj.SysPwrMd = 0;

        break;

    default:
        break;
    }

    return status;
}
