
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CAN_ANALYSIS_H__
#define __CAN_ANALYSIS_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include "app_common.h"

/************************************************************************/
/*结构体定义段*/
/************************************************************************/
/**
 * @brief 流媒体后视镜相关参数结构
 * @param KDisplayKeepTimer           熄火后图像在监视器维持时间
 * @param KKeepAliveTimer             CMS系统在整车电源模式OFF下保活总时间
 * @param KDefalutViewX_Left          左监视器出厂静态视野X坐标
 * @param KDefalutViewY_Left          左监视器出厂静态视野Y坐标
 * @param KDefalutViewX_Right         右监视器出厂静态视野X坐标
 * @param KDefalutViewY_Right         右监视器出厂静态视野Y坐标
 * @param KDefaultBackLight           出厂默认背景光
 * @param KReverseDebanceTimer        倒车档防抖时间
 * @param KToTurnViewAngle            转向视野切换方向盘角度阀值
 * @param KToNonTurnViewAngle         转向视野恢复方向盘角度阀值
 * @param KToHighspeedViewSpeed       高速视野切换速度阀值
 * @param KToNonHighspeedViewSpeed    高速视野恢复阀值
 * @param KReverseViewMinification    倒车视野图像缩小倍数  精度：0.1
 * @param KReverseViewDownPiexls      倒车视野缩小倍数后下移像素点数。
 * @param KTurnViewMiniFactor	        转向视野图像缩小倍数  精度：0.01
 * @param KTurnViewDownPiexls         转向视野缩小倍数后下移像素点数。
 * @param KHighspeedViewMiniFactor    高速视野图像缩小倍数 精度：0.01
 * @param KLongFocusViewMagniFactor   长焦放大倍数 精度：0.01
 * @param KStaticViewHintTimer        切换到用户静态视野提醒文字显示的时间
 * @param KVedioFilesSaveNum          可保存的录制视频个数
 * @param KCamDirtDetectFreq          摄像头污垢检测频率
 * @param KMonitorHeatTemp_Hi         显示屏自动加热高温阀值 精度：1℃，偏置量：-40℃
 * @param KMonitorHeatTemp_Lo         显示屏自动加热低温阀值 精度：1℃，偏置量：-40℃
 * @param KBacklightSwitch            背光控制开关（系统初始值）
 * @param KAutoBacklightSwitch 	      自动调整背光开关（系统初始值）
 * @param KDayBacklightValue          位置灯关闭时背光亮度（白天模式）
 * @param KNightBacklightValue        位置灯打开时背光亮度（夜晚模式）
 * @param KMonitorHeatStatusInterval  屏幕加热状态获取时间间隔
 */
typedef struct
{
  int16_t KDisplayKeepTimer;          //熄火后图像在监视器维持时间
  int16_t KKeepAliveTimer;            // CMS系统在整车电源模式OFF下保活总时间
  int16_t KDefalutViewX_Left;         //左监视器出厂静态视野X坐标
  int16_t KDefalutViewY_Left;         //左监视器出厂静态视野Y坐标
  int16_t KDefalutViewX_Right;        //右监视器出厂静态视野X坐标
  int16_t KDefalutViewY_Right;        //右监视器出厂静态视野Y坐标
  int16_t KDefaultBackLight;          //出厂默认背景光
  int16_t KReverseDebanceTimer;       //倒车档防抖时间
  int16_t KToTurnViewAngle;           //转向视野切换方向盘角度阀值
  int16_t KToNonTurnViewAngle;        //转向视野恢复方向盘角度阀值
  int16_t KToHighspeedViewSpeed;      //高速视野切换速度阀值
  int16_t KToNonHighspeedViewSpeed;   //高速视野恢复阀值
  int16_t KReverseViewMinification;   //倒车视野图像缩小倍数  精度：0.1
  int16_t KReverseViewDownPiexls;     //倒车视野缩小倍数后下移像素点数。
  int16_t KTurnViewMiniFactor;        //转向视野图像缩小倍数  精度：0.01
  int16_t KTurnViewDownPiexls;        //转向视野缩小倍数后下移像素点数。
  int16_t KHighspeedViewMiniFactor;   //高速视野图像缩小倍数 精度：0.01
  int16_t KLongFocusViewMagniFactor;  //长焦放大倍数 精度：0.01
  int16_t KStaticViewHintTimer;       //切换到用户静态视野提醒文字显示的时间
  int16_t KVedioFilesSaveNum;         //可保存的录制视频个数
  int16_t KCamDirtDetectFreq;         //摄像头污垢检测频率
  int16_t KMonitorHeatTemp_Hi;        //显示屏自动加热高温阀值 精度：1℃，偏置量：-40℃
  int16_t KMonitorHeatTemp_Lo;        //显示屏自动加热低温阀值 精度：1℃，偏置量：-40℃
  int16_t KBacklightSwitch;           //背光控制开关（系统初始值）
  int16_t KAutoBacklightSwitch;       //自动调整背光开关（系统初始值）
  int16_t KDayBacklightValue;         //位置灯关闭时背光亮度（白天模式）
  int16_t KNightBacklightValue;       //位置灯打开时背光亮度（夜晚模式）
  int16_t KMonitorHeatStatusInterval; //屏幕加热状态获取时间间隔
} CMSParamObj;

/**
  * @brief
  * @param  SysPwrMdV;    //整车电源有效标志位
  * @param  SysPwrMd;     //整车电源模式 1 为 on； 0为 off
  *
  * @param  CTDSts;       //外部解锁

  * @param  DrAjr_DRV;
  * @param  DrAjr_DR;
  * @param  DrAjr_PRV;
  * @param  DrAjr_PR;

  * @param  CmrLnsHeatFdbk_L; //左侧摄像头加热反馈
  * @param  CmrLnsHeatFdbk_R; //右侧摄像头加热反馈
  * @param  CmrLnsHeatReq;   //lll

  * @param  VehSpdV;          //车速有效标志位
  * @param  VehSpd;           //当前车速

  * @param  TrShftLvrPosV;    //档位有效标志位
  * @param  TrShftLvrPos;     //档位信息

  * @param  TrnLmpSw;         //转向灯开关
  * @param  TrnLmpSts_L;      //左转向灯标志位
  * @param  TrnLmpSts_R;      //右转向灯标志位

  * @param  StrgWhAngV;       //方向盘转角有效标志
  * @param  StrgWhAng;        //方向盘转角值

  * @param  MrrFldSts;        //支架状态 展开或收回
  * @param  MrrFldAvlbl;      //摄像机支架展开和折叠按钮
  * @param  MrrFldOprtCtrlReq;//折叠请求计数

  * @param  PosLmpSts_F;      //前位置灯
  * @param  PosLmpSts_R;      //后位置灯

  * @param  Enable_RecordVideo;//录像标志位

  * @param  BDAWrnngLvlL;
  * @param  DOWWrnngLvlL;
  * @param  BDAWrnngLvlR;
  * @param  DOWWrnngLvlR;
 *
 */
typedef struct
{
  bool SysPwrMdV; //整车电源有效标志位
  bool SysPwrMd;  //整车电源模式 1 为 on； 0为 off

  u_int8_t CTDSts; //外部解锁

  bool DrAjr_DRV;
  bool DrAjr_DR;
  bool DrAjr_PRV;
  bool DrAjr_PR;

  u_int8_t CmrLnsHeatFdbk_L; //左侧摄像头加热反馈
  u_int8_t CmrLnsHeatFdbk_R; //右侧摄像头加热反馈
  u_int8_t CmrLnsHeatReq;    //

  bool VehSpdV;   //车速有效标志位
  int16_t VehSpd; //当前车速

  bool TrShftLvrPosV;    //档位有效标志位
  u_int8_t TrShftLvrPos; //档位信息

  u_int8_t TrnLmpSw; //转向灯开关
  bool TrnLmpSts_L;  //左转向灯标志位
  bool TrnLmpSts_R;  //右转向灯标志位

  bool StrgWhAngV;   //方向盘转角有效标志
  int16_t StrgWhAng; //方向盘转角值

  bool MrrFldSts;             //支架状态 展开或收回
  bool MrrFldAvlbl;           //摄像机支架展开和折叠按钮
  u_int8_t MrrFldOprtCtrlReq; //折叠请求计数

  bool PosLmpSts_F; //前位置灯
  bool PosLmpSts_R; //后位置灯

  bool Enable_RecordVideo; //录像标志位

  bool BDAWrnngLvlL;
  bool DOWWrnngLvlL;
  bool BDAWrnngLvlR;
  bool DOWWrnngLvlR;
} CMSCanSignalObj;

/**
 * @brief
 * @param canID CAN ID
 * @param length 数据长度
 * @param buff  数据缓冲区
 */
typedef struct
{
  u_int32_t canID;   // CAN ID
  u_int8_t length;   // 数据长度
  u_int8_t buff[64]; // 数据缓冲区
} CMSUdpCanObj;

/**
 * @brief
 * @param cmsParamObj 车辆后视镜相关参数
 * @param cmsCanSignalObj 车辆状态信号
 * @param cmsCanObj
 */
typedef struct
{
  CMSParamObj cmsParamObj;
  CMSCanSignalObj cmsCanSignalObj;
  CMSUdpCanObj cmsUdpCanObj;
} CANObj;

// typedef  union{

//   struct{
//           unsigned char bit0: 1;
//           unsigned char bit1: 1;
//           unsigned char bit2: 1;
//           unsigned char bit3: 1;
//           unsigned char bit4: 1;
//           unsigned char bit5: 1;
//           unsigned char bit6: 1;
//           unsigned char bit7: 1;
//           unsigned char bit8: 1;
//           // unsigned char bit9: 1;
//           // unsigned char bit10: 1;
//           // unsigned char bit11: 1;
//           // unsigned char bit12: 1;
//           // unsigned char bit13: 1;
//           // unsigned char bit14: 1;
//           // unsigned char bit15: 1;
//           // unsigned char bit16: 1;
//           // unsigned char bit17: 1;
//           // unsigned char bit18: 1;
//           // unsigned char bit19: 1;
//           // unsigned char bit20: 1;
//           // unsigned char bit21: 1;
//           // unsigned char bit22: 1;
//           // unsigned char bit23: 1;
//           // unsigned char bit24: 1;
//           // unsigned char bit25: 1;
//           // unsigned char bit26: 1;
//           // unsigned char bit27: 1;
//           // unsigned char bit28: 1;
//           // unsigned char bit29: 1;
//           // unsigned char bit30: 1;
//           // unsigned char bit31: 1;
//           // unsigned char bit32: 1;
//           // unsigned char bit33: 1;
//           // unsigned char bit34: 1;
//           // unsigned char bit35: 1;
//           // unsigned char bit36: 1;
//           // unsigned char bit37: 1;
//           // unsigned char bit38: 1;
//           // unsigned char bit39: 1;
//           // unsigned char bit40: 1;
//           // unsigned char bit41: 1;
//           // unsigned char bit42: 1;
//           // unsigned char bit43: 1;
//           // unsigned char bit44: 1;
//           // unsigned char bit45: 1;
//           // unsigned char bit46: 1;
//           // unsigned char bit47: 1;
//           // unsigned char bit48: 1;
//           // unsigned char bit49: 1;
//           // unsigned char bit50: 1;
//           // unsigned char bit51: 1;
//           // unsigned char bit52: 1;
//           // unsigned char bit53: 1;
//           // unsigned char bit54: 1;
//           // unsigned char bit55: 1;
//           // unsigned char bit56: 1;
//           // unsigned char bit57: 1;
//           // unsigned char bit58: 1;
//           // unsigned char bit59: 1;
//           // unsigned char bit60: 1;
//           // unsigned char bit61: 1;
//           // unsigned char bit62: 1;
//           // unsigned char bit63: 1;
//   }field;
//   char buff[8];
//   };

/************************************************************************/
/*宏定义数据段*/
/************************************************************************/
#define CAN_VehiclePwrSts_MESSAGE_ID 0x000001BD

/************************************************************************/
/*函数声明段*/
/************************************************************************/

/**
 * @brief can报文分析初始化函数
 *
 * @param canObj CAN报文相关的对象指针
 * @return vx_status 初始化can相关的状态
 */
vx_status app_can_analysis_init(CANObj *canObj);

/**
 * @brief can 报文解析子函数，获取收到的UDP报文的CAN信息
 *
 * @param src   源缓冲数组，从UDP获取到的数据
 * @param canObj 目标地址，将数据放入数据接收区
 * @return vx_status 返回tiovx的状态信息
 */
vx_status app_can_analysis(char *src, void *can);

#endif /*__UDP_SERVER_H__ */
