

#include "app_capture_module.h"
#include <iss_sensors.h>
#include <iss_sensor_if.h>

vx_status app_init_capture(vx_context context,
                           CaptureObj *captureObj,
                           vx_uint32 sensorNum,
                           vx_uint32 img_width,
                           vx_uint32 img_height)
{
    vx_status status = VX_SUCCESS;

    vx_image image;
    vx_uint32 num_capt_instances;
    vx_int32 id, lane, q, ch;

    num_capt_instances = 1;

    tivx_capture_params_init(&captureObj->params);

    captureObj->params.numInst = num_capt_instances;
    captureObj->params.numCh = sensorNum;

    ch = 0;
    for (id = 0; id < num_capt_instances; id++)
    {
        captureObj->params.instId[id] = id;
        captureObj->params.instCfg[id].enableCsiv2p0Support = (uint32_t)vx_true_e;
        captureObj->params.instCfg[id].numDataLanes = sensorNum;
        APP_PRINTF("[App init info -> Capture]  captureObj->params.numDataLanes = %d \n", captureObj->params.instCfg[id].numDataLanes);

        for (lane = 0; lane < captureObj->params.instCfg[id].numDataLanes; lane++)
        {
            captureObj->params.instCfg[id].dataLanesMap[lane] = lane + 1;
            APP_PRINTF("[App init info -> Capture]  captureObj->params.dataLanesMap[%d] = %d \n",
                       lane,
                       captureObj->params.instCfg[id].dataLanesMap[lane]);
        }
        for (q = 0; q < NUM_CAPT_CHANNELS; q++)
        {
            captureObj->params.chVcNum[ch] = q;
            captureObj->params.chInstMap[ch] = id;
            ch++;
        }
    }

    captureObj->config = vxCreateUserDataObject(context, "tivx_capture_params_t", sizeof(tivx_capture_params_t), &captureObj->params);

    captureObj->capture_format = VX_DF_IMAGE_UYVY;

    image = vxCreateImage(context, img_width, img_height, VX_DF_IMAGE_UYVY); // VX_DF_IMAGE_UYVY

    for (q = 0; q < sensorNum; q++)
    {
        captureObj->image_arr[q] = vxCreateObjectArray(context, (vx_reference)image, sensorNum);
    }

    vxReleaseImage(&image);

    captureObj->file_path = NULL;
    captureObj->file_prefix = NULL;
    captureObj->write_node = NULL;
    captureObj->write_cmd = NULL;

    return (status);
}
void app_deinit_capture(CaptureObj *captureObj, vx_int32 bufq_depth)
{
    vx_int32 i;

    vxReleaseUserDataObject(&captureObj->config);
    for (i = 0; i < bufq_depth; i++)
    {
        vxReleaseObjectArray(&captureObj->image_arr[i]);
    }
}

void app_delete_capture(CaptureObj *captureObj)
{
    if (captureObj->node != NULL)
    {
        vxReleaseNode(&captureObj->node);
    }
}

vx_status app_create_graph_capture(vx_graph graph, CaptureObj *captureObj)
{
    vx_status status = VX_SUCCESS;

    captureObj->node = tivxCaptureNode(graph, captureObj->config, captureObj->image_arr[0]);
    status = vxGetStatus((vx_reference)captureObj->node);
    vxSetNodeTarget(captureObj->node, VX_TARGET_STRING, TIVX_TARGET_CAPTURE1);
    vxSetReferenceName((vx_reference)captureObj->node, "capture_node");
    return status;
}
