
#ifndef _APP_CAPTURE_MODULE
#define _APP_CAPTURE_MODULE

#include "app_common.h"
//#include "app_sensor_module.h"

#define NUM_CAPT_CHANNELS (4u)

typedef struct
{
    /*! Capture node object */
    vx_node node;

    /*! User data object for config parameter, used as node parameter of Capture node */
    vx_user_data_object config;

    /*! Capture node params structure to initialize config object */
    tivx_capture_params_t params;

    /*! Capture node object array output */
    vx_object_array image_arr[APP_MODULES_MAX_BUFQ_DEPTH];

    /*! Capture node graph parameter index */
    vx_int32 graph_parameter_index;

    /*! Flag to indicate whether or not the intermediate output is written */
    vx_int32 en_out_capture_write;

    /*! Flag to enable test mode */
    vx_int32 test_mode;

    /*! Node used to write capture output */
    vx_node write_node;

    /*! File path used to write capture node output */
    vx_array file_path;

    /*! File path prefix used to write capture node output */
    vx_array file_prefix;

    /*! User data object containing write cmd parameters */
    vx_user_data_object write_cmd;

    /*! Output file path for capture node output */
    vx_char output_file_path[TIVX_FILEIO_FILE_PATH_LENGTH];

    /*! Name of capture module */
    vx_char objName[APP_MODULES_MAX_OBJ_NAME_SIZE];

    /*! Raw image used when camera gets disconnected */
    tivx_raw_image error_frame_raw_image;

    /*! Flag to enable detection of camera disconnection */
    vx_uint8 enable_error_detection;

    /*! Capture node format, taken from sensor_out_format */
    vx_uint32 capture_format;
} CaptureObj;

/**
 * @brief 初始化获取图像节点的相关任务
 *
 * @param context       节点运行的上下文
 * @param captureObj    capture对象指针
 * @param sensorNum     摄像头数量
 * @param img_width     获取到的图像的宽度
 * @param img_height    获取到的图像的高度
 * @return vx_status    初始化capture节点的状态
 */
vx_status app_init_capture(vx_context context,
                           CaptureObj *captureObj,
                           vx_uint32 sensorNum,
                           vx_uint32 img_width,
                           vx_uint32 img_height);

/**
 * @brief 反向初始化capture 节点
 *
 * @param captureObj capture对象指针
 * @param bufq_depth 设置的缓冲区的深度，和摄像头数量有关
 */
void app_deinit_capture(CaptureObj *captureObj, vx_int32 bufq_depth);

/**
 * @brief 删除capture节点
 *
 * @param captureObj capture对象指针
 */
void app_delete_capture(CaptureObj *captureObj);

/**
 * @brief 在graph中创建capture节点用于采集摄像头的图像
 *
 * @param graph     capture节点运行的graph
 * @param captureObj capture对象指针
 * @return vx_status 创建capture节点的状态
 */
vx_status app_create_graph_capture(vx_graph graph, CaptureObj *captureObj);

#endif
