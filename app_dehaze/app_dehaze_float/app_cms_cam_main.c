#include "app_cms_cam_main.h"

AppObj appObj; //创建APP所需的所有对象

vx_status app_init(AppObj *obj)
{
    vx_status status = VX_SUCCESS;

    obj->context = vxCreateContext(); //创建context
    APP_ASSERT_VALID_REF(obj->context);
    obj->graph = vxCreateGraph(obj->context); //创建graph

    tivxHwaLoadKernels(obj->context);     //加载硬件模块
    APP_PRINTF("[App Load Kernel info]   Hwa load Done! \n");

    tivxImagingLoadKernels(obj->context); //加载图像获取模块内核
    APP_PRINTF("[App Load Kernel info]   Imaging load Done! \n");

    tivxImgProcLoadKernels(obj->context); //加载图像预处理模块内核
    APP_PRINTF("[App Load Kernel info]   ImgProc load Done! \n");

    tivxTIDLLoadKernels(obj->context);    //加载图像网络推理、深度学习内核
    APP_PRINTF("[App Load Kernel info]   TIDL load Done! \n");

    tivxFileIOLoadKernels(obj->context);  //加载文件操作相关内核
    APP_PRINTF("[App Load Kernel info]   FileIO load Done! \n");

    tivxDehazeFloatLoadKernels(obj->context); //加载除雾模块
    APP_PRINTF("[App Load Kernel info]   Dehaze load Done! \n");

    // USB 摄像头相关初始化
    if (VX_SUCCESS == status)
    {
        if (VX_SUCCESS == status)
        {
            sprintf(obj->usbCameraObj.devName, "/dev/video2");

            status = app_init_usbCamera(&obj->context, &(obj->usbCameraObj)); //初始化usb摄像头
        }
        else
        {
            printf("USB Camera init error!\n");
        }
    }


    //初始化相机属性参数操作
    if (VX_SUCCESS == status)
    {
        obj->cmsSensorObj.sensorNum = obj->configFileObj.sensorNum;
        obj->cmsSensorObj.sensorOutputHeight = obj->configFileObj.sensorOutputHeight; //复制配置文件内的配置信息
        obj->cmsSensorObj.sensorOutputWidth = obj->configFileObj.sensorOutputWidth;

        status = app_init_sensor(&obj->cmsSensorObj);
        APP_PRINTF("[App init info]    Sensor init done!\n");
    }

    //初始化颜色转换节点内容
    if (VX_SUCCESS == status)
    {
        obj->colorConvertObj.inWidth = obj->cmsSensorObj.sensorOutputWidth;   //摄像头输出图像宽度
        obj->colorConvertObj.inHeight = obj->cmsSensorObj.sensorOutputHeight; //摄像头输出图像高度
        obj->colorConvertObj.outFormat = VX_DF_IMAGE_RGB;                    //经过颜色转换节点以后，输出NV12格式的图像
        obj->colorConvertObj.bufNum = obj->cmsSensorObj.sensorNum;            //根据摄像头数量确定buf数量

        status = app_init_colorConvert(obj->context, &obj->colorConvertObj);

        APP_PRINTF("[App init info]    ColorConvert init done!\n");
    }

    //初始化除雾模块
    if (VX_SUCCESS == status)
    {
        obj->dehazeFloatObj.getDarkAndTransParams.w = 0.95; // w为去雾程度，一般取0.95  w的值越小，去雾效果越不明显

        obj->dehazeFloatObj.config.cameraNum = obj->cmsSensorObj.sensorNum;
        obj->dehazeFloatObj.config.imageWidth = obj->cmsSensorObj.sensorOutputWidth;
        obj->dehazeFloatObj.config.imageHeight = obj->cmsSensorObj.sensorOutputHeight;

        obj->dehazeFloatObj.guideFilterParams.r = 20;
        obj->dehazeFloatObj.guideFilterParams.r = 0.0001;
        obj->dehazeFloatObj.guideFilterParams.t0 = 0.25;

        status = app_init_dehaze_float(obj->context, &obj->dehazeFloatObj, "dehaze");
    }



    //初始化显示相关内容
    if (VX_SUCCESS == status)
    {
        if (1 == obj->configFileObj.displayLeftOption) //如果开启了左侧显示通道，则创建左侧显示通道
        {
            APP_PRINTF("[App init info]    Display Left node init \n");
            memset(&obj->displayobjLeft, 0, sizeof(DisplayObj));
            obj->displayobjLeft.display_option = obj->configFileObj.displayLeftOption;
            obj->displayobjLeft.disp_params.opMode = TIVX_KERNEL_DISPLAY_ZERO_BUFFER_COPY_MODE; // TIVX_KERNEL_DISPLAY_BUFFER_COPY_MODE;
            obj->displayobjLeft.disp_params.pipeId = 0;
            obj->displayobjLeft.disp_params.outWidth = obj->configFileObj.displayLeftOutWidth;   //左侧显示图像输出宽度
            obj->displayobjLeft.disp_params.outHeight = obj->configFileObj.displayLeftOutHeight; //左侧显示图像输出高度
            obj->displayobjLeft.disp_params.posX = obj->configFileObj.displayLeftPosX;           //左侧图像显示的坐标X
            obj->displayobjLeft.disp_params.posY = obj->configFileObj.displayLeftPosY;           //左侧图像显示的坐标Y

            obj->displayobjLeft.disp_params.globalAlpha = 255;
            obj->displayobjLeft.disp_params.preMultiplyAlpha=0;

            sprintf(obj->displayobjLeft.objName, "displayLeft node");
            sprintf(obj->displayobjLeft.targetName, TIVX_TARGET_DISPLAY1);
            status = app_init_display(obj->context, &obj->displayobjLeft, obj->displayobjLeft.objName);
            APP_PRINTF("[App init info]    Display Left node init done !\n");
        }
    }

    if (VX_SUCCESS == status)
    {
        if (1 == obj->configFileObj.displayRightOption) //如果开启了左侧显示通道，则创建左侧显示通道
        {
            APP_PRINTF("[App init info]    Display Right node init \n");
            memset(&obj->displayobjRight, 0, sizeof(DisplayObj));
            obj->displayobjRight.display_option = obj->configFileObj.displayRightOption;
            obj->displayobjRight.disp_params.opMode = TIVX_KERNEL_DISPLAY_ZERO_BUFFER_COPY_MODE; // TIVX_KERNEL_DISPLAY_BUFFER_COPY_MODE;
            obj->displayobjRight.disp_params.pipeId = 3;
            obj->displayobjRight.disp_params.outWidth = obj->configFileObj.displayRightOutWidth;   //左侧显示图像输出宽度
            obj->displayobjRight.disp_params.outHeight = obj->configFileObj.displayRightOutHeight; //左侧显示图像输出高度
            obj->displayobjRight.disp_params.posX = obj->configFileObj.displayRightPosX;           //左侧图像显示的坐标X
            obj->displayobjRight.disp_params.posY = obj->configFileObj.displayRightPosY;           //左侧图像显示的坐标Y


            obj->displayobjRight.disp_params.globalAlpha = 255;
            obj->displayobjRight.disp_params.preMultiplyAlpha=0;


            sprintf(obj->displayobjRight.objName, "displayRight node");
            sprintf(obj->displayobjRight.targetName, TIVX_TARGET_DISPLAY2);
            status = app_init_display(obj->context, &obj->displayobjRight, obj->displayobjRight.objName);
            APP_PRINTF("[App init info]    Display Right node init done !\n");
        }
    }

    return status;
}

vx_status app_deinit(AppObj *obj)
{
    vx_status status = VX_SUCCESS;

    app_deinit_colorConvert(&obj->colorConvertObj);
    APP_PRINTF("[App deinit info]   ColorConvertObj deinit Done! \n");

    app_deinit_dehaze_float(&obj->dehazeFloatObj);
    APP_PRINTF("[App deinit info]   DehazeFloatObj deinit Done! \n");

    app_deinit_display(&obj->displayobjLeft);
    APP_PRINTF("[App deinit info]   Display Left deinit Done! \n");

    app_deinit_display(&obj->displayobjRight);
    APP_PRINTF("[App deinit info]   Display Right deinit Done! \n");


    tivxDehazeFloatUnLoadKernels(obj->context); //卸载除雾相关内核
    APP_PRINTF("[App UnLoad Kernel info]   Dehaze unload Done! \n");

    tivxFileIOUnLoadKernels(obj->context); //加载文件操作相关内核
    APP_PRINTF("[App UnLoad Kernel info]   FileIO unload Done! \n");

    tivxTIDLUnLoadKernels(obj->context); //加载图像网络推理、深度学习内核
    APP_PRINTF("[App UnLoad Kernel info]   TIDL unload Done! \n");

    tivxImgProcUnLoadKernels(obj->context); //加载图像预处理模块内核
    APP_PRINTF("[App UnLoad Kernel info]   ImgProc unload Done! \n");

    tivxImagingUnLoadKernels(obj->context); //加载图像获取模块内核
    APP_PRINTF("[App UnLoad Kernel info]   Imaging unload Done! \n");

    tivxHwaUnLoadKernels(obj->context);
    APP_PRINTF("[App UnLoad Kernel info]   HWA unload Done! \n");

    vxReleaseContext(&obj->context);
    APP_PRINTF("[App UnLoad Kernel info]   Context released! \n");

    return status;
}

vx_status app_create_graph(AppObj *obj)
{
    vx_status status = VX_SUCCESS;

    if (VX_SUCCESS == status) //创建颜色转换节点从VX_DF_IMAGE_UYVY 格式转换成 NV12格式
    {
        status = app_create_colorConvert(obj->graph,
                                         &obj->colorConvertObj,
                                         &obj->colorConvertObj.inImage_arr[0],
                                         &obj->colorConvertObj.outFrames[0]);


                                         
        APP_PRINTF("[Graph info]    ColorConvert graph create done !\n");
    }

    if (VX_SUCCESS == status)
    {
        status = app_create_graph_dehaze_float(obj->graph,
                                               &obj->dehazeFloatObj,
                                               &obj->colorConvertObj.outFrames[0],
                                               &obj->dehazeFloatObj.out[0]);

        APP_PRINTF("[Graph info]    Dehaze graph create done !\n");
    }

    if (VX_SUCCESS == status) //创建display节点，用于显示图像
    {
        if (1 == obj->displayobjLeft.display_option) //如果使能左侧显示通道，则创建左侧相关
        {
            //vx_image displayLeft_image = (vx_image)vxGetObjectArrayItem(obj->colorConvertObj.outFrames[0], 0);
            vx_image displayLeft_image = (vx_image)vxGetObjectArrayItem(obj->dehazeFloatObj.out[0], 0);

            status = app_create_graph_display(obj->graph, &obj->displayobjLeft, displayLeft_image);
            APP_PRINTF("[Graph info]    Display Left graph done!\n");
            vxReleaseImage(&displayLeft_image);
        }
    }

    if (VX_SUCCESS == status) //创建display节点，用于显示图像
    {
        if (1 == obj->displayobjRight.display_option) //如果使能左侧显示通道，则创建左侧相关
        {
            vx_image displayRight_image = (vx_image)vxGetObjectArrayItem(obj->colorConvertObj.outFrames[0], 0);
            
            //vx_image displayRight_image = (vx_image)vxGetObjectArrayItem(obj->dehazeFloatObj.out[0], 0);

            status = app_create_graph_display(obj->graph, &obj->displayobjRight, displayRight_image);
            APP_PRINTF("[Graph info]    Display Right graph done!\n");
            vxReleaseImage(&displayRight_image);
        }
    }


    return status;
}

vx_status app_delete_graph(AppObj *obj)
{
    vx_status status = VX_SUCCESS;

    app_delete_sensor(&obj->cmsSensorObj);
    APP_PRINTF("[App delete node info]   cmsSensorObj node deleted! \n");
#ifdef DEBUG_WITH_SENSOR
    app_delete_capture(&obj->captureObj);
    APP_PRINTF("[App delete node info]   captureObj node deleted! \n");
#endif
    app_delete_colorConvert(&obj->colorConvertObj);
    APP_PRINTF("[App delete node info]   colorConvertObj node deleted! \n");

    app_delete_colorConvert(&obj->colorConvertObjTest);
    APP_PRINTF("[App delete node info]   colorConvertObjTest node deleted! \n");

    app_delete_dehaze_float(&obj->dehazeFloatObj);
    APP_PRINTF("[App delete node info]   dehazeFloatObj node deleted! \n");

    app_delete_scaler(&obj->scalerObj);
    APP_PRINTF("[App delete node info]   scalerObj node deleted! \n");

    app_delete_pre_proc(&obj->preProcObj);
    APP_PRINTF("[App delete node info]   preProcObj node deleted! \n");

    app_delete_tidl(&obj->tidlObj);
    APP_PRINTF("[App delete node info]   tidlObj node deleted! \n");

    app_delete_draw_detections(&obj->drawDetectionsObj);
    APP_PRINTF("[App delete node info]   drawDetectionsObj node deleted! \n");

    app_delete_img_mosaic(&obj->imgMosaicObj);
    APP_PRINTF("[App delete node info]   imgMosaicObj node deleted! \n");

    app_delete_display(&obj->displayobjLeft);
    APP_PRINTF("[App delete node info]   displayobjLeft node deleted! \n");

    app_delete_display(&obj->displayobjRight);
    APP_PRINTF("[App delete node info]   displayobjRight node deleted! \n");

    app_delete_display(&obj->displayobjRight);
    APP_PRINTF("[App delete node info]   displayobjRight node deleted! \n");

    vxReleaseGraph(&obj->graph);
    APP_PRINTF("[App delete node info]   graph deleted! \n");

    return status;
}

vx_status app_run_remoteService(AppObj *obj)
{
    vx_status status = VX_SUCCESS;
    AppSensorCmdParams cmdPrms;
    /* After first trigger, configure and start the sensor */
    cmdPrms.numSensors = obj->cmsSensorObj.sensorNum;
    if (cmdPrms.numSensors <= 4)
    {
        cmdPrms.portNum = 1;
    }
    else
    {
        cmdPrms.portNum = 2;
    }
    APP_PRINTF("[Remote Service info]    cmdPrms.numSensors =%d,NUM_CAPT_CHANNELS =%d\n", cmdPrms.numSensors, obj->cmsSensorObj.sensorNum);
    unsigned int loop_id;
    for (loop_id = 0U; loop_id < cmdPrms.portNum; loop_id++)
    {
        cmdPrms.portIdMap[loop_id] = loop_id;
    }

#ifdef DEBUG_WITH_SENSOR    //如果使用SENSOR作为图像的输入，则启用此部分
    if (VX_SUCCESS == status)
    {
        APP_PRINTF("[Remote Service info]    Remote service creating ...\n");
        status = appRemoteServiceRun(APP_IPC_CPU_MCU2_0,
                                     APP_REMOTE_SERVICE_SENSOR_NAME,
                                     APP_REMOTE_SERVICE_SENSOR_CMD_CONFIG_MAX96712_96701, &cmdPrms, sizeof(cmdPrms), 0);
        if (VX_SUCCESS == status)
        {
            APP_PRINTF("[Remote Service info]   Remote service create done !\n");
        }
        else
        {
            printf("[Error Remote Service]  appRemoteServiceRun is error\n");
        }
    }
#endif

    return status;
}

vx_status add_graph_parameter_by_node_index(vx_graph graph, vx_node node, vx_uint32 node_parameter_index)
{
    vx_status status = VX_SUCCESS;
    vx_parameter parameter = vxGetParameterByIndex(node, node_parameter_index);
    if (VX_SUCCESS == status)
    {
        status = vxAddParameterToGraph(graph, parameter);
    }
    status = vxReleaseParameter(&parameter);
    return status;
}

void app_task_run_graph(void *app_var)
{
    vx_status status = VX_SUCCESS;
    AppObj *obj = (AppObj *)app_var;


    if (VX_SUCCESS == status) // graph状态及功能检查
    {
        APP_PRINTF("[Verify Graph info]    vx verify graph starting ......\n");
        status = vxVerifyGraph(obj->graph); //检查graph 状态
        if (VX_SUCCESS == status)
        {
            APP_PRINTF("[Verify Graph info]    vx verify graph is done\n");
        }
        else
        {
            printf("[Error Verify Graph info]    vx verify graph error!\n");
        }
    }

    // vx_image img;
    // char filename[128];
    // vx_uint32 img_width, img_height;

    while (VX_SUCCESS == status)
    {
        status = tivxMutexLock(obj->usbCameraObj.mutex); //上锁，准备获取USB数据
        if (VX_SUCCESS == status)
        {
            status = app_usb_camera_read_to_image(&obj->colorConvertObj.inImage_arr[0], &obj->usbCameraObj.imageOutYUYV);
        }
        if (VX_SUCCESS == status)
        {
            status = tivxMutexUnlock(obj->usbCameraObj.mutex); //解锁
        }

        if (status == VX_SUCCESS)
        {
            status = vxProcessGraph(obj->graph); //执行graph，等待颜色转换完成
        }
        APP_PRINTF("vxProcessGraph running !\n");

    //     if (status == VX_SUCCESS)
    //     {

    // //        img = tivx_utils_create_vximage_from_pngfile(obj->context, "/home/root/sv52/pic/bg_right.png", vx_false_e);

    //         img = vxCreateImage(obj->context, 480, 322, VX_DF_IMAGE_RGB); // VX_DF_IMAGE_UYVY

    //         sprintf(filename, "/home/root/sv52/pic/bg_right.png");

    //         status = tivx_utils_load_vximage_from_pngfile(img, filename, vx_false_e);

    //         if (status == VX_SUCCESS)
    //         {
    //             printf("Image from PNG load OK!\n");
    //             vxQueryImage(img, VX_IMAGE_WIDTH, &img_width, sizeof(vx_uint32));
    //             vxQueryImage(img, VX_IMAGE_HEIGHT, &img_height, sizeof(vx_uint32));                
    //         }
    //         else
    //         {
    //             printf("Image from PNG load Failed!\n");
    //         }


    //         vxReleaseImage(&img);
    //     }


    }
    //结束Pipeline工作，释放所有资源
    if (VX_SUCCESS == status)
    {
        status = vxWaitGraph(obj->graph); /* ensure all graph processing is complete */
        status = app_delete_graph(obj);   //删除graph
        status = app_deinit(obj);         //反向初始化
        APP_PRINTF("[Delete Graph info] App Delete Graph Done! \n");
    }
    obj->graphStopTaskDone = 1;
}

void app_task_run_interactive(void *app_var)
{
    vx_status status = VX_SUCCESS;
    AppObj *obj = (AppObj *)app_var;
    char ch;

    obj->isInteractiveStopTask = 0;
    obj->isInteractiveStopTaskDone = 0;

    while (!obj->isInteractiveStopTask)   //用户交互程序
    {
        ch = getchar();
        switch (ch)
        {
        case 'x':
            obj->isInteractiveStopTask = 1;         //停止交互的task
            obj->graphStopTask = 1;                 //停止graph运行
            #ifdef DEBUG_WITH_USB_CAMERA
            obj->usbCameraObj.stopTask = 1;         //USB摄像头停止采集图像
            #endif
            obj->stateMschineObj.taskStop = 1;      //状态机任务停止

            break;
        }
        tivxTaskWaitMsecs(50); // 50ms执行一次
    }

    if (VX_SUCCESS == status)
    {
        while (!obj->graphStopTaskDone) //等待结束标志
        {
            tivxTaskWaitMsecs(100); // 100ms执行一次
        }
        tivxTaskDelete(&obj->graphTask);

        #ifdef DEBUG_WITH_USB_CAMERA
        while (!obj->usbCameraObj.stopTaskDone) //等待结束标志
        {
            tivxTaskWaitMsecs(100); // 100ms执行一次
        }
        tivxTaskDelete(&obj->usbCameraObj.task);
        #endif

        while (!obj->stateMschineObj.stopTaskDone) //等待结束标志
        {
            tivxTaskWaitMsecs(100); // 100ms执行一次
        }
        tivxTaskDelete(&obj->stateMschineObj.task);
    }
}

vx_status app_task_create(AppObj *obj)
{
    tivx_task_create_params_t params;
    vx_status status = VX_SUCCESS;

    if (VX_SUCCESS == status) //创建graph运行任务
    {
        APP_PRINTF("[Create Task info]    App Graph task creating ......\n");
        tivxTaskSetDefaultCreateParams(&params);
    
        params.task_main = app_task_run_graph;
        params.app_var = obj;
        obj->graphStopTask = 0;
        obj->graphStopTaskDone = 0;
        status = tivxTaskCreate(&obj->graphTask, &params);

        if (VX_SUCCESS == status)
        {
            APP_PRINTF("[Create Task info]    App Graph task create done ! \n");
        }
        else
        {
            printf("[Error Create Task info]    App Graph task create error ! \n");
        }
    }

    if (VX_SUCCESS == status) //创建UDP运行任务
    {
        status = app_can_analysis_init(&obj->canObj);
        if (VX_SUCCESS == status)
        {
            APP_PRINTF("[Create Task info]    App UDP task creating ......\n");

            obj->socketUdpObj.udp_can_analysis = app_can_analysis;
            obj->socketUdpObj.outBuff = (void *)(&obj->canObj.cmsUdpCanObj);
            obj->socketUdpObj.port = 8600;
            obj->socketUdpObj.taskFunciton = &app_socket_udpServerTask;
            status = app_socket_udpServer_task_create(&obj->socketUdpObj); //创建UDP socket server 任务
            if (VX_SUCCESS == status)
            {
                APP_PRINTF("[Create Task info]    App UDP task create done ! \n");
            }
            else
            {
                printf("[Error Create Task info]    App UDP task create error ! \n");
            }
        } 
        else
        {
            printf("[Error Create Task info]    App UDP init error ! \n");             
        }       

    }

    if(VX_SUCCESS == status)//创建状态机运行任务    
    {
        status = app_state_machine_init(&obj->stateMschineObj);
        if(VX_SUCCESS == status)
        {
            status = app_state_machine_task_create(&obj->stateMschineObj);
            if(VX_SUCCESS == status)
            {
                APP_PRINTF("[Create Task info]    App State Machine task create done ! \n");
            }
            else
            {
                printf("[Error Create Task info]    App State Machine task create error ! \n");                 
            }
        }
        else
        {
            printf("[Error Create Task info]    App State Machine init error ! \n");           
        }
    }

    if(VX_SUCCESS == status)//创建摄像头采集图像任务
    {
        #ifdef DEBUG_WITH_USB_CAMERA
        //创建USB 摄像头任务，获取USB 摄像头图像 720P
        obj->usbCameraObj.run_task = &app_usb_camera_task_runVideo1;
        app_usb_camera_task_create(&obj->usbCameraObj);
        #endif
    }
    return status;
}

void app_cms_cam_main(int argc, char *argv[])
{
    vx_status status = VX_SUCCESS;
    AppObj *obj = &appObj;

    memset(obj, 0, sizeof(AppObj)); //初始化所有对象

    APP_PRINTF("[CMS Vision]    3.2.0 !\n");
    if (VX_SUCCESS == status) //读取命令行输入的配置文件内容
    {
        status = app_parse_cmd_line_args(&obj->configFileObj, argc, argv); //从命令行获取文件名称，然后读取配置
    }

    if (VX_SUCCESS == status) //各个模块初始化操作
    {
        APP_PRINTF("[App init info]    App init strating ...... !\n");
        status = app_init(obj); //应用初始化操作
        APP_PRINTF("[App init info]    App init done !\n\n");
    }

    if (VX_SUCCESS == status) //创建graph内运行的各个node
    {
        APP_PRINTF("[Create Graph info]    App creating graph......\n");
        status = app_create_graph(obj); //创建graph的各个node
        APP_PRINTF("[Create Graph info]    App create graph done !\n\n");
    }

    if (VX_SUCCESS == status)
    {
        APP_PRINTF("[Create Task info]    App creating tasks......\n");
        status = app_task_create(obj); //创建各个任务
        APP_PRINTF("[Create Task info]    App creating tasks done!  \n\n");
    }

    if (VX_SUCCESS == status) //创建交互任务
    {
        APP_PRINTF("[Create Task info]    App interactive task creating......\n");
        app_task_run_interactive((void *)obj);
    }
}

vx_status app_get_cam_od_lable(AppObj *obj)
{
    vx_status status = VX_SUCCESS;

    vx_tensor output;
    vx_size numCh;
    vx_int32 ch;

    vxQueryObjectArray((vx_object_array)obj->tidlObj.output_tensor_arr[0], VX_OBJECT_ARRAY_NUMITEMS, &numCh, sizeof(vx_size));

    for (ch = 0; ch < numCh; ch++)
    {
        vx_size num_dims;
        void *data_ptr;
        vx_map_id map_id;

        vx_size start[APP_MAX_TENSOR_DIMS];
        vx_size tensor_strides[APP_MAX_TENSOR_DIMS];
        vx_size tensor_sizes[APP_MAX_TENSOR_DIMS];

        /* Write Y plane */
        output = (vx_tensor)vxGetObjectArrayItem((vx_object_array)obj->tidlObj.output_tensor_arr[0], ch);

        vxQueryTensor(output, VX_TENSOR_NUMBER_OF_DIMS, &num_dims, sizeof(vx_size));

        if (num_dims != 3)
        {
            printf("Number of dims are != 3! exiting.. \n");
            break;
        }

        vxQueryTensor(output, VX_TENSOR_DIMS, tensor_sizes, 3 * sizeof(vx_size));

        start[0] = start[1] = start[2] = 0;

        tensor_strides[0] = 1;
        tensor_strides[1] = tensor_strides[0];
        tensor_strides[2] = tensor_strides[1] * tensor_strides[1];

        status = tivxMapTensorPatch(output, num_dims, start, tensor_sizes, &map_id, tensor_strides, &data_ptr, VX_READ_ONLY, VX_MEMORY_TYPE_HOST);

        sTIDL_IOBufDesc_t *ioBufDesc;

        TIDL_ODLayerHeaderInfo *pHeader;
        TIDL_ODLayerObjInfo *pObjInfo;
        vx_float32 *pOut;
        vx_uint32 numObjs;
        vx_float32 *output_buffer;
        vx_size output_sizes[4];

        vx_int32 i;

        ioBufDesc = (sTIDL_IOBufDesc_t *)&obj->drawDetectionsObj.params.ioBufDesc;

        output_buffer = (vx_float32 *)data_ptr;

        output_sizes[0] = ioBufDesc->outWidth[0] + ioBufDesc->outPadL[0] + ioBufDesc->outPadR[0];
        output_sizes[1] = ioBufDesc->outHeight[0] + ioBufDesc->outPadT[0] + ioBufDesc->outPadB[0];
        output_sizes[2] = ioBufDesc->outNumChannels[0];

        pOut = (vx_float32 *)output_buffer + (ioBufDesc->outPadT[0] * output_sizes[0]) + ioBufDesc->outPadL[0];
        pHeader = (TIDL_ODLayerHeaderInfo *)pOut;
        pObjInfo = (TIDL_ODLayerObjInfo *)((uint8_t *)pOut + (vx_uint32)pHeader->objInfoOffset);
        numObjs = (vx_uint32)pHeader->numDetObjects;

        for (i = 0; i < numObjs; i++)
        {
            pObjInfo = pObjInfo;
            TIDL_ODLayerObjInfo *pDet = (TIDL_ODLayerObjInfo *)((uint8_t *)pObjInfo + (i * ((vx_uint32)pHeader->objInfoSize)));

            if ((pDet->score >= obj->drawDetectionsObj.params.viz_th)) // mAP值大于用户设定值
            {
                // vx_int32 label = (vx_int32)pDet->label;
                // APP_PRINTF("Label = %i  It's a %s !!!  Score = %f\n",
                //           label, (char *)&od_coco_labels[label], pDet->score);
            }
        }

        if (VX_SUCCESS == status)
        {
            tivxUnmapTensorPatch(output, map_id);
        }
        vxReleaseTensor(&output);
    }
    return (status);
}



#ifdef DEBUG_WITH_USB_CAMERA
void app_find_object_array_index(vx_object_array object_array[], vx_reference ref, vx_int32 array_size, vx_int32 *array_idx)
{
    vx_int32 i;

    *array_idx = -1;

    for (i = 0; i < array_size; i++)
    {
        vx_image img_ref = (vx_image)vxGetObjectArrayItem((vx_object_array)object_array[i], 0);
        if(ref == (vx_reference)img_ref)
        {
            *array_idx = i;
            vxReleaseImage(&img_ref);
            break;
        }
        vxReleaseImage(&img_ref);
    }
}
#endif
