
#ifndef _APP_CMS_MAIN_H_
#define _APP_CMS_MAIN_H_

#include <string.h>
#include <unistd.h>

#include <VX/vx.h>
#include <TI/tivx.h>
#include <TI/j7.h>

#include <TI/j7_kernels_imaging_aewb.h>

#include <utils/draw2d/include/draw2d.h>
#include <utils/perf_stats/include/app_perf_stats.h>
#include <utils/console_io/include/app_get.h>
#include <utils/grpx/include/app_grpx.h>
#include <utils/sensors/include/app_sensors.h>
#include <utils/remote_service/include/app_remote_service.h>
#include <utils/ipc/include/app_ipc.h>
#include <VX/vx_khr_pipelining.h>



#include "app_common.h"
#include "app_config_file.h"
#include "app_sensor.h"
//#include "app_sensor_module.h"
#include "app_capture_module.h"
// #include "app_viss_module.h"
// #include "app_aewb_module.h"
// #include "app_ldc_module.h"
#include "app_scaler_module.h"
#include "app_pre_proc_module.h"
#include "app_tidl_module.h"
#include "app_draw_detections_module.h"
#include "app_img_mosaic_module.h"
#include "app_display_module.h"
#include "app_color_converte_module.h"
#include "app_socket_udpServer.h"
#include "app_can_analysis.h"
#include "app_state_machine.h"
#include "app_dehaze_float_module.h"

#ifdef DEBUG_WITH_USB_CAMERA
#include "app_usb_camera.h"
#endif


#include "labels.h"
// static char menu[] = {
//     "\n"
//     "\n ========================="
//     "\n TIDL Demo - Object Detection"
//     "\n ========================="
//     "\n"
//     "\n p: Print performance statistics"
//     "\n"
//     "\n x: Exit"
//     "\n"
//     "\n Enter Choice: "
// };

#define APP_PIPELINE_DEPTH (7)





/**
 * @brief 应用所有参数列表
 *
 * @param configFileObj 配置文件对象
 * @param cmsSensorObj;  //传感器对象
 * @param captureObj;      //获取图像对象
 * @param colorConvertObj;//颜色转换对象
 * @param scalerObj;    //图像缩放对象
 * @param preProcObj;  //预处理对象
 * @param tidlObj;        //深度学习对象
 * @param drawDetectionsObj;//绘制检测对象
 * @param imgMosaicObj;  //图像镶嵌对象
 * @param displayobjLeft; //图像显示对象
 * @param displayobjRight; //图像显示对象
 * @param canObj;       //CAN报文对象
 * @param socketUdpObj； //UDP通讯相关
 * @param context;  //APP运行graph的上下文
 * @param graph；   //所有node运行的graph参数
 * @param graphTask;      //Task入口指针，graph run task
 * @param graphStopTask；  //graph task 停止标志位
 * @param graphStopTaskDone； //graph task 停止运行完成标志位
 */
typedef struct
{
    ConfigFileObj configFileObj; //配置文件对象

#ifdef DEBUG_WITH_USB_CAMERA
    USBCameraObj usbCameraObj;       // USB摄像头相关结构
#endif

    // modules
    CmsSensorObj cmsSensorObj;                  //传感器对象
    CaptureObj captureObj;                      //获取图像对象
    ColorConvertObj colorConvertObj;            //颜色转换对象
    ColorConvertObj colorConvertObjTest;            //颜色转换对象
    DehazeFloatObj dehazeFloatObj;                        //除雾相关对象
    ScalerObj scalerObj;                        //图像缩放对象
    PreProcObj preProcObj;                      //预处理对象
    TIDLObj tidlObj;                            //深度学习对象
    DrawDetectionsObj drawDetectionsObj;        //绘制检测对象
    ImgMosaicObj imgMosaicObj;                  //图像镶嵌对象
    DisplayObj displayobjLeft, displayobjRight; //图像显示对象

    CANObj canObj;             // CAN报文对象
    SocketUdpObj socketUdpObj; // UDP通讯相关

    StateMachineObj stateMschineObj;    //状态机相关内容

    // tiovx
    vx_context context; //应用上下文
    vx_graph graph;     //应用graph

    tivx_task graphTask;
    vx_uint32 graphStopTask;
    vx_uint32 graphStopTaskDone;

    vx_uint32 isInteractiveStopTask;
    vx_uint32 isInteractiveStopTaskDone;


    app_perf_point_t total_perf;
    app_perf_point_t fileio_perf;
    app_perf_point_t draw_perf;

} AppObj;

extern AppObj appObj;

/**
 * @brief APP 运行初始化函数
 *
 * @param obj 参数指针
 * @return vx_status 初始化状态
 */
vx_status app_init(AppObj *obj);

/**
 * @brief APP 逆向初始化函数
 *
 * @param obj 参数指针
 * @return vx_status 初始化状态
 */
vx_status app_deinit(AppObj *obj);

/**
 * @brief 创建各个node形成Graph函数
 *
 * @param obj 参数指针
 * @return vx_status 返回创建graph的状态
 */
vx_status app_create_graph(AppObj *obj);

/**
 * @brief APP 删除graph函数
 *
 * @param obj 参数指针
 * @return vx_status
 */
vx_status app_delete_graph(AppObj *obj);

/**
 * @brief 运行远程服务函数
 *
 * @param obj 参数指针
 * @return vx_status 返回状态
 */
vx_status app_run_remoteService(AppObj *obj);

/**
 * @brief 自定义函数通过索引向graph添加参数
 *
 * @param graph 添加参数的graph
 * @param node 指定添加参数的node
 * @param node_parameter_index 指定添加参数的索引号
 * @return vx_status 返回添加状态
 */
vx_status add_graph_parameter_by_node_index(vx_graph graph, vx_node node, vx_uint32 node_parameter_index);

/**
 * @brief graph运行子函数，作为单独任务运行graph
 *
 * @param app_var 创建任务时的传入参数
 */
void app_task_run_graph(void *app_var);

/**
 * @brief 终端交互任务运行子函数，作为单独任务接收终端命令行输入的各个指令
 *
 * @param app_var  创建任务时的传入参数
 */
void app_task_run_interactive(void *app_var);

/**
 * @brief 创建所有任务入口
 *
 * @param obj 创建任务时的参数指针
 * @return vx_status 返回各个任务的创建状态
 */
vx_status app_task_create(AppObj *obj);

/**
 * @brief CMS 流媒体外后视镜运行主程序
 *
 * @param argc 外部参数数量
 * @param argv 外部参数列表
 */
void app_cms_cam_main(int argc, char *argv[]);

/**
 * @brief 获取目标识别后的物体标签，可以实现标签对应识别到的物体
 * 
 * @param obj 
 * @return vx_status 
 */
vx_status app_get_cam_od_lable(AppObj *obj);



#ifdef DEBUG_WITH_USB_CAMERA

void app_find_object_array_index(vx_object_array object_array[], vx_reference ref, vx_int32 array_size, vx_int32 *array_idx);
#endif

#endif
