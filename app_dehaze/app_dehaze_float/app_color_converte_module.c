
#include "app_color_converte_module.h"

vx_status app_init_colorConvert(vx_context context, ColorConvertObj *colorConvertobj)
{
    vx_status status = VX_SUCCESS;
    vx_image image;

    image = vxCreateImage(context,
                          colorConvertobj->inWidth,
                          colorConvertobj->inHeight,
                          colorConvertobj->outFormat);
    uint16_t i = 0;
    for (i = 0; i < colorConvertobj->bufNum; i++)
    {
        colorConvertobj->outFrames[i] = vxCreateObjectArray(context,
                                                            (vx_reference)image,
                                                            colorConvertobj->bufNum);
    }

#ifdef DEBUG_WITH_USB_CAMERA
    vx_image usbImage = vxCreateImage(context, colorConvertobj->inWidth,colorConvertobj->inHeight, VX_DF_IMAGE_YUYV); // VX_DF_IMAGE_UYVY
    for (i = 0; i < colorConvertobj->bufNum; i++)
    {
        colorConvertobj->inImage_arr[i] = vxCreateObjectArray(context, (vx_reference)usbImage, colorConvertobj->bufNum);
        colorConvertobj->input_images[i]  = (vx_image)vxGetObjectArrayItem((vx_object_array)colorConvertobj->inImage_arr[i], 0);        
    }
    vxReleaseImage(&usbImage);
#endif


    vxReleaseImage(&image);

    return status;
}

void app_deinit_colorConvert(ColorConvertObj *colorConvertobj)
{
    vx_int16 i = 0;
    for (i = 0; i < colorConvertobj->bufNum; i++)
    {
        vxReleaseObjectArray(&colorConvertobj->outFrames[i]);
#ifdef DEBUG_WITH_USB_CAMERA
        vxReleaseObjectArray(&colorConvertobj->inImage_arr[i]);
#endif
    }


}

void app_delete_colorConvert(ColorConvertObj *colorConvertobj)
{
    if (colorConvertobj->node != NULL)
    {
        vxReleaseNode(&colorConvertobj->node);
    }
}

vx_status app_create_colorConvert(vx_graph graph, ColorConvertObj *colorConvertobj, vx_object_array *inArray, vx_object_array *outArray)
{
    vx_status status = VX_SUCCESS;

    vx_image inImage = (vx_image)vxGetObjectArrayItem(inArray[0], 0);
    vx_image outImage = (vx_image)vxGetObjectArrayItem(outArray[0], 0);

    colorConvertobj->node = vxColorConvertNode(graph, inImage, outImage); //创建颜色转换node

    status = (vx_status)vxGetStatus((vx_reference)colorConvertobj->node);

    if (VX_SUCCESS == status)
    {
        status = vxSetNodeTarget(colorConvertobj->node, //设置目标target为DSP1
                                 VX_TARGET_STRING,
                                 TIVX_TARGET_DSP1);
    }

    // if (VX_SUCCESS == status)
    // {
    //     vx_bool replicate[] = {vx_true_e, vx_true_e};
    //     status = vxReplicateNode(graph, colorConvertobj->node, replicate, 2);
    // }
    return status;
}
