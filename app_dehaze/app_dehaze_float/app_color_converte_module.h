
#ifndef _APP_COLOR_CONVERTE_MODULE
#define _APP_COLOR_CONVERTE_MODULE

#include "app_common.h"

/**
 * @brief 颜色转换节点使用到的对象宏定义
 * 
 * @param node 颜色转换节点的node
 * @param inWidth 输入图像的宽度
 * @param inHeight 输入图像的高度
 * @param outFormat 输出图像的格式
 * @param bufNum    图像缓冲的个数
 * @param outFrames 输出图像缓冲个数
 */
typedef struct {
    //tiovx 相关
    vx_node node;   //颜色转换节点的node

#ifdef DEBUG_WITH_USB_CAMERA
    /*! Capture node graph parameter index */
    vx_int32 graph_parameter_index;
    /*! Capture node object array output */
    vx_object_array inImage_arr[APP_MODULES_MAX_BUFQ_DEPTH];
    vx_image input_images[APP_MODULES_MAX_BUFQ_DEPTH];    
#endif
    uint16_t inWidth;   //输入图像的宽度
    uint16_t inHeight;  //输入图像的高度
    vx_df_image outFormat;//输出图像的格式
    uint16_t bufNum;    //图像缓冲的个数
    vx_object_array outFrames[4];//输出图像缓冲个数
} ColorConvertObj;



/**
 * @brief 初始化颜色转换节点
 * 
 * @param context   颜色转换节点运行的上下文环境
 * @param colorConvertobj colorConver 对象指针
 * @return vx_status 初始化colorConver的状态
 */
vx_status app_init_colorConvert(vx_context context, ColorConvertObj *colorConvertobj);

/**
 * @brief 反向初始化
 * 
 * @param colorConvertobj colorConver 对象指针
 */
void app_deinit_colorConvert(ColorConvertObj *colorConvertobj);

/**
 * @brief 创建颜色转换节点
 * 
 * @param graph     颜色转换节点运行的graph
 * @param colorConvertobj colorConver 对象指针
 * @param inArray 需要转换图像的输入数组
 * @param outArray 完成转换以后的图像输出数组
 * @return vx_status 创建colorConver节点的状态
 */
vx_status app_create_colorConvert(vx_graph graph,
                                  ColorConvertObj *colorConvertobj,
                                  vx_object_array *inArray,
                                  vx_object_array *outArray);

/**
 * @brief 删除colorConver对象
 * 
 * @param colorConvertobj colorConver 对象指针
 */
void app_delete_colorConvert(ColorConvertObj *colorConvertobj);
#endif
