
#include "app_config_file.h"
#include <string.h>

vx_status app_init_config(ConfigFileObj *config)
{
    vx_status status = VX_SUCCESS;

    memset(config, 0, sizeof(ConfigFileObj)); //将配置文件清零初始化

    return status;
}

vx_status app_set_config_default(ConfigFileObj *configFileObj)
{
    vx_status status = VX_SUCCESS;

    configFileObj->sensorNum = 4; //默认相机数量为4个

    configFileObj->sensorOutputWidth = 1280; //摄像头输出图像宽度
    configFileObj->sensorOutputHeight = 720; //摄像头输出图像高度

    // Scaler 节点相关
    configFileObj->scalerOutputImageWidth = 1024; // Scaler节点输出图像宽度
    configFileObj->scalerOutputImageHight = 512;  // Scaler节点输出图像高度

    //显示相关  left disp paramaters
    configFileObj->displayLeftOption = 1;          //左侧显示选项
    configFileObj->displayLeftPipelineChannel = 0; //左侧显示流水线内图像通道
    configFileObj->displayLeftOutWidth = 1280;     //左侧显示图像输出宽度
    configFileObj->displayLeftOutHeight = 720;     //左侧显示图像输出高度
    configFileObj->displayLeftPosX = 0;            //左侧图像显示的坐标X
    configFileObj->displayLeftPosY = 0;            //左侧图像显示的坐标Y

    // #left crop paramaters
    configFileObj->displayLeftEnableCropping = 1; //左侧显示裁剪功能使能
    configFileObj->displayLeftCropStartX = 0;     //左侧图像裁剪开始坐标X
    configFileObj->displayLeftCropStartY = 0;     //左侧图像裁剪开始坐标Y
    configFileObj->displayLeftCropWidth = 640;    //左侧图像裁剪宽度
    configFileObj->displayLeftCropHeight = 480;   //左侧图像裁剪高度

    // #left disp move and zoom cmd step
    configFileObj->displayLeftMoveStep = 50; //右侧显示图像平移步长
    configFileObj->displayLeftZoomStep = 50; //右侧显示图像缩放步长

    //显示相关  right disp paramaters
    configFileObj->displayRightOption = 1;          //右侧显示选项
    configFileObj->displayRightPipelineChannel = 0; //右侧显示流水线内图像通道
    configFileObj->displayRightOutWidth = 1280;     //右侧显示图像输出宽度
    configFileObj->displayRightOutHeight = 720;     //右侧显示图像输出高度
    configFileObj->displayRightPosX = 0;            //右侧图像显示的坐标X
    configFileObj->displayRightPosY = 0;            //右侧图像显示的坐标Y

    // #Right crop paramaters
    configFileObj->displayRightEnableCropping = 1; //右侧显示裁剪功能使能
    configFileObj->displayRightCropStartX = 0;     //右侧图像裁剪开始坐标X
    configFileObj->displayRightCropStartY = 0;     //右侧图像裁剪开始坐标Y
    configFileObj->displayRightCropWidth = 640;    //右侧图像裁剪宽度
    configFileObj->displayRightCropHeight = 480;   //右侧图像裁剪高度

    // #Right disp move and zoom cmd step
    configFileObj->displayRightMoveStep = 50; //右侧显示图像平移步长
    configFileObj->displayRightZoomStep = 50; //右侧显示图像缩放步长

    configFileObj->exportGraphToDotOption = 1; //是否导出以供查看graph流程图

    configFileObj->printPerformance = 0; //默认不输出性能信息

    // deep learning related
    sprintf(configFileObj->config_file_path, "./");  //配置文件的默认路径位当前文件夹下
    sprintf(configFileObj->network_file_path, "./"); //网络文件默认路径位当前文件夹下

    configFileObj->num_classes = 80; //默认分类位80个分类
    configFileObj->viz_th = 0.95;    //默认的置信度为 95%

    return status;
}

vx_status app_read_config(ConfigFileObj *config, vx_char *cfg_file_name)
{
    vx_status status = VX_SUCCESS;

    //检测文件名是否为空
    if ((NULL != cfg_file_name))
    {
        FILE *fp;

        fp = fopen(cfg_file_name, "r"); //以只读方式打开文件，文件必须存在

        if (NULL == fp) //如果打开文件失败，则报错
        {
            printf("[Error] Unable to open file %s to config the paramaters!\n", cfg_file_name);
            status = VX_FAILURE;
        }

        char line_str[1024];
        char *token;

        while (fgets(line_str, sizeof(line_str), fp) != NULL)
        {
            char s[] = " \t";

            if (strchr(line_str, '#'))
            {
                continue;
            }

            /* get the first token */
            token = strtok(line_str, s);

            if (token != NULL)
            {
                if (strcmp(token, "sensorNum") == 0) //摄像头数量
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->sensorNum = atoi(token);
                        if (config->sensorNum > SENSOR_MAX_NUM)
                            config->sensorNum = 0;
                        APP_PRINTF("[Config file Read infor]    Config sensor num = %d  !\n", config->sensorNum);
                    }
                }

                else if (strcmp(token, "sensorOutputWidth") == 0) //摄像头输出图像宽度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->sensorOutputWidth = atoi(token);
                        if (config->sensorOutputWidth > SENSOR_OUTPUT_WIDTH_MAX)
                            config->sensorOutputWidth = 1280;
                        APP_PRINTF("[Config file Read infor]    Config sensor Output Width = %d !\n", config->sensorOutputWidth);
                    }
                }

                else if (strcmp(token, "sensorOutputHeight") == 0) //摄像头输出图像高度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->sensorOutputHeight = atoi(token);
                        if (config->sensorOutputHeight > SENSOR_OUTPUT_HEIGHT_MAX)
                            config->sensorOutputHeight = 720;
                        APP_PRINTF("[Config file Read infor]    Config sensor Output Height = %d    !\n", config->sensorOutputHeight);
                    }
                }

                else if (strcmp(token, "scalerOutputImageWidth") == 0) // Scaler节点输出图像宽度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->scalerOutputImageWidth = atoi(token);
                        if (config->scalerOutputImageWidth > SENSOR_OUTPUT_WIDTH_MAX)
                            config->scalerOutputImageWidth = 1080;
                        APP_PRINTF("[Config file Read infor]    Config scalerOutputImageWidth = %d  !\n", config->scalerOutputImageWidth);
                    }
                }

                else if (strcmp(token, "scalerOutputImageHight") == 0) // Scaler节点输出图像高度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->scalerOutputImageHight = atoi(token);
                        if (config->scalerOutputImageHight > SENSOR_OUTPUT_HEIGHT_MAX)
                            config->scalerOutputImageHight = 720;
                        APP_PRINTF("[Config file Read infor]    Config scalerOutputImageHight = %d  !\n", config->scalerOutputImageHight);
                    }
                }

                else if (strcmp(token, "displayLeftOption") == 0) //左侧显示选项
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayLeftOption = atoi(token);
                        if (config->displayLeftOption > 1)
                            config->displayLeftOption = 1;
                        APP_PRINTF("[Config file Read infor]    Config displayLeftOption = %d   !\n", config->displayLeftOption);
                    }
                }

                else if (strcmp(token, "displayLeftPipelineChannel") == 0) //左侧显示流水线内图像通道
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayLeftPipelineChannel = atoi(token);
                        if (config->displayLeftPipelineChannel > 7)
                            config->displayLeftPipelineChannel = 0;
                        APP_PRINTF("[Config file Read infor]    Config displayLeftPipelineChannel = %d  !\n", config->displayLeftPipelineChannel);
                    }
                }

                else if (strcmp(token, "displayLeftOutWidth") == 0) //左侧显示图像输出宽度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayLeftOutWidth = atoi(token);
                        if (config->displayLeftOutWidth > 1920)
                            config->displayLeftOutWidth = 1280;
                        APP_PRINTF("[Config file Read infor]    Config displayLeftOutWidth = %d !\n", config->displayLeftOutWidth);
                    }
                }

                else if (strcmp(token, "displayLeftOutHeight") == 0) //左侧显示图像输出高度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayLeftOutHeight = atoi(token);
                        if (config->displayLeftOutHeight > 1080)
                            config->displayLeftOutHeight = 720;
                        APP_PRINTF("[Config file Read infor]    Config displayLeftOutHeight = %d    !\n", config->displayLeftOutHeight);
                    }
                }

                else if (strcmp(token, "displayLeftPosX") == 0) //左侧图像显示的坐标X
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayLeftPosX = atoi(token);
                        if (config->displayLeftPosX > 1920)
                            config->displayLeftPosX = 0;
                        APP_PRINTF("[Config file Read infor]    Config displayLeftPosX = %d !\n", config->displayLeftPosX);
                    }
                }

                else if (strcmp(token, "displayLeftPosY") == 0) //左侧图像显示的坐标Y
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayLeftPosY = atoi(token);
                        if (config->displayLeftPosY > 1080)
                            config->displayLeftPosY = 0;
                        APP_PRINTF("[Config file Read infor]    Config displayLeftPosY = %d !\n", config->displayLeftPosY);
                    }
                }

                else if (strcmp(token, "displayLeftEnableCropping") == 0) //左侧显示裁剪功能使能
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayLeftEnableCropping = atoi(token);
                        if (config->displayLeftEnableCropping > 1)
                            config->displayLeftEnableCropping = 1;
                        APP_PRINTF("[Config file Read infor]    Config displayLeftEnableCropping = %d   !\n", config->displayLeftEnableCropping);
                    }
                }

                else if (strcmp(token, "displayLeftCropStartX") == 0) //左侧图像裁剪开始坐标X
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayLeftCropStartX = atoi(token);
                        if (config->displayLeftCropStartX > 1920)
                            config->displayLeftCropStartX = 0;
                        APP_PRINTF("[Config file Read infor]    Config displayLeftCropStartX = %d   !\n", config->displayLeftCropStartX);
                    }
                }

                else if (strcmp(token, "displayLeftCropStartY") == 0) //左侧图像裁剪开始坐标Y
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayLeftCropStartY = atoi(token);
                        if (config->displayLeftCropStartY > 1080)
                            config->displayLeftCropStartY = 0;
                        APP_PRINTF("[Config file Read infor]    Config displayLeftCropStartY = %d   !\n", config->displayLeftCropStartY);
                    }
                }

                else if (strcmp(token, "displayLeftCropWidth") == 0) //左侧图像裁剪宽度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayLeftCropWidth = atoi(token);
                        if (config->displayLeftCropWidth > 1920)
                            config->displayLeftCropWidth = 1280;
                        APP_PRINTF("[Config file Read infor]    Config displayLeftCropWidth = %d    !\n", config->displayLeftCropWidth);
                    }
                }

                else if (strcmp(token, "displayLeftCropHeight") == 0) //左侧图像裁剪高度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayLeftCropHeight = atoi(token);
                        if (config->displayLeftCropHeight > 1080)
                            config->displayLeftCropHeight = 720;
                        APP_PRINTF("[Config file Read infor]    Config displayLeftCropHeight = %d   !\n", config->displayLeftCropHeight);
                    }
                }

                //右侧屏幕相关配置参数
                else if (strcmp(token, "displayRightOption") == 0) //右侧显示选项
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayRightOption = atoi(token);
                        if (config->displayRightOption > 1)
                            config->displayRightOption = 1;
                        APP_PRINTF("[Config file Read infor]    Config displayRightOption = %d  !\n", config->displayRightOption);
                    }
                }

                else if (strcmp(token, "displayRightPipelineChannel") == 0) //右侧显示流水线内图像通道
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayRightPipelineChannel = atoi(token);
                        if (config->displayRightPipelineChannel > 7)
                            config->displayRightPipelineChannel = 0;
                        APP_PRINTF("[Config file Read infor]    Config displayRightPipelineChannel = %d !\n", config->displayRightPipelineChannel);
                    }
                }

                else if (strcmp(token, "displayRightOutWidth") == 0) //右侧显示图像输出宽度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayRightOutWidth = atoi(token);
                        if (config->displayRightOutWidth > 1920)
                            config->displayRightOutWidth = 1280;
                        APP_PRINTF("[Config file Read infor]    Config displayRightOutWidth = %d    !\n", config->displayRightOutWidth);
                    }
                }

                else if (strcmp(token, "displayRightOutHeight") == 0) //右侧显示图像输出高度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayRightOutHeight = atoi(token);
                        if (config->displayRightOutHeight > 1080)
                            config->displayRightOutHeight = 720;
                        APP_PRINTF("[Config file Read infor]    Config displayRightOutHeight = %d   !\n", config->displayRightOutHeight);
                    }
                }

                else if (strcmp(token, "displayRightPosX") == 0) //右侧图像显示的坐标X
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayRightPosX = atoi(token);
                        if (config->displayRightPosX > 1920)
                            config->displayRightPosX = 0;
                        APP_PRINTF("[Config file Read infor]    Config displayRightPosX = %d    !\n", config->displayRightPosX);
                    }
                }

                else if (strcmp(token, "displayRightPosY") == 0) //右侧图像显示的坐标Y
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayRightPosY = atoi(token);
                        if (config->displayRightPosY > 1080)
                            config->displayRightPosY = 0;
                        APP_PRINTF("[Config file Read infor]    Config displayRightPosY = %d    !\n", config->displayRightPosY);
                    }
                }

                else if (strcmp(token, "displayRightEnableCropping") == 0) //右侧显示裁剪功能使能
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayRightEnableCropping = atoi(token);
                        if (config->displayRightEnableCropping > 1)
                            config->displayRightEnableCropping = 1;
                        APP_PRINTF("[Config file Read infor]    Config displayRightEnableCropping = %d  !\n", config->displayRightEnableCropping);
                    }
                }

                else if (strcmp(token, "displayRightCropStartX") == 0) //右侧图像裁剪开始坐标X
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayRightCropStartX = atoi(token);
                        if (config->displayRightCropStartX > 1920)
                            config->displayRightCropStartX = 0;
                        APP_PRINTF("[Config file Read infor]    Config displayRightCropStartX = %d  !\n", config->displayRightCropStartX);
                    }
                }

                else if (strcmp(token, "displayRightCropStartY") == 0) //右侧图像裁剪开始坐标Y
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayRightCropStartY = atoi(token);
                        if (config->displayRightCropStartY > 1080)
                            config->displayRightCropStartY = 0;
                        APP_PRINTF("[Config file Read infor]    Config displayRightCropStartY = %d  !\n", config->displayRightCropStartY);
                    }
                }

                else if (strcmp(token, "displayRightCropWidth") == 0) //右侧图像裁剪宽度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayRightCropWidth = atoi(token);
                        if (config->displayRightCropWidth > 1920)
                            config->displayRightCropWidth = 1280;
                        APP_PRINTF("[Config file Read infor]    Config displayRightCropWidth = %d   !\n", config->displayRightCropWidth);
                    }
                }

                else if (strcmp(token, "displayRightCropHeight") == 0) //右侧图像裁剪高度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->displayRightCropHeight = atoi(token);
                        if (config->displayRightCropHeight > 1080)
                            config->displayRightCropHeight = 720;
                        APP_PRINTF("[Config file Read infor]    Config displayRightCropHeight = %d  !\n", config->displayRightCropHeight);
                    }
                }

                else if (strcmp(token, "exportGraphToDotOption") == 0) //右侧图像裁剪高度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->exportGraphToDotOption = atoi(token);
                        if (config->exportGraphToDotOption > 1)
                            config->exportGraphToDotOption = 1;
                        APP_PRINTF("[Config file Read infor]    Config exportGraphToDotOption = %d  !\n", config->exportGraphToDotOption);
                    }
                }

                else if (strcmp(token, "printPerformance") == 0) //右侧图像裁剪高度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->printPerformance = atoi(token);
                        if (config->printPerformance > 1)
                            config->printPerformance = 1;
                        APP_PRINTF("[Config file Read infor]    Config printPerformance = %d  !\n", config->printPerformance);
                    }
                }

                else if (strcmp(token, "config_file_path") == 0) //深度学习网络配置文件路径
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        strcpy(config->config_file_path, token);
                    }
                    APP_PRINTF("[Config file Read infor]    Config config_file_path = %s  !\n", config->config_file_path);
                }
                else if (strcmp(token, "network_file_path") == 0) //深度学习网络模型文件路径
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        strcpy(config->network_file_path, token);
                    }
                    APP_PRINTF("[Config file Read infor]    Config network_file_path = %s  !\n", config->network_file_path);
                }
                else if (strcmp(token, "num_classes") == 0) //深度学习种类总数
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->num_classes = atoi(token);
                        if (config->num_classes > 1024)
                            config->num_classes = 1024;
                        APP_PRINTF("[Config file Read infor]    Config num_classes = %d  !\n", config->num_classes);
                    }
                }
                else if (strcmp(token, "viz_th") == 0) //深度学习目标识别的置信度
                {
                    token = strtok(NULL, s);
                    if (token != NULL)
                    {
                        token[strlen(token) - 1] = 0;
                        config->viz_th = atof(token);
                        if (config->viz_th >= 1.0)
                            config->viz_th = 0.8;
                        //格式化输出，保留三位有效小数
                        APP_PRINTF("[Config file Read infor]    Config viz_th = %.3lf  !\n", config->viz_th);
                    }
                }
            }
        }
        fclose(fp); //关闭文件
    }
    else
    {
        printf("[Error Config file] Config file path or name is NULL!\n");
    }
    return status;
}

void app_show_usage(vx_int32 argc, vx_char *argv[])
{
    printf("\n");
    printf(" SAIC CMS Demo - (c) SAIC MAXUS 2022\n");
    printf(" ========================================================\n");
    printf("\n");
    printf(" Usage,\n");
    printf("  %s --cfg <config file>\n", argv[0]);
    printf("\n");
}

vx_status app_parse_cmd_line_args(ConfigFileObj *config, vx_int32 argc, vx_char *argv[])
{
    vx_status status = VX_SUCCESS;
    vx_int32 i;

    if (VX_SUCCESS == status)
    {
        APP_PRINTF("[Config file Init infor]    Config Init paramaters starting......!\n");
        status = app_init_config(config);
        APP_PRINTF("[Config file Init infor]    Config Init paramaters done !\n\n");
    }

    //设置默认配置参数，防止没有读取到数据，或者配置文件中参数为空
    if (VX_SUCCESS == status)
    {
        APP_PRINTF("[Config file default infor]    Config default paramaters starting......!\n");
        status = app_set_config_default(config); //防止配置文件内没有读取到数据，设置一个默认值
        APP_PRINTF("[Config file default infor]    Config default paramaters done !\n\n");
    }

    if (argc == 1) //如果参数不足，则显示教程信息
    {
        app_show_usage(argc, argv);
        exit(0);
    }

    for (i = 0; i < argc; i++) //输入多个参数
    {
        if (strcmp(argv[i], "--cfg") == 0)
        {
            i++;
            if (i >= argc)
            {
                app_show_usage(argc, argv);
            }
            APP_PRINTF("[Config file Read infor]    Reading config file………\n");

            status = app_read_config(config, argv[i]); //读取配置文件

            APP_PRINTF("[Config file Read infor]    Read config file done !\n\n");
        }
        else if (strcmp(argv[i], "--help") == 0)
        {
            app_show_usage(argc, argv);
            exit(0);
        }
    }
    return status;
}