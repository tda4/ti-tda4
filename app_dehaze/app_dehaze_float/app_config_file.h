
#ifndef _APP_READ_CONFIG_FILE_H
#define _APP_READ_CONFIG_FILE_H

#include "app_common.h"

/**
 * @brief
 *
 */
typedef struct
{

  // Sensor 相关
  vx_uint32 sensorNum;          //摄像头数量
  vx_uint32 sensorOutputWidth;  //摄像头输出图像宽度
  vx_uint32 sensorOutputHeight; //摄像头输出图像高度

  // Scaler 节点相关
  vx_uint32 scalerOutputImageWidth; // Scaler节点输出图像宽度
  vx_uint32 scalerOutputImageHight; // Scaler节点输出图像高度

  //显示相关  left disp paramaters
  vx_uint32 displayLeftOption;          //左侧显示选项
  vx_uint32 displayLeftPipelineChannel; //左侧显示流水线内图像通道
  vx_uint32 displayLeftOutWidth;        //左侧显示图像输出宽度
  vx_uint32 displayLeftOutHeight;       //左侧显示图像输出高度
  vx_uint32 displayLeftPosX;            //左侧图像显示的坐标X
  vx_uint32 displayLeftPosY;            //左侧图像显示的坐标Y

  // #left crop paramaters
  vx_uint32 displayLeftEnableCropping; //左侧显示裁剪功能使能
  vx_uint32 displayLeftCropStartX;     //左侧图像裁剪开始坐标X
  vx_uint32 displayLeftCropStartY;     //左侧图像裁剪开始坐标Y
  vx_uint32 displayLeftCropWidth;      //左侧图像裁剪宽度
  vx_uint32 displayLeftCropHeight;     //左侧图像裁剪高度

  // #left disp move and zoom cmd step
  vx_uint32 displayLeftMoveStep; //右侧显示图像平移步长
  vx_uint32 displayLeftZoomStep; //右侧显示图像缩放步长

  //显示相关  right disp paramaters
  vx_uint32 displayRightOption;          //右侧显示选项
  vx_uint32 displayRightPipelineChannel; //右侧显示流水线内图像通道
  vx_uint32 displayRightOutWidth;        //右侧显示图像输出宽度
  vx_uint32 displayRightOutHeight;       //右侧显示图像输出高度
  vx_uint32 displayRightPosX;            //右侧图像显示的坐标X
  vx_uint32 displayRightPosY;            //右侧图像显示的坐标Y

  // #Right crop paramaters
  vx_uint32 displayRightEnableCropping; //右侧显示裁剪功能使能
  vx_uint32 displayRightCropStartX;     //右侧图像裁剪开始坐标X
  vx_uint32 displayRightCropStartY;     //右侧图像裁剪开始坐标Y
  vx_uint32 displayRightCropWidth;      //右侧图像裁剪宽度
  vx_uint32 displayRightCropHeight;     //右侧图像裁剪高度

  // #Right disp move and zoom cmd step
  vx_uint32 displayRightMoveStep; //右侧显示图像平移步长
  vx_uint32 displayRightZoomStep; //右侧显示图像缩放步长

  vx_uint32 exportGraphToDotOption; //是否导出graph分析的内容

  vx_char printPerformance; //输出性能信息

  // TIDL相关 深度学习相关配置
  vx_char config_file_path[APP_MODULES_MAX_FILE_PATH_SIZE];  //深度学习网络配置文件路径
  vx_char network_file_path[APP_MODULES_MAX_FILE_PATH_SIZE]; //深度学习网络模型文件路径
  vx_int16 num_classes;                                      //目标识别的种类
  vx_float32 viz_th;                                         //置信度

} ConfigFileObj;

/**
 * @brief 初始化配置读取文件的对象
 *
 * @param config 配置文件对象指针
 * @return vx_status 初始化返回状态
 */
vx_status app_init_config(ConfigFileObj *config);

/**
 * @brief 设置初始化配置的各个参数值
 *
 * @param config 配置文件对象指针
 * @return vx_status 设置默认值的状态
 */
vx_status app_set_config_default(ConfigFileObj *config);

/**
 * @brief 从文件内读取配置数据
 *
 * @param config 配置文件对象指针
 * @param cfg_file_name 读取文件的文件名
 * @return vx_status 读取配置文件的返回状态
 */
vx_status app_read_config(ConfigFileObj *config, vx_char *cfg_file_name);

/**
 * @brief 从命令行获取参数，并对配置相关参数进行读取和初始化
 *
 * @param obj 参数指针
 * @param argc 外部参数数量
 * @param argv 外部参数列表
 * @return vx_status 返回状态
 */
vx_status app_parse_cmd_line_args(ConfigFileObj *config, vx_int32 argc, vx_char *argv[]);

/**
 * @brief 显示使用教程函数
 *
 * @param argc 外部参数数量
 * @param argv 外部参数列表
 */
void app_show_usage(vx_int32 argc, vx_char *argv[]);

#endif
