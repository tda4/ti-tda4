
#include "app_dehaze_float_module.h"

vx_status app_init_dehaze_float(vx_context context, DehazeFloatObj *dehazeFloatObj, char *objName)
{
  vx_status status = VX_SUCCESS;

  vx_image image = vxCreateImage(context,
                                 dehazeFloatObj->config.imageWidth,
                                 dehazeFloatObj->config.imageHeight,
                                 VX_DF_IMAGE_RGB);

  vx_image imageU8 = vxCreateImage(context,
                                   dehazeFloatObj->config.imageWidth,
                                   dehazeFloatObj->config.imageHeight,
                                   VX_DF_IMAGE_U8);
  //创建矩阵，第二个参数表示矩阵的形状
  vx_matrix mask = vxCreateMatrixFromPattern(context, VX_PATTERN_BOX, 15, 15);

  vx_matrix matrix = vxCreateMatrix(context, VX_TYPE_FLOAT32, dehazeFloatObj->config.imageWidth, dehazeFloatObj->config.imageHeight);

  vx_float32 atom;
  vx_scalar scalar = vxCreateScalar(context, (vx_enum)VX_TYPE_FLOAT32, &atom);

  uint16_t i = 0;
  for (i = 0; i < dehazeFloatObj->config.cameraNum; i++)
  {
    //通道分离相关初始化 R
    dehazeFloatObj->channelR[i] = vxCreateObjectArray(context,
                                                      (vx_reference)imageU8,
                                                      dehazeFloatObj->config.cameraNum);
    //通道分离相关初始化 G
    dehazeFloatObj->channelG[i] = vxCreateObjectArray(context,
                                                      (vx_reference)imageU8,
                                                      dehazeFloatObj->config.cameraNum);
    //通道分离相关初始化 B
    dehazeFloatObj->channelB[i] = vxCreateObjectArray(context,
                                                      (vx_reference)imageU8,
                                                      dehazeFloatObj->config.cameraNum);

    dehazeFloatObj->gray[i] = vxCreateObjectArray(context,
                                                      (vx_reference)imageU8,
                                                      dehazeFloatObj->config.cameraNum);

    dehazeFloatObj->trans[i] = vxCreateObjectArray(context,
                                                  (vx_reference)imageU8,
                                                  dehazeFloatObj->config.cameraNum);

    //通道分离相关初始化 灰度图
    dehazeFloatObj->matrixGray[i] = vxCreateObjectArray(context,
                                                        (vx_reference)matrix,
                                                        dehazeFloatObj->config.cameraNum);

    dehazeFloatObj->maskArray[i] = vxCreateObjectArray(context, (vx_reference)mask, 2);

    dehazeFloatObj->matrixTrans[i] = vxCreateObjectArray(context,
                                                        (vx_reference)matrix,
                                                        dehazeFloatObj->config.cameraNum);
    dehazeFloatObj->matrixguideTrans[i] = vxCreateObjectArray(context,
                                                              (vx_reference)matrix,
                                                              dehazeFloatObj->config.cameraNum);

    dehazeFloatObj->out[i] = vxCreateObjectArray(context,
                                                 (vx_reference)image,
                                                 dehazeFloatObj->config.cameraNum);

    dehazeFloatObj->atomScalar[i] = vxCreateObjectArray(context, (vx_reference)scalar, dehazeFloatObj->config.cameraNum);
  }

  dehazeFloatObj->getAtomParamsObj = vxCreateUserDataObject(context, "tivxDehazeGetAtmoFloatParams", sizeof(tivxDehazeGetAtmoFloatParams), &dehazeFloatObj->getAtomParams);
  status = vxGetStatus((vx_reference)dehazeFloatObj->getAtomParamsObj);

  dehazeFloatObj->getDarkAndTransParamsObj = vxCreateUserDataObject(context, "tivxDehazeDarkandtransFloatParams", sizeof(tivxDehazeDarkandtransFloatParams), &dehazeFloatObj->getDarkAndTransParams);
  status = vxGetStatus((vx_reference)dehazeFloatObj->getDarkAndTransParamsObj);

  dehazeFloatObj->guideFilterParamsObj = vxCreateUserDataObject(context, "tivxDehazeGetGuidefilterFloatParams", sizeof(tivxDehazeGetGuidefilterFloatParams), &dehazeFloatObj->guideFilterParams);
  status = vxGetStatus((vx_reference)dehazeFloatObj->guideFilterParamsObj);

  vxReleaseImage(&image);
  vxReleaseImage(&imageU8);
  vxReleaseMatrix(&matrix);
  vxReleaseMatrix(&mask);
  return status;
}

void app_deinit_dehaze_float(DehazeFloatObj *dehazeFloatObj)
{
 
}

void app_delete_dehaze_float(DehazeFloatObj *dehazeFloatObj)
{

}

vx_status app_create_graph_dehaze_float(vx_graph graph, DehazeFloatObj *dehazeFloatObj, vx_object_array *inArray, vx_object_array *outArray)
{
  vx_status status = VX_SUCCESS;



  vx_image inImage = (vx_image)vxGetObjectArrayItem(inArray[0], 0);

  //通道分离相关
  vx_image channelRu8 = (vx_image)vxGetObjectArrayItem(dehazeFloatObj->channelR[0], 0);
  vx_image channelGu8 = (vx_image)vxGetObjectArrayItem(dehazeFloatObj->channelG[0], 0);
  vx_image channelBu8 = (vx_image)vxGetObjectArrayItem(dehazeFloatObj->channelB[0], 0);
  vx_matrix matrixGray = (vx_matrix)vxGetObjectArrayItem(dehazeFloatObj->matrixGray[0], 0);

    dehazeFloatObj->rgbChannelExtractNode = tivxDehazeRgbChannelsExtractFloatNode(graph, inImage, channelRu8, channelGu8, channelBu8, matrixGray);
    status = (vx_status)vxGetStatus((vx_reference)dehazeFloatObj->rgbChannelExtractNode);
    if (VX_SUCCESS == status)
    {
      status = vxSetNodeTarget(dehazeFloatObj->rgbChannelExtractNode, //设置目标target为DSP1
                              VX_TARGET_STRING,
                              TIVX_TARGET_DSP2);
    }
    // if (VX_SUCCESS == status)
    // {
    //   vx_bool replicate[] = {vx_true_e, vx_true_e, vx_true_e, vx_true_e, vx_true_e};
    //   status = vxReplicateNode(graph, dehazeFloatObj->rgbChannelExtractNode, replicate, 5);
    // }

    //获取大气光值
    vx_scalar atom = (vx_scalar)vxGetObjectArrayItem(dehazeFloatObj->atomScalar[0], 0);
    dehazeFloatObj->getAtomNode = tivxDehazeGetAtmoFloatNode(graph, channelRu8, channelGu8, channelBu8, dehazeFloatObj->getAtomParamsObj, atom);
    status = (vx_status)vxGetStatus((vx_reference)dehazeFloatObj->getAtomNode);
    if (VX_SUCCESS == status)
    {
      status = vxSetNodeTarget(dehazeFloatObj->getAtomNode, //设置目标target为DSP1
                               VX_TARGET_STRING,
                               TIVX_TARGET_DSP1);
    }
    // if (VX_SUCCESS == status)
    // {
    //   vx_bool replicate[] = {vx_true_e, vx_true_e, vx_true_e, vx_false_e, vx_true_e};
    //   status = vxReplicateNode(graph, dehazeFloatObj->getAtomNode, replicate, 5);
    // }
    //获取透射图相关
    //腐蚀图像
    vx_matrix mask = (vx_matrix)vxGetObjectArrayItem(dehazeFloatObj->maskArray[0], 0);
    vx_matrix matrixTrans = (vx_matrix)vxGetObjectArrayItem(dehazeFloatObj->matrixTrans[0], 0);
    dehazeFloatObj->getTransNode = tivxDehazeDarkandtransFloatNode(graph,
                                                                   channelRu8,
                                                                   channelGu8,
                                                                   channelBu8,
                                                                   atom,
                                                                   mask,
                                                                   dehazeFloatObj->getDarkAndTransParamsObj,
                                                                   matrixTrans);

    status = (vx_status)vxGetStatus((vx_reference)dehazeFloatObj->getTransNode);
    if (VX_SUCCESS == status)
    {
      status = vxSetNodeTarget(dehazeFloatObj->getTransNode, //设置目标target为DSP1
                               VX_TARGET_STRING,
                               TIVX_TARGET_DSP2);
    }
    // if (VX_SUCCESS == status)
    // {
    //   vx_bool replicate[] = {vx_true_e, vx_true_e, vx_true_e, vx_true_e,vx_false_e,vx_false_e,vx_true_e};
    //   status = vxReplicateNode(graph, dehazeFloatObj->getTransNode, replicate, 7);
    // }



    
    vx_matrix matrixguideTrans = (vx_matrix)vxGetObjectArrayItem(dehazeFloatObj->matrixguideTrans[0], 0);
    dehazeFloatObj->guideFilterNode = tivxDehazeGetGuidefilterFloatNode(graph,
                                                                        matrixTrans,
                                                                        matrixGray,
                                                                        dehazeFloatObj->guideFilterParamsObj,
                                                                        matrixguideTrans);
    status = (vx_status)vxGetStatus((vx_reference)dehazeFloatObj->guideFilterNode);
    if (VX_SUCCESS == status)
    {
      status = vxSetNodeTarget(dehazeFloatObj->guideFilterNode, //设置目标target为DSP1
                               VX_TARGET_STRING,
                               TIVX_TARGET_DSP1);
    }
    // if (VX_SUCCESS == status)
    // {
    //   vx_bool replicate[] = {vx_true_e, vx_true_e, vx_false_e, vx_true_e};
    //   status = vxReplicateNode(graph, dehazeFloatObj->guideFilterNode, replicate, 4);
    // }


    vx_image out = (vx_image)vxGetObjectArrayItem(dehazeFloatObj->out[0], 0);
    dehazeFloatObj->combineNode = tivxDehazeChannelsCombineFloatNode(graph,
                                                                     channelRu8,
                                                                     channelGu8,
                                                                     channelBu8,
                                                                     matrixguideTrans,
                                                                     atom, 
                                                                     out);

    status = (vx_status)vxGetStatus((vx_reference)dehazeFloatObj->combineNode);
    if (VX_SUCCESS == status)
    {
      status = vxSetNodeTarget(dehazeFloatObj->combineNode, //设置目标target为DSP1
                               VX_TARGET_STRING,
                               TIVX_TARGET_DSP1);
    }
    // if (VX_SUCCESS == status)
    // {
    //   vx_bool replicate[] = {vx_true_e, vx_true_e, vx_true_e, vx_true_e,vx_true_e,vx_true_e};
    //   status = vxReplicateNode(graph, dehazeFloatObj->combineNode, replicate, 6);
    // }





    // vx_image gray = (vx_image)vxGetObjectArrayItem(dehazeFloatObj->gray[0], 0);
    // vx_image trans = (vx_image)vxGetObjectArrayItem(dehazeFloatObj->trans[0], 0);

    // dehazeFloatObj->dispTestNode = tivxDehazeDisptestFloatNode(graph, matrixguideTrans, matrixGray, trans, gray);
    // status = (vx_status)vxGetStatus((vx_reference)dehazeFloatObj->dispTestNode);
    // if (VX_SUCCESS == status)
    // {
    //   status = vxSetNodeTarget(dehazeFloatObj->dispTestNode, //设置目标target为DSP1
    //                            VX_TARGET_STRING,
    //                            TIVX_TARGET_DSP2);
    // }
    // if (VX_SUCCESS == status)
    // {
    //   vx_bool replicate[] = {vx_true_e, vx_true_e, vx_true_e, vx_true_e};
    //   status = vxReplicateNode(graph, dehazeFloatObj->dispTestNode, replicate, 4);
    // }


    //vxReleaseMatrix(&matrixguideTrans);
    //vxReleaseImage(&gray);
    vxReleaseMatrix(&matrixTrans);
    vxReleaseMatrix(&mask);
    vxReleaseScalar(&atom);
    vxReleaseMatrix(&matrixGray);
    vxReleaseImage(&channelRu8);
    vxReleaseImage(&channelGu8);
    vxReleaseImage(&channelBu8);
    vxReleaseImage(&inImage);
    return status;
}
