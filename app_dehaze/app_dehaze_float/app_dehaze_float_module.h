
#ifndef _APP_DEHAZE_FLOAT_MODULE
#define _APP_DEHAZE_FLOAT_MODULE

#include "app_common.h"

typedef struct
{
  vx_char cameraNum;
  vx_int32 imageWidth, imageHeight;
} DehazeFloatConfig;



typedef struct
{
  DehazeFloatConfig config;

  //通道分离相关
  vx_node rgbChannelExtractNode;
  vx_object_array channelR[4], channelG[4], channelB[4];
  vx_object_array matrixGray[4];

  //获取大气光值相关
  vx_node getAtomNode;
  vx_user_data_object getAtomParamsObj;      //参数对象实例
  tivxDehazeGetAtmoFloatParams getAtomParams; // dehaze 参数
  vx_object_array atomScalar[4];

//获取透射图相关
  vx_node getTransNode;
  vx_user_data_object getDarkAndTransParamsObj;                 //参数对象实例
  tivxDehazeDarkandtransFloatParams getDarkAndTransParams;      // dehaze 参数
  vx_object_array matrixTrans[4];
  vx_object_array maskArray[4];


//测试相关
  vx_node dispTestNode;
  vx_object_array gray[4];
  vx_object_array trans[4];

//导向滤波
  vx_node guideFilterNode;  
  vx_user_data_object guideFilterParamsObj;                 //参数对象实例
  tivxDehazeGetGuidefilterFloatParams guideFilterParams;      // dehaze 参数
  vx_object_array matrixguideTrans[4];

//图像合并
  vx_node combineNode;  




  //最终除雾的输出
  vx_object_array out[4];
  vx_reference refs[1]; // Pipeline运行起来以后可以向node发送参数的索引
} DehazeFloatObj;

/**
 * @brief 初始化dehaze节点
 *
 * @param context dehaze节点运行的上下文环境
 * @param dehazeFloatObj dehaze节点对象指针
 * @param objName 空
 * @return vx_status
 */
vx_status app_init_dehaze_float(vx_context context, DehazeFloatObj *dehazeFloatObj, char *objName);

/**
 * @brief 反向初始化dehaze节点
 *
 * @param dehazeFloatObj dehaze节点对象指针
 */
void app_deinit_dehaze_float(DehazeFloatObj *dehazeFloatObj);

/**
 * @brief 删除dehaze节点
 *
 * @param dehazeFloatObj dehaze节点对象指针
 */
void app_delete_dehaze_float(DehazeFloatObj *dehazeFloatObj);

/**
 * @brief 创建dehaze节点
 *
 * @param graph  Display节点运行的graph
 * @param dehazeFloatObj dehaze节点对象指针
 * @param disp_image 需要显示的图像
 * @return vx_status 创建dehaze节点的状态
 */
vx_status app_create_graph_dehaze_float(vx_graph graph, DehazeFloatObj *dehazeFloatObj, vx_object_array *inArray, vx_object_array *outArray);

#endif
