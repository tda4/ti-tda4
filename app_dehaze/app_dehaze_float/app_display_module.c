
#include "app_display_module.h"
#define DISPLAY_WIDTH_OUT 1920  // 1920(1920*2)//
#define DISPLAY_HEIGHT_OUT 1080 // 1080

vx_status app_init_display(vx_context context, DisplayObj *displayObj, char *objName)
{
  vx_status status = VX_SUCCESS;

  if ((vx_true_e == tivxIsTargetEnabled(displayObj->targetName)) && (displayObj->display_option == 1))
  {
    status = VX_SUCCESS;
  }
  else
  {
    status = VX_FAILURE;
  }

  if (VX_SUCCESS == status)
  {
    displayObj->disp_params_obj = vxCreateUserDataObject(context, "tivx_display_params_t", sizeof(tivx_display_params_t), &displayObj->disp_params);
    status = vxGetStatus((vx_reference)displayObj->disp_params_obj);
  }

  return status;
}

void app_deinit_display(DisplayObj *displayObj)
{
  if ((vx_true_e == tivxIsTargetEnabled(displayObj->targetName)) && (displayObj->display_option == 1))
  {
    vxReleaseUserDataObject(&displayObj->disp_params_obj);
  }
}

void app_delete_display(DisplayObj *displayObj)
{
  if (displayObj->disp_node != NULL)
  {
    vxReleaseNode(&displayObj->disp_node);
  }
}

vx_status app_create_graph_display(vx_graph graph, DisplayObj *displayObj, vx_image disp_image)
{
  vx_status status = VX_SUCCESS;

  if ((vx_true_e == tivxIsTargetEnabled(displayObj->targetName)) && (displayObj->display_option == 1))
  {
    displayObj->disp_node = tivxDisplayNode(graph, displayObj->disp_params_obj, disp_image);
    vxSetNodeTarget(displayObj->disp_node, VX_TARGET_STRING, displayObj->targetName);
    vxSetReferenceName((vx_reference)displayObj->disp_node, "display_node");
    status = vxGetStatus((vx_reference)displayObj->disp_node);
  }
  return status;
}
