
#ifndef _APP_DISPLAY_MODULE
#define _APP_DISPLAY_MODULE

#include "app_common.h"

typedef struct
{
  vx_node disp_node;                   //显示节点
  vx_user_data_object disp_params_obj; //参数对象实例
  tivx_display_params_t disp_params;   // display 参数

  vx_int32 display_option; //显示开启控制位

  vx_int32 graph_parameter_index; // pipeline参数索引

  vx_char objName[APP_MAX_FILE_PATH];

  vx_char targetName[APP_MAX_FILE_PATH]; //节点运行的目标核名称 DSP1或者DSP2

  vx_reference refs[1]; // Pipeline运行起来以后可以向node发送参数的索引
} DisplayObj;

/**
 * @brief 初始化display节点
 *
 * @param context display节点运行的上下文环境
 * @param displayObj display节点对象指针
 * @param objName 空
 * @return vx_status
 */
vx_status app_init_display(vx_context context, DisplayObj *displayObj, char *objName);

/**
 * @brief 反向初始化display节点
 *
 * @param displayObj display节点对象指针
 */
void app_deinit_display(DisplayObj *displayObj);

/**
 * @brief 删除display节点
 *
 * @param displayObj display节点对象指针
 */
void app_delete_display(DisplayObj *displayObj);

/**
 * @brief 创建display节点
 *
 * @param graph  Display节点运行的graph
 * @param displayObj display节点对象指针
 * @param disp_image 需要显示的图像
 * @return vx_status 创建display节点的状态
 */
vx_status app_create_graph_display(vx_graph graph, DisplayObj *displayObj, vx_image disp_image);

#endif
