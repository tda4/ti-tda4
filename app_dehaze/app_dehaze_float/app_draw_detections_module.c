

#include "app_draw_detections_module.h"

vx_status app_init_draw_detections(vx_context context, DrawDetectionsObj *drawDetectionsObj, char *objName, vx_int32 num_cameras)
{
    vx_status status = VX_SUCCESS;
    vx_char ref_name[APP_MAX_FILE_PATH];

    drawDetectionsObj->config = vxCreateUserDataObject(context, "", sizeof(tivxDrawBoxDetectionsParams), NULL);
    status = vxGetStatus((vx_reference)drawDetectionsObj->config);
    if (status == VX_SUCCESS)
    {
        status = vxCopyUserDataObject(drawDetectionsObj->config, 0, sizeof(tivxDrawBoxDetectionsParams),
                                      &drawDetectionsObj->params, VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);

        snprintf(ref_name, APP_MAX_FILE_PATH, "%s_config", objName);
        vxSetReferenceName((vx_reference)drawDetectionsObj->config, ref_name);
    }
    if (status == VX_SUCCESS)
    {
        vx_image output = vxCreateImage(context, drawDetectionsObj->params.width, drawDetectionsObj->params.height, VX_DF_IMAGE_NV12);
        drawDetectionsObj->output_image_arr = vxCreateObjectArray(context, (vx_reference)output, num_cameras);
        vxReleaseImage(&output);
    }

    return status;
}

vx_status app_update_draw_detections(DrawDetectionsObj *drawDetectionsObj, vx_user_data_object config)
{
    vx_status status = VX_SUCCESS;

    vx_map_id map_id_config;
    sTIDL_IOBufDesc_t *ioBufDesc;
    tivxTIDLJ7Params *tidlParams;

    status = vxMapUserDataObject(config, 0, sizeof(tivxTIDLJ7Params), &map_id_config,
                                 (void **)&tidlParams, VX_READ_ONLY, VX_MEMORY_TYPE_HOST, 0);

    if (status == VX_SUCCESS)
    {
        ioBufDesc = (sTIDL_IOBufDesc_t *)&tidlParams->ioBufDesc;
        memcpy(&drawDetectionsObj->params.ioBufDesc, ioBufDesc, sizeof(sTIDL_IOBufDesc_t));

        status = vxUnmapUserDataObject(config, map_id_config);
    }

    return status;
}

void app_deinit_draw_detections(DrawDetectionsObj *drawDetectionsObj)
{
    vxReleaseUserDataObject(&drawDetectionsObj->config);
    vxReleaseObjectArray(&drawDetectionsObj->output_image_arr);
}

void app_delete_draw_detections(DrawDetectionsObj *drawDetectionsObj)
{
    if (drawDetectionsObj->node != NULL)
    {
        vxReleaseNode(&drawDetectionsObj->node);
    }
}

vx_status app_create_graph_draw_detections(vx_graph graph,
                                           DrawDetectionsObj *drawDetectionsObj,
                                           vx_object_array input_tensor_arr,
                                           vx_object_array input_image_arr)
{
    vx_status status = VX_SUCCESS;
    vx_image output_image;
    vx_image input_image;

    vx_tensor input_tensor = (vx_tensor)vxGetObjectArrayItem((vx_object_array)input_tensor_arr, 0);
    input_image = (vx_image)vxGetObjectArrayItem((vx_object_array)input_image_arr, 0);
    output_image = (vx_image)vxGetObjectArrayItem((vx_object_array)drawDetectionsObj->output_image_arr, 0);

    drawDetectionsObj->node = tivxDrawBoxDetectionsNode(graph,
                                                        drawDetectionsObj->config,
                                                        input_tensor,
                                                        input_image,
                                                        output_image);

    APP_ASSERT_VALID_REF(drawDetectionsObj->node);
    status = vxSetNodeTarget(drawDetectionsObj->node, VX_TARGET_STRING, TIVX_TARGET_DSP2);
    if (status == VX_SUCCESS)
    {
        status = vxSetReferenceName((vx_reference)drawDetectionsObj->node, "DrawBoxDetectionsNode");
    }
    // vx_bool replicate[] = {vx_false_e, vx_true_e, vx_true_e, vx_true_e};
    // vxReplicateNode(graph, drawDetectionsObj->node, replicate, 4);
    vxReleaseTensor(&input_tensor);
    vxReleaseImage(&input_image);
    status = vxReleaseImage(&output_image);
    return (status);
}
