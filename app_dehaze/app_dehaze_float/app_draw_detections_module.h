
#ifndef _APP_DRAW_DETECTIONS_MODULE
#define _APP_DRAW_DETECTIONS_MODULE

#include "app_common.h"
#include "itidl_ti.h"

typedef struct
{
    vx_node node; // node节点

    vx_user_data_object config;         //配置信息
    tivxDrawBoxDetectionsParams params; //参数信息

    vx_object_array output_image_arr; //运行节点后输出图像的数组

    vx_int32 graph_parameter_index; // Pipeline运行参数列表的索引值

    vx_char objName[APP_MAX_FILE_PATH]; //参数设置的参考名称

    vx_uint32 out_width;  //??
    vx_uint32 out_height; //??

} DrawDetectionsObj;

/**
 * @brief 更新绘制检测到图像的配置信息
 *
 * @param drawDetectionsObj 绘制检测对象指针
 * @param config 绘制节点的配置信息
 * @return vx_status 更新的状态
 */
vx_status app_update_draw_detections(DrawDetectionsObj *drawDetectionsObj, vx_user_data_object config);

/**
 * @brief 初始化检测绘制图像
 *
 * @param context 节点运行的上下文环境
 * @param drawDetectionsObj 对象指针
 * @param objName 对象参数名称
 * @param num_cameras 摄像头数量
 * @return vx_status 初始化状态
 */
vx_status app_init_draw_detections(vx_context context, DrawDetectionsObj *drawDetectionsObj, char *objName, vx_int32 num_cameras);

/**
 * @brief 反向初始化
 *
 * @param drawDetectionsObj 对象指针
 */
void app_deinit_draw_detections(DrawDetectionsObj *drawDetectionsObj);

/**
 * @brief 删除节点
 *
 * @param drawDetectionsObj 对象指针
 */
void app_delete_draw_detections(DrawDetectionsObj *drawDetectionsObj);

/**
 * @brief 创建检测对象绘制节点
 *
 * @param graph 节点运行的graph
 * @param drawDetectionsObj 对象指针
 * @param input_tensor_arr 输入的矩阵数组
 * @param output_image_arr 输出的矩阵数组
 * @return vx_status 创建状态
 */
vx_status app_create_graph_draw_detections(vx_graph graph,
                                           DrawDetectionsObj *drawDetectionsObj,
                                           vx_object_array input_tensor_arr,
                                           vx_object_array input_image_arr);

#endif
