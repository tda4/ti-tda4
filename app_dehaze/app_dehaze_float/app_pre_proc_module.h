
#ifndef _APP_PRE_PROC_MODULE
#define _APP_PRE_PROC_MODULE

#define APP_PRE_PROC_MAX_TENSORS (8)

#include "app_common.h"
#include "tiadalg_interface.h"
#include "itidl_ti.h"

typedef struct
{
    vx_node node;

    vx_array config;
    tivxImgPreProcParams params;

    sTIDL_IOBufDesc_t ioBufDesc;

    vx_uint32 num_input_tensors;
    vx_uint32 num_output_tensors;

    vx_object_array output_tensor_arr[APP_PRE_PROC_MAX_TENSORS];

    vx_int32 graph_parameter_index;

    vx_char objName[APP_MAX_FILE_PATH];

} PreProcObj;

/**
 * @brief 更新预处理节点的配置参数
 *
 * @param context 预处理节点的运行上下文
 * @param preProcObj 预处理节点的对象指针
 * @param config 配置参数
 * @param num_cameras 摄像头个数
 * @return vx_status 更新状态
 */
vx_status app_update_pre_proc(vx_context context, PreProcObj *preProcObj, vx_user_data_object config, vx_int32 num_cameras);

/**
 * @brief 初始化预处理节点
 *
 * @param context 预处理节点的运行上下文
 * @param preProcObj 预处理节点的对象指针
 * @param objName
 * @return vx_status 初始化预处理节点的状态
 */
vx_status app_init_pre_proc(vx_context context, PreProcObj *preProcObj, char *objName);

/**
 * @brief 反向初始化预处理节点
 *
 * @param obj 预处理节点的对象指针
 */
void app_deinit_pre_proc(PreProcObj *obj);

/**
 * @brief 删除预处理节点
 *
 * @param obj 预处理节点的对象指针
 */
void app_delete_pre_proc(PreProcObj *obj);

/**
 * @brief 创建预处理节点
 *
 * @param graph 预处理几点运行的graph
 * @param preProcObj 预处理节点的对象指针
 * @param input_arr 预处理节点的输入图像数组
 * @return vx_status 创建预处理节点的状态
 */
vx_status app_create_graph_pre_proc(vx_graph graph, PreProcObj *preProcObj, vx_object_array input_arr);

/**
 * @brief 将预处理几点处理的数据，写入到文件内
 *
 * @param file_name 目标文件名称
 * @param preProcObj 预处理节点的对象指针
 * @return vx_status 写入文件的状态
 */
vx_status writePreProcOutput(char *file_name, PreProcObj *preProcObj);

#endif
