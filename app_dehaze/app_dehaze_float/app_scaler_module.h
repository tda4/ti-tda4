
#ifndef _APP_SCALER_MODULE
#define _APP_SCALER_MODULE

//#include "app_modules.h"
#include "app_common.h"

#ifdef DEBUG_WITH_SENSOR

#define APP_MODULES_MAX_SCALER_OUTPUTS (5)
#define APP_MODULES_READ_FILE (0x333)    /* Reads entire file */
#define APP_MODULES_READ_CHANNEL (0x444) /* Reads selected channel */

typedef struct
{
  vx_object_array arr;

  vx_int32 width;
  vx_int32 height;

} ImgObj;

typedef struct
{
  vx_node node;

  ImgObj output[APP_MODULES_MAX_SCALER_OUTPUTS];

  vx_user_data_object coeff_obj;

  vx_int32 graph_parameter_index;

  /* These params are needed only for writing intermediate output */
  vx_int32 en_out_scaler_write;
  vx_array file_path;
  vx_array file_prefix[APP_MODULES_MAX_SCALER_OUTPUTS];
  vx_node write_node[APP_MODULES_MAX_SCALER_OUTPUTS];
  vx_user_data_object write_cmd[APP_MODULES_MAX_SCALER_OUTPUTS];

  vx_char output_file_path[TIVX_FILEIO_FILE_PATH_LENGTH];

  vx_char objName[512];

  vx_int32 num_ch;
  vx_int32 num_outputs;
  vx_int32 color_format;

} ScalerObj;

/**
 * @brief 设置sacle缩放的因子
 *
 * @param coeff 因子
 * @param interpolation 图像差值的模式选择，有线性差值和非线性差值
 */
void scale_set_coeff(tivx_vpac_msc_coefficients_t *coeff, uint32_t interpolation);

/**
 * @brief 初始化scaler相关对象
 *
 * @param context scaler节点运行的上下文
 * @param scalerObj sacler对象指针
 * @param num_ch scaler的通道数
 * @param num_outputs scaler输出的对象个数
 * @return vx_status
 */
vx_status app_init_scaler(vx_context context, ScalerObj *scalerObj, vx_int32 num_ch, vx_int32 num_outputs);

/**
 * @brief 反向初始化scaler节点对象
 *
 * @param obj sacler对象指针
 */
void app_deinit_scaler(ScalerObj *obj);

/**
 * @brief 删除sacler node节点
 *
 * @param obj sacler对象指针
 */
void app_delete_scaler(ScalerObj *obj);

/**
 * @brief 创建scaler节点
 *
 * @param context scaler节点运行的上下文环境
 * @param graph   scaler节点运行的graph
 * @param scalerObj sacler对象指针
 * @param input_img_arr 输入图像的数组
 * @return vx_status 创建scaler节点的状态
 */
vx_status app_create_graph_scaler(vx_context context, vx_graph graph, ScalerObj *scalerObj, vx_object_array input_img_arr);

/**
 * @brief 创建scaler写入文件的节点
 *
 * @param graph scaler节点运行的graph
 * @param scalerObj sacler对象指针
 * @param output_idx 输出图像的索引
 * @return vx_status
 */
vx_status app_create_graph_scaler_write_output(vx_graph graph, ScalerObj *scalerObj, vx_int32 output_idx);

/**
 * @brief
 *
 * @param scalerObj
 * @param start_frame
 * @param num_frames
 * @param num_skip
 * @return vx_status
 */
vx_status app_send_cmd_scaler_write_node(ScalerObj *scalerObj, vx_uint32 start_frame, vx_uint32 num_frames, vx_uint32 num_skip);

/**
 * @brief
 *
 * @param file_name
 * @param img_arr
 * @param read_mode
 * @param ch_num
 * @return vx_status
 */
vx_status readScalerInput(char *file_name, vx_object_array img_arr, vx_int32 read_mode, vx_int32 ch_num);

/**
 * @brief
 *
 * @param file_name
 * @param img_arr
 * @return vx_status
 */
vx_status writeScalerOutput(char *file_name, vx_object_array img_arr);

#endif

#ifdef DEBUG_WITH_USB_CAMERA



/**
 * \defgroup group_vision_apps_modules_scaler Scaler Node Module
 *
 * \brief This section contains module APIs for the TIOVX Scaler node tivxVpacMscScaleNode
 *
 * \ingroup group_vision_apps_modules
 *
 * @{
 */

/** \brief Maximum amount of values allowed from scaler node
 *
 */
#define APP_MODULES_MAX_SCALER_OUTPUTS (5)

/** \brief Read mode for reading entire file
 *
 */
#define APP_MODULES_READ_FILE (0x333)

/** \brief Read mode for reading selected channel
 *
 */
#define APP_MODULES_READ_CHANNEL (0x444)

/** \brief Scaler Image Data Structure
 *
 * Contains image data structure parameters for the scaler module
 *
 */
typedef struct {
    /*! Object array for Scaler Image */
   vx_object_array arr;

    /*! Scaler image width */
   vx_int32 width;

    /*! Scaler image height */
   vx_int32 height;

} ImgObj;

/** \brief Scaler Module Data Structure
 *
 * Contains the data objects required to use tivxVpacMscScaleNode
 *
 */
typedef struct {
    /*! Scaler node object */
   vx_node    node;

    /*! Scaler image output structure array */
   ImgObj output[APP_MODULES_MAX_SCALER_OUTPUTS];

    /*! Scaler node coefficient user data object */
   vx_user_data_object coeff_obj;

    /*! Scaler node graph parameter index */
   vx_int32 graph_parameter_index;

    /*! Flag to enable writing scaler output  */
   vx_int32 en_out_scaler_write;

    /*! File path used to write scaler node output */
   vx_array file_path;

    /*! File path prefix used to write scaler node output */
   vx_array file_prefix[APP_MODULES_MAX_SCALER_OUTPUTS];

    /*! Node used to write scaler output */
   vx_node write_node[APP_MODULES_MAX_SCALER_OUTPUTS];

    /*! User data object containing write cmd parameters */
   vx_user_data_object write_cmd[APP_MODULES_MAX_SCALER_OUTPUTS];

    /*! Output file path for scaler node output */
   vx_char output_file_path[TIVX_FILEIO_FILE_PATH_LENGTH];

    /*! Name of scaler module */
   vx_char objName[APP_MODULES_MAX_OBJ_NAME_SIZE];

    /*! Number of channels used by scaler module */
   vx_int32 num_ch;

    /*! Number of outputs used by scaler module */
   vx_int32 num_outputs;

    /*! Color format used by scaler node; supported values of \ref VX_DF_IMAGE_U8 and \ref VX_DF_IMAGE_NV12 */
   vx_int32 color_format;

} ScalerObj;

/** \brief Scaler module helper function for setting MSC coefficients
 *
 * This Scaler helper function sets the MSC coefficients based on the type of interpolation
 *
 * \param [out] coeff          MSC coefficients set based on interpolation type
 * \param [in]  interpolation  Scaler interpolation type; valid values are \ref VX_INTERPOLATION_BILINEAR and \ref VX_INTERPOLATION_NEAREST_NEIGHBOR
 *
 */
void scale_set_coeff(tivx_vpac_msc_coefficients_t *coeff,  uint32_t interpolation);

/** \brief Scaler module init helper function
 *
 * This Scaler init helper function will create all the data objects required to create the Scaler
 * node
 *
 * \param [in]  context     OpenVX context which must be created using \ref vxCreateContext
 * \param [out] scalerObj   Scaler Module object which gets populated with Scaler node data objects
 * \param [in]  objName     String of the name of this object
 * \param [in]  num_ch      Number of Scaler channels
 * \param [in]  num_outputs Number of Scaler outputs
 *
 */
vx_status app_init_scaler(vx_context context, ScalerObj *scalerObj, char *objName, vx_int32 num_ch, vx_int32 num_outputs);

/** \brief Scaler module deinit helper function
 *
 * This Scaler deinit helper function will release all the data objects created during the \ref app_init_scaler call
 *
 * \param [in,out] obj    Scaler Module object which contains scaler node data objects which are released in this function
 *
 */
void app_deinit_scaler(ScalerObj *obj);

/** \brief Scaler module delete helper function
 *
 * This scaler delete helper function will delete the scaler node and write node that is created during the \ref app_create_graph_scaler call
 *
 * \param [in,out] obj   Scaler Module object which contains scaler node objects which are released in this function
 *
 */
void app_delete_scaler(ScalerObj *obj);

/** \brief Scaler module create helper function
 *
 * This scaler create helper function will create the node using all the data objects created during the \ref app_init_scaler call.
 * Internally calls \ref app_create_graph_scaler_write_output if en_out_scaler_write is set
 *
 * \param [in]     context         OpenVX context which must be created using \ref vxCreateContext
 * \param [in]     graph           OpenVX graph that has been created using \ref vxCreateGraph and where the scaler node is created
 * \param [in,out] scalerObj       Scaler Module object which contains scaler node and write node which are created in this function
 * \param [in]     input_img_arr   Input object array to Scaler node.  Must be created separately using \ref vxCreateObjectArray
 *
 */
vx_status app_create_graph_scaler(vx_context context, vx_graph graph, ScalerObj *scalerObj, vx_object_array input_img_arr);

/** \brief Scaler module write output helper function
 *
 * This scaler create helper function will create the node for writing the scaler output
 *
 * \param [in]     graph         OpenVX graph that has been created using \ref vxCreateGraph and where the scaler node is created
 * \param [in,out] scalerObj     Scaler Module object which contains the write node used in this function
 * \param [in]     output_idx    Output index of scaler images to write
 *
 */
vx_status app_create_graph_scaler_write_output(vx_graph graph, ScalerObj *scalerObj, vx_int32 output_idx);

/** \brief Scaler module write output helper function
 *
 * This scaler create helper function will create the node for writing the scaler output
 *
 * \param [in] scalerObj     Scaler Module object which contains the write node used in this function
 * \param [in] start_frame   Starting frame to write
 * \param [in] num_frames    Total number of frames to write
 * \param [in] num_skip      Number of capture frames to skip writing
 *
 */
vx_status app_send_cmd_scaler_write_node(ScalerObj *scalerObj, vx_uint32 start_frame, vx_uint32 num_frames, vx_uint32 num_skip);

/** \brief Helper function to read image from file
 *
 * This scaler create helper function will read in the image from file into the provided image array
 *
 * \param [in]  file_name  Full path to file to read
 * \param [out] img_arr    Object array containing images which file is read in to
 * \param [in]  read_mode  Read mode; options are \ref APP_MODULES_READ_FILE and \ref APP_MODULES_READ_CHANNEL
 * \param [in]  ch_num     Channel number of array to write
 *
 */
vx_status readScalerInput(char* file_name, vx_object_array img_arr, vx_int32 read_mode, vx_int32 ch_num);

/** \brief Helper function to write image from file
 *
 * This scaler create helper function will write the image from OpenVX object array to the provided file path
 *
 * \param [in]  file_name  Full path to file to write
 * \param [in]  img_arr    Object array containing images which file written from
 *
 */
vx_status writeScalerOutput(char* file_name, vx_object_array img_arr);

/* @} */


#endif


#endif