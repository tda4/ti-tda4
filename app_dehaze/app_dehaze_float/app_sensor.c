

#include "app_sensor.h"
#include "app_config_file.h"

vx_status app_init_sensor(CmsSensorObj *cmsSensorObj)
{
    vx_status status = VX_SUCCESS;

    if (cmsSensorObj->sensorNum > 0)
    {
        APP_PRINTF("[App init info]CMS sensor number = %d !\n", cmsSensorObj->sensorNum);
        status = VX_SUCCESS;
    }
    else
    {
        cmsSensorObj->sensorNum = 0;
        printf("[Error CMS Sensor init]  Wrong sensor number !\n");
        status = VX_FAILURE;
    }
    return status;
}

void app_deinit_sensor(CmsSensorObj *cmsSensorObj)
{
}

void app_delete_sensor(CmsSensorObj *cmsSensorObj)
{
}