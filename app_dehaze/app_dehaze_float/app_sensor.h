

#ifndef _APP_SENSOR_H_
#define _APP_SENSOR_H_

#include "app_common.h"

typedef struct
{

  vx_uint32 sensorNum;          //摄像头数量
  vx_uint32 sensorOutputWidth;  //摄像头输出图像宽度
  vx_uint32 sensorOutputHeight; //摄像头输出图像高度

} CmsSensorObj;

/**
 * @brief 初始化传感器部分
 *
 * @param cmsSensorObj 传感器对象指针
 * @return vx_status 初始化传感器状态
 */
vx_status app_init_sensor(CmsSensorObj *cmsSensorObj);

/**
 * @brief 反向初始化传感器部分
 *
 * @param cmsSensorObj 传感器对象指针
 */
void app_deinit_sensor(CmsSensorObj *cmsSensorObj);

/**
 * @brief 删除传感器相关对象
 *
 * @param cmsSensorObj 传感器对象指针
 */
void app_delete_sensor(CmsSensorObj *cmsSensorObj);

#endif
