
#include "app_socket_udpServer.h"

vx_status app_socket_udpServer_init(SocketUdpObj *udp)
{
    vx_status status = VX_SUCCESS;
    if (udp != NULL)
    {
        udp->socketID = socket(PF_INET, SOCK_DGRAM, 0);
        if (udp->socketID < 0)
        {
            printf("[Error UDP Socket Task] socket create error !\n");
            exit(1);
        }

        memset(&udp->localaddr, 0, sizeof(udp->localaddr));

        udp->localaddr.sin_family = AF_INET;

        udp->localaddr.sin_port = htons(udp->port);

        udp->localaddr.sin_addr.s_addr = htonl(INADDR_ANY);
        if (bind(udp->socketID, (struct sockaddr *)&udp->localaddr, sizeof(udp->localaddr)) < 0)
        {
            printf("[Error ! UDP Socket Task] socket bind error!\n");
            close(udp->socketID);
            exit(1);
        }
        udp->clientaddr_len = sizeof(udp->clientaddr);

        APP_PRINTF("[Task info -> UDP task] port = %d       Receiving data ...\n", udp->port);
    }
    else
    {
        status = VX_FAILURE;
        return status;
    }
    return status;
}

void app_socket_udpServerTask(void *app_var)
{
    vx_status status = VX_SUCCESS;
    SocketUdpObj *udp = (SocketUdpObj *)app_var; //函数指针获取

    app_socket_udpServer_init(udp);

    while (1)
    {
        udp->ret_len = recvfrom(udp->socketID, udp->buffer, 4096, 0, (struct sockaddr *)&udp->clientaddr, &udp->clientaddr_len);
        if (udp->ret_len <= 0)
        {
            printf("[Error ! UDP Socket Task] udp recvfrom error!");
            break;
        }
        else
        {
            udp->all_len += udp->ret_len;
            printf("[Task info -> UDP task] udp port = %d   length = %ld   data = %s\n\n",
                   udp->port,
                   udp->ret_len,
                   udp->buffer);

            if (NULL != udp->udp_can_analysis)
            {
                status = udp->udp_can_analysis(udp->buffer, udp->outBuff);
                
                if (VX_FAILURE == status)
                {
                    printf("[Error ! UDP Socket Task] udp CAN analysis failed!\n\n");
                }
            }
            //等待处理
            memset(udp->buffer, 0, udp->ret_len);
        }

        char tmp_out_res[128];
        memset(tmp_out_res,0,sizeof(tmp_out_res));
        sprintf(tmp_out_res, "hello tda4 \n");
        int send_data_len = sendto(udp->socketID, tmp_out_res, strlen(tmp_out_res), 0, (struct sockaddr *)&udp->clientaddr, udp->clientaddr_len);

        if(send_data_len)
        {
            printf("send_data_len = %d\n", send_data_len);
        }

        if (1 == udp->taskStop)
        {
            break;
        }
        usleep(500); // 500us延迟
    }
    close(udp->socketID);
    udp->taskStopDone = 1;

    app_socket_udpServer_task_delete(udp);
}

vx_status app_socket_udpServer_task_create(SocketUdpObj *udpObj)
{
    tivx_task_create_params_t params;
    vx_status status = VX_SUCCESS;

    if (NULL != udpObj->taskFunciton) //判断函数指针是否为空
    {
        tivxTaskSetDefaultCreateParams(&params);         //初始化任务参数
        params.task_main = udpObj->taskFunciton;         //创建任务函数指针
        params.app_var = udpObj;                         //传递结构体指针，给任务函数
        status = tivxTaskCreate(&udpObj->task, &params); //创建任务
    }
    else
    {
        status = VX_FAILURE;
        return status;
    }
    return status;
}

vx_status app_socket_udpServer_task_delete(SocketUdpObj *udpObj)
{
    vx_status status = VX_SUCCESS;
    if (VX_SUCCESS == status)
    {
        while (!udpObj->taskStopDone) //等待结束标志
        {
            tivxTaskWaitMsecs(100);
        }
        tivxTaskDelete(&udpObj->task);
    }
    return status;
}
