/**
 ******************************************************************************
 * @file    udp_server.h
 * @author
 * @version 1.0.0
 * @date    2022.4.6
 * @brief   dup server
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2022  </center></h2>
 *
 *  (1) Usage: udp_server port
 *
 *  (2)
 *
 *  (3)
 *
 ******************************************************************************
 */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __UDP_SERVER_H__
#define __UDP_SERVER_H__

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

  /** @addtogroup UDP_SERVER
   * @{
   */
  /* Exported types ------------------------------------------------------------*/
  /* Exported constants --------------------------------------------------------*/
  /* Exported macro ------------------------------------------------------------*/
  /* Exported functions --------------------------------------------------------*/

  /**
   * @}
   */

#ifdef __cplusplus
}
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include "app_common.h"

/**
 * @brief UDP对象相关结构定义
 * @param socketID        socket 的文件描述符
 * @param port            提供服务的端口号
 * @param localaddr       本地主机地址
 * @param clientaddr      客户端地址
 * @param clientaddr_len  客户端地址长度
 * @param ret_len         本次接收数据的长度
 * @param all_len         接收数据的总长度
 * @param buffer          接收数据的buff
 * @param task            TI-RTOS的任务指针
 * @param run_funciton        创建的任务实际运行的函数指针
 */
typedef struct
{
  int socketID;                  // socket 的文件描述符
  unsigned int port;             //提供服务的端口号
  struct sockaddr_in localaddr;  //本地主机地址
  struct sockaddr_in clientaddr; //客户端地址
  socklen_t clientaddr_len;      //客户端地址长度

  uint64_t ret_len; //本次接收数据的长度
  uint64_t all_len; //接收数据的总长度

  char buffer[4 * 1024]; //接收数据的缓冲

  tivx_task task;                      //任务
  void (*taskFunciton)(void *app_var); //任务函数函数运行指针
  vx_uint16 taskStop;                  //任务停止标志
  vx_uint16 taskStopDone;              //任务已经停止标志

  //外部通讯接口，注册一个回调函数，用于需要使用的地方对接收到的数据进行分析
  vx_status (*udp_can_analysis)(char *, void *);
  void *outBuff; //传出参数

} SocketUdpObj;

/**
 * @brief UDP服务的初始化函数
 *
 * @param udp
 * @return int
 */
int app_socket_udpServer_init(SocketUdpObj *udp);

/**
 * @brief UDP服务端的任务函数主体
 *
 * @param app_var
 */
void app_socket_udpServerTask(void *app_var);

/**
 * @brief 创建UDP服务函数的系统调用，对接TI-RTOS部分
 *
 * @param udpObj
 * @return vx_status
 */
vx_status app_socket_udpServer_task_create(SocketUdpObj *udpObj);

/**
 * @brief 删除udp任务
 *
 * @param udpObj 对象指针
 * @return vx_status 删除的状态
 */
vx_status app_socket_udpServer_task_delete(SocketUdpObj *udpObj);

#endif /*__UDP_SERVER_H__ */

/************************ (C) COPYRIGHT *****END OF FILE****************/