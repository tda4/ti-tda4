
#include "app_state_machine.h"


vx_status app_state_machine_init(StateMachineObj *obj)
{
    vx_status status = VX_SUCCESS;


    //对外函数接口初始化



    return status;
}


vx_status app_state_machine_deinit(StateMachineObj *obj)
{
    vx_status status = VX_SUCCESS;


    //反向初始化



    return status;
}





void app_state_machine_runTask(void *app_var)
{
    StateMachineObj *obj = (StateMachineObj *)app_var;
    obj->stopTaskDone = 0;

    while (!obj->taskStop)
    {
        /*
        task运行相关 
         */
        //printf("app_state_machine_runTask\n");
        tivxTaskWaitMsecs(100); // 100ms执行一次
    }
    obj->stopTaskDone = 1;  //任务结束成功
}


vx_status app_state_machine_task_create(StateMachineObj *obj)
{
    tivx_task_create_params_t params;
    vx_status status = VX_SUCCESS;

    tivxTaskSetDefaultCreateParams(&params);         //初始化任务参数
    params.task_main = app_state_machine_runTask;         //创建任务函数指针
    params.app_var = obj;                         //传递结构体指针，给任务函数
    status = tivxTaskCreate(&obj->task, &params); //创建任务

    return status;
}




//add code by MaJun





//end

