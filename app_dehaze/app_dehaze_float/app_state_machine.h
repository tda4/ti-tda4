
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STATE_MACHINE_H__
#define __STATE_MACHINE_H__


/*include files*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include "app_common.h"

/*typedef */
/**
 * @brief 用于接收UDP广播的CAN报文数据
 * 
 */
typedef struct
{

}StateMachineInputData;

/**
 * @brief 用于修改经过状态机判断以后对应状态和视野的控制数据
 * 
 */
typedef struct
{

}StateMachineOutputData;



/**
 * @brief 状态机相关数据
 * 
 */
typedef struct 
{
  StateMachineInputData inputData;
  StateMachineOutputData outputData;



  //创建任务相关控制
  tivx_task task;                      //任务
  void (*taskFunciton)(void *app_var); //函数运行指针
  vx_uint16 taskStop;                  //任务停止标志
  vx_uint16 stopTaskDone;              //任务已经停止标志

} StateMachineObj;






//以下函数声明部分
/**
 * @brief 状态机初始化函数
 * 
 * @param obj 
 * @return vx_status 
 */
vx_status app_state_machine_init(StateMachineObj *obj);
/**
 * @brief 状态机反向初始化函数
 * 
 * @param obj 
 * @return vx_status 
 */
vx_status app_state_machine_deinit(StateMachineObj *obj);
/**
 * @brief 状态机运行的独立task 函数
 * 
 * @param app_var 
 */
void app_state_machine_runTask(void *app_var);
/**
 * @brief 创建一个状态机的独立task
 * 
 * @param obj 
 * @return vx_status 
 */
vx_status app_state_machine_task_create(StateMachineObj *obj);

#endif /*__STATE_MACHINE_H__ */
