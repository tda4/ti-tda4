
#ifndef _APP_TIDL_MODULE
#define _APP_TIDL_MODULE



#include "app_common.h"

#ifdef DEBUG_WITH_SENSOR

#include "itidl_ti.h"

#define APP_MODULE_TIDL_MAX_TENSORS (8)

typedef struct
{
  vx_node node;
  vx_kernel kernel;

  vx_user_data_object config;  //配置信息
  vx_user_data_object network; //推理网络
  vx_user_data_object createParams;

  vx_object_array in_args_arr;
  vx_object_array out_args_arr;

  vx_object_array trace_data_arr;

  vx_object_array output_tensor_arr[APP_MODULE_TIDL_MAX_TENSORS]; //输出矩阵的数组

  vx_uint32 num_input_tensors;  //输出矩阵的数量
  vx_uint32 num_output_tensors; //输出矩阵的数量

  vx_int32 graph_parameter_index; // pipeline 运行时的索引

  vx_char config_file_path[APP_MODULES_MAX_FILE_PATH_SIZE];  //配置文件的路径
  vx_char network_file_path[APP_MODULES_MAX_FILE_PATH_SIZE]; //推理网络的路径

  vx_char objName[APP_MODULES_MAX_OBJ_NAME_SIZE]; //参考名称

  vx_uint8 config_checksum[TIVX_TIDL_J7_CHECKSUM_SIZE];  //配置文件的和教研
  vx_uint8 network_checksum[TIVX_TIDL_J7_CHECKSUM_SIZE]; //网络文件的和校验

} TIDLObj;

/**
 * @brief 初始化深度学习模块
 *
 * @param context 深度学习推理运行的上下文环境
 * @param obj 对象指针
 * @param objName 参考的名称
 * @param num_cameras 摄像头的个数
 * @return vx_status
 */
vx_status app_init_tidl(vx_context context, TIDLObj *obj, char *objName, vx_int32 num_cameras);

/**
 * @brief 反向初始化
 *
 * @param obj 对象指针
 */
void app_deinit_tidl(TIDLObj *obj);

/**
 * @brief 删除推理node
 *
 * @param obj 对象的指针
 */
void app_delete_tidl(TIDLObj *obj);

/**
 * @brief 创建推理网络节点
 *
 * @param context 上下文环境
 * @param graph 运行的graph
 * @param tidlObj 对象的指针
 * @param input_tensor_arr 输入的网络数组（图像经过预处理以后的结果）
 * @return vx_status 创建node的状态
 */
vx_status app_create_graph_tidl(vx_context context, vx_graph graph, TIDLObj *tidlObj, vx_object_array input_tensor_arr[]);

/**
 * @brief 将tidl推理输出写到文件
 *
 * @param file_name 目标文件名
 * @param tidlObj 对象指针
 * @return vx_status 写入状态
 */
vx_status writeTIDLOutput(char *file_name, TIDLObj *tidlObj);

#endif

#ifdef DEBUG_WITH_USB_CAMERA


/**
 * \defgroup group_vision_apps_modules_tidl TIDL Node Module
 *
 * \brief This section contains module APIs for the TIOVX TIDL node tivxTIDLNode
 *
 * \ingroup group_vision_apps_modules
 *
 * @{
 */

#include "app_modules.h"
#include "itidl_ti.h"

/** \brief Maximum number of TIDL tensors
 *
 */
#define APP_MODULE_TIDL_MAX_TENSORS (8)

typedef struct {
  /*! TIDL node object */
  vx_node    node;

  /*! TIDL kernel object */
  vx_kernel  kernel;

  /*! TIDL config user data object */
  vx_user_data_object  config;

  /*! TIDL network user data object */
  vx_user_data_object  network;

  /*! TIDL create params user data object */
  vx_user_data_object  createParams;

  /*! TIDL create params user data object */
  vx_object_array  in_args_arr;

  /*! TIDL object array of output args */
  vx_object_array  out_args_arr;

  /*! TIDL object array of trace data */
  vx_object_array trace_data_arr;

  /*! TIDL object array of output tensors */
  vx_object_array  output_tensor_arr[APP_MODULE_TIDL_MAX_TENSORS];

  /*! TIDL number of input tensors */
  vx_uint32 num_input_tensors;

  /*! TIDL number of output tensors */
  vx_uint32 num_output_tensors;

  /*! TIDL graph parameter index */
  vx_int32 graph_parameter_index;

  /*! Config structure file path */
  vx_char config_file_path[APP_MODULES_MAX_FILE_PATH_SIZE];

  /*! Network structure file path */
  vx_char network_file_path[APP_MODULES_MAX_FILE_PATH_SIZE];

  /*! Name of TIDL module */
  vx_char objName[APP_MODULES_MAX_OBJ_NAME_SIZE];

  /*! Config structure checksum */
  vx_uint8 config_checksum[TIVX_TIDL_J7_CHECKSUM_SIZE];

  /*! Network structure checksum */
  vx_uint8 network_checksum[TIVX_TIDL_J7_CHECKSUM_SIZE];

} TIDLObj;

/** \brief TIDL module init helper function
 *
 * This TIDL init helper function will create all the data objects required to create the TIDL
 * node
 *
 * \param [in]  context     OpenVX context which must be created using \ref vxCreateContext
 * \param [out] obj         TIDL Module object which gets populated with TIDL node data objects
 * \param [in]  objName     String of the name of this object
 * \param [in]  num_cameras Number of cameras used by TIDL
 *
 */
vx_status app_init_tidl(vx_context context, TIDLObj *obj, char *objName, vx_int32 num_cameras);

/** \brief TIDL module deinit helper function
 *
 * This TIDL deinit helper function will release all the data objects created during the \ref app_init_tidl call
 *
 * \param [in,out] obj    TIDL Module object which contains TIDL node data objects which are released in this function
 *
 */
void app_deinit_tidl(TIDLObj *obj);

/** \brief TIDL module delete helper function
 *
 * This TIDL delete helper function will delete the TIDL node that is created during the \ref app_create_graph_tidl call
 *
 * \param [in,out] obj   TIDL Module object which contains TIDL node objects which are released in this function
 *
 */
void app_delete_tidl(TIDLObj *obj);

/** \brief TIDL module create helper function
 *
 * This TIDL create helper function will create the node using all the data objects created during the \ref app_init_tidl call.
 *
 * \param [in]     context           OpenVX context which must be created using \ref vxCreateContext
 * \param [in]     graph             OpenVX graph that has been created using \ref vxCreateGraph and where the TIDL node is created
 * \param [in,out] tidlObj           TIDL Module object which contains TIDL node which is created in this function
 * \param [in,out] input_tensor_arr  Input tensors to TIDL node; must be created separately outside the TIDL module
 *
 */
vx_status app_create_graph_tidl(vx_context context, vx_graph graph, TIDLObj *tidlObj, vx_object_array input_tensor_arr[]);

/** \brief TIDL module write TIDL output helper function
 *
 * This TIDL helper function will write each element of the output_tensor_arr to file.
 *
 * \param [in]  file_name  Full path to file to write
 * \param [in]  tidlObj    TIDL Module object which contains the output_tensor_arr which is written to file in this function
 *
 */
vx_status writeTIDLOutput(char *file_name, TIDLObj *tidlObj);

/* @} */


#endif



#endif
