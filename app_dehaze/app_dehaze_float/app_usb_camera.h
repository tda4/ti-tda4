
#ifndef __APP_USB_CAMERA_H__
#define __APP_USB_CAMERA_H__
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include "app_common.h"


#define IMAGEWIDTH 640
#define IMAGEHEIGHT 480

struct buffer
{
    void *start;
    unsigned int length;
};

typedef struct V4LSTRUCT
{
    //tiovx 相关
    vx_context *context;

    char devName[16];
    int fd;

    struct v4l2_capability cap;
    struct v4l2_fmtdesc fmtdesc;
    struct v4l2_format fmt, fmtack;
    struct v4l2_streamparm setfps;
    struct v4l2_requestbuffers req;
    struct v4l2_buffer buf;

    struct buffer* buffers;

    enum v4l2_buf_type type;

    vx_image imageOutYUYV;              //USB摄像头原始输出图像YUV422格式

    tivx_mutex mutex;                   //创建一个任务锁，用于查询USB摄像头是否更新数据
    tivx_task task;                     //任务
    vx_uint32 stopTask;                 //终止任务标志位
    vx_uint32 stopTaskDone;             //任务终止结束标识符
    void (*run_task)(void *app_var);    //函数运行指针

} USBCameraObj;

extern USBCameraObj usbCameraObj;

/*!
 * \brief 初始化V4L2相关设备
 *
 * \param usbCameraObj [in] Pointer to usb camera object
 *
 * \return VX_SUCCESS on success
 *
 * \ingroup group_usb_camera modules
 */
vx_status app_init_v4l2(USBCameraObj *usbCameraObj);
vx_status app_v4l2_grab(USBCameraObj *usbCameraObj);
void app_close_v4l2(USBCameraObj *usbCameraObj);
void app_usb_camera_delete(USBCameraObj *usbCameraObj);
vx_status app_init_usbCamera(vx_context *context, USBCameraObj *usbCameraObj);
vx_status app_running_usbCamera(USBCameraObj *usbCameraObj);

vx_status app_usb_camera_task_create(USBCameraObj *usbCameraObj);
void app_usb_camera_task_runVideo1(void *app_var);
vx_status app_usb_camera_read_to_image(vx_object_array *img_arr  , vx_image *srcImg);
#endif
