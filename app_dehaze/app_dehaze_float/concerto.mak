ifeq ($(TARGET_CPU), $(filter $(TARGET_CPU), x86_64 A72))

include $(PRELUDE)

TARGET      := vx_app_dehaze_float



ifeq ($(TARGET_CPU),x86_64)

TARGETTYPE  := exe

CSOURCES    += main_x86.c

include $(VISION_APPS_PATH)/apps/concerto_x86_64_inc.mak

STATIC_LIBS += $(IMAGING_LIBS)

endif

ifeq ($(TARGET_CPU),A72)
ifeq ($(TARGET_OS), $(filter $(TARGET_OS), LINUX QNX))

TARGETTYPE  := exe

CSOURCES    += main_linux_arm.c
CSOURCES    += app_cms_cam_main.c
CSOURCES    += app_sensor.c 
CSOURCES    += app_display_module.c
CSOURCES    += app_capture_module.c
CSOURCES    += app_color_converte_module.c
CSOURCES    += app_img_mosaic_module.c  
CSOURCES    += app_scaler_module.c
CSOURCES    += app_tidl_module.c
CSOURCES    += app_pre_proc_module.c
CSOURCES    += app_draw_detections_module.c
CSOURCES    += app_config_file.c
CSOURCES    += app_socket_udpServer.c
CSOURCES    += app_can_analysis.c
CSOURCES    += labels.c
CSOURCES    += app_usb_camera.c
CSOURCES    += app_state_machine.c
CSOURCES    += app_dehaze_float_module.c
include $(VISION_APPS_PATH)/apps/concerto_a72_inc.mak

STATIC_LIBS += $(IMAGING_LIBS)
STATIC_LIBS += $(VISION_APPS_KERNELS_LIBS)
STATIC_LIBS += $(TIADALG_LIBS)
STATIC_LIBS += $(IMAGING_LIBS)
STATIC_LIBS += vx_kernels_img_proc
STATIC_LIBS += vx_kernels_fileio


STATIC_LIBS += vx_kernels_dehaze_float

#STATIC_LIBS += vx_kernels_imaging

#STATIC_LIBS += $(IMAGING_LIBS)
#STATIC_LIBS += vx_conformance_engine

endif
endif

ifeq ($(TARGET_CPU),A72)
ifeq ($(TARGET_OS),SYSBIOS)

TARGETTYPE  := library

include $(VISION_APPS_PATH)/apps/concerto_a72_inc.mak

endif
endif

IDIRS += $(IMAGING_IDIRS)

IDIRS += $(VISION_APPS_PATH)/kernels/img_proc/include
IDIRS += $(VISION_APPS_PATH)/kernels/fileio/include
IDIRS += $(VISION_APPS_PATH)/kernels/dehaze_float/include
IDIRS += $(TIOVX_PATH)/conformance_tests
IDIRS += $(TIOVX_PATH)/conformance_tests/kernels/include
IDIRS += $(TIOVX_PATH)/kernels
IDIRS += $(TIOVX_PATH)/conformance_tests/kernels
IDIRS += $(TIOVX_PATH)/kernels_j7
IDIRS += $(TIOVX_PATH)/kernels_j7/hwa/test/


IDIRS += $(VISION_APPS_PATH)/modules/include


STATIC_LIBS += $(TIADALG_LIBS)
STATIC_LIBS += vx_app_modules

include $(FINALE)

endif
