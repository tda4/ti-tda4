

#ifndef __LABELS_H__
#define __LABELS_H__

/**
 * @brief Pelee. 网络训练的物体标签，共20个类别的物体。
 * 
 */
extern const char od_labels[21][16];
extern const char od_coco_labels[81][16];

#endif