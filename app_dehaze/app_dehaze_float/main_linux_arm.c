

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <TI/tivx.h>
#include <app_init.h>
#include "app_cms_cam_main.h"

int32_t appInit()
{
    vx_status status = VX_SUCCESS;

    status = appCommonInit();

    if (status == VX_SUCCESS)
    {
        tivxInit();
        tivxHostInit();
    }
    return status;
}

int32_t appDeInit()
{
    vx_status status = VX_SUCCESS;

    tivxHostDeInit();
    tivxDeInit();
    appCommonDeInit();

    return status;
}

int main(int argc, char *argv[])
{
    vx_status status = VX_SUCCESS;
    status = appInit();

    APP_PRINTF("\n\n*********************************************************************\n");
    APP_PRINTF("*****************************CMS Demo Begin**************************\n");
    APP_PRINTF("*********************************************************************\n\n");

    if (status == VX_SUCCESS)
    {
        app_cms_cam_main(argc, argv);
        appDeInit();
    }
    return 0;
}
