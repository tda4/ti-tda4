# This file lists the places in the generated code where the developer is expected
# to add custom code beyond what the script can generate.  This is generated as 
# part of the KernelExportCode.export() function, but may also be called independently 
# by calling the KernelExportCode.todo() function with the requirement that the 
# VISION_APPS_PATH environment variable is defined. This function simply searches
# for the "< DEVELOPER_TODO ...>" string in all the files from this path, and lists them.
# Removing the "< DEVELOPER_TODO ...>" comment block from the files will effectively remove those
# lines from showing up in this file the next time KernelExportCode.todo() is run.

/home/ubuntu/Saicmotor/documents/gitLocal/ti-processor-sdk-rtos-j721e-evm-08_02_00_05/vision_apps/kernels/dehaze_float/c66/vx_dehaze_get_atmo_float_target.c
    71: Uncomment if kernel context is needed  

    103: Uncomment if kernel context is needed  

    126: Uncomment if kernel context is needed  

    136: Uncomment if kernel context is needed  

    199: Add target kernel processing code here  

    259: Uncomment if kernel context is needed  

    264: (Optional) Add any target kernel create code here (e.g. allocating 
    265: local memory buffers, one time initialization, etc)  

    282: Replace <Add type here with correct data type  

    288: Uncomment if kernel context is needed  

    323: Uncomment if kernel context is needed  

    329: (Optional) Add any target kernel delete code here (e.g. freeing 
    330: local memory buffers, etc)  

    331: Uncomment if kernel context is needed  

    365: (Optional) Add any target kernel control code here (e.g. commands 
    366: the user can call to modify the processing of the kernel at run-time)  

/home/ubuntu/Saicmotor/documents/gitLocal/ti-processor-sdk-rtos-j721e-evm-08_02_00_05/vision_apps/kernels/dehaze_float/c66/vx_dehaze_disptest_float_target.c
    71: Uncomment if kernel context is needed  

    106: Uncomment if kernel context is needed  

    127: Uncomment if kernel context is needed  

    136: Uncomment if kernel context is needed  

    217: Add target kernel processing code here  

    249: Uncomment if kernel context is needed  

    254: (Optional) Add any target kernel create code here (e.g. allocating 
    255: local memory buffers, one time initialization, etc)  

    269: Uncomment if kernel context is needed  

    304: Uncomment if kernel context is needed  

    310: (Optional) Add any target kernel delete code here (e.g. freeing 
    311: local memory buffers, etc)  

    312: Uncomment if kernel context is needed  

    345: (Optional) Add any target kernel control code here (e.g. commands 
    346: the user can call to modify the processing of the kernel at run-time)  

/home/ubuntu/Saicmotor/documents/gitLocal/ti-processor-sdk-rtos-j721e-evm-08_02_00_05/vision_apps/kernels/dehaze_float/c66/vx_dehaze_darkandtrans_float_target.c
    71: Uncomment if kernel context is needed  

    128: Uncomment if kernel context is needed  

    155: Uncomment if kernel context is needed  

    167: Uncomment if kernel context is needed  

    309: Add target kernel processing code here  

    352: Uncomment if kernel context is needed  

    357: (Optional) Add any target kernel create code here (e.g. allocating 
    358: local memory buffers, one time initialization, etc)  

    377: Replace <Add type here with correct data type  

    383: Uncomment if kernel context is needed  

    418: Uncomment if kernel context is needed  

    424: (Optional) Add any target kernel delete code here (e.g. freeing 
    425: local memory buffers, etc)  

    426: Uncomment if kernel context is needed  

    462: (Optional) Add any target kernel control code here (e.g. commands 
    463: the user can call to modify the processing of the kernel at run-time)  

/home/ubuntu/Saicmotor/documents/gitLocal/ti-processor-sdk-rtos-j721e-evm-08_02_00_05/vision_apps/kernels/dehaze_float/c66/vx_dehaze_rgb_channels_extract_float_target.c
    71: Uncomment if kernel context is needed  

    103: Uncomment if kernel context is needed  

    126: Uncomment if kernel context is needed  

    136: Uncomment if kernel context is needed  

    237: Add target kernel processing code here  

    275: Uncomment if kernel context is needed  

    280: (Optional) Add any target kernel create code here (e.g. allocating 
    281: local memory buffers, one time initialization, etc)  

    296: Uncomment if kernel context is needed  

    331: Uncomment if kernel context is needed  

    337: (Optional) Add any target kernel delete code here (e.g. freeing 
    338: local memory buffers, etc)  

    339: Uncomment if kernel context is needed  

    373: (Optional) Add any target kernel control code here (e.g. commands 
    374: the user can call to modify the processing of the kernel at run-time)  

/home/ubuntu/Saicmotor/documents/gitLocal/ti-processor-sdk-rtos-j721e-evm-08_02_00_05/vision_apps/kernels/dehaze_float/c66/vx_dehaze_get_guidefilter_float_target.c
    315: Uncomment if kernel context is needed  

    349: Uncomment if kernel context is needed  

    370: Uncomment if kernel context is needed  

    379: Uncomment if kernel context is needed  

    423: Add target kernel processing code here  

    638: Uncomment if kernel context is needed  

    643: (Optional) Add any target kernel create code here (e.g. allocating 
    644: local memory buffers, one time initialization, etc)  

    660: Replace <Add type here with correct data type  

    666: Uncomment if kernel context is needed  

    701: Uncomment if kernel context is needed  

    707: (Optional) Add any target kernel delete code here (e.g. freeing 
    708: local memory buffers, etc)  

    709: Uncomment if kernel context is needed  

    742: (Optional) Add any target kernel control code here (e.g. commands 
    743: the user can call to modify the processing of the kernel at run-time)  

/home/ubuntu/Saicmotor/documents/gitLocal/ti-processor-sdk-rtos-j721e-evm-08_02_00_05/vision_apps/kernels/dehaze_float/c66/concerto.mak
    12: Add any custom include paths using 'IDIRS' 

    13: Add any custom preprocessor defines or build options needed using
    14: 'CFLAGS'. 

    15: Adjust which cores this library gets built on using 'SKIPBUILD'. 

/home/ubuntu/Saicmotor/documents/gitLocal/ti-processor-sdk-rtos-j721e-evm-08_02_00_05/vision_apps/kernels/dehaze_float/c66/vx_dehaze_channels_combine_float_target.c
    71: Uncomment if kernel context is needed  

    103: Uncomment if kernel context is needed  

    128: Uncomment if kernel context is needed  

    139: Uncomment if kernel context is needed  

    214: Add target kernel processing code here  

    252: Uncomment if kernel context is needed  

    257: (Optional) Add any target kernel create code here (e.g. allocating 
    258: local memory buffers, one time initialization, etc)  

    274: Uncomment if kernel context is needed  

    309: Uncomment if kernel context is needed  

    315: (Optional) Add any target kernel delete code here (e.g. freeing 
    316: local memory buffers, etc)  

    317: Uncomment if kernel context is needed  

    352: (Optional) Add any target kernel control code here (e.g. commands 
    353: the user can call to modify the processing of the kernel at run-time)  

/home/ubuntu/Saicmotor/documents/gitLocal/ti-processor-sdk-rtos-j721e-evm-08_02_00_05/vision_apps/kernels/dehaze_float/host/vx_dehaze_darkandtrans_float_host.c
    178: Replace <Add type here with correct data type  

    191: Replace <Add type here with correct data type  

    209: (Optional) Add any custom parameter type or range checking not 
    210: covered by the code-generation script.)  

    212: (Optional) If intending to use a virtual data object, set metas using appropriate TI API. 
    213: For a code example, please refer to the validate callback of the follow file: 
    214: tiovx/kernels/openvx-core/host/vx_absdiff_host.c. For further information regarding metas, 
    215: please refer to the OpenVX 1.1 spec p. 260, or search for vx_kernel_validate_f.  

    251: (Optional) Set padding values based on valid region if border mode is 
    252: set to VX_BORDER_UNDEFINED and remove the if 0 and endif lines. 
    253: Else, remove this entire if 0 ... endif block  

/home/ubuntu/Saicmotor/documents/gitLocal/ti-processor-sdk-rtos-j721e-evm-08_02_00_05/vision_apps/kernels/dehaze_float/host/vx_dehaze_get_guidefilter_float_host.c
    151: Replace <Add type here with correct data type  

    169: (Optional) Add any custom parameter type or range checking not 
    170: covered by the code-generation script.)  

    172: (Optional) If intending to use a virtual data object, set metas using appropriate TI API. 
    173: For a code example, please refer to the validate callback of the follow file: 
    174: tiovx/kernels/openvx-core/host/vx_absdiff_host.c. For further information regarding metas, 
    175: please refer to the OpenVX 1.1 spec p. 260, or search for vx_kernel_validate_f.  

/home/ubuntu/Saicmotor/documents/gitLocal/ti-processor-sdk-rtos-j721e-evm-08_02_00_05/vision_apps/kernels/dehaze_float/host/vx_dehaze_rgb_channels_extract_float_host.c
    178: (Optional) Add any custom parameter type or range checking not 
    179: covered by the code-generation script.)  

    181: (Optional) If intending to use a virtual data object, set metas using appropriate TI API. 
    182: For a code example, please refer to the validate callback of the follow file: 
    183: tiovx/kernels/openvx-core/host/vx_absdiff_host.c. For further information regarding metas, 
    184: please refer to the OpenVX 1.1 spec p. 260, or search for vx_kernel_validate_f.  

    219: (Optional) Set padding values based on valid region if border mode is 
    220: set to VX_BORDER_UNDEFINED and remove the if 0 and endif lines. 
    221: Else, remove this entire if 0 ... endif block  

/home/ubuntu/Saicmotor/documents/gitLocal/ti-processor-sdk-rtos-j721e-evm-08_02_00_05/vision_apps/kernels/dehaze_float/host/vx_dehaze_disptest_float_host.c
    165: (Optional) Add any custom parameter type or range checking not 
    166: covered by the code-generation script.)  

    168: (Optional) If intending to use a virtual data object, set metas using appropriate TI API. 
    169: For a code example, please refer to the validate callback of the follow file: 
    170: tiovx/kernels/openvx-core/host/vx_absdiff_host.c. For further information regarding metas, 
    171: please refer to the OpenVX 1.1 spec p. 260, or search for vx_kernel_validate_f.  

    203: (Optional) Set padding values based on valid region if border mode is 
    204: set to VX_BORDER_UNDEFINED and remove the if 0 and endif lines. 
    205: Else, remove this entire if 0 ... endif block  

/home/ubuntu/Saicmotor/documents/gitLocal/ti-processor-sdk-rtos-j721e-evm-08_02_00_05/vision_apps/kernels/dehaze_float/host/vx_dehaze_channels_combine_float_host.c
    175: Replace <Add type here with correct data type  

    192: (Optional) Add any custom parameter type or range checking not 
    193: covered by the code-generation script.)  

    195: (Optional) If intending to use a virtual data object, set metas using appropriate TI API. 
    196: For a code example, please refer to the validate callback of the follow file: 
    197: tiovx/kernels/openvx-core/host/vx_absdiff_host.c. For further information regarding metas, 
    198: please refer to the OpenVX 1.1 spec p. 260, or search for vx_kernel_validate_f.  

    234: (Optional) Set padding values based on valid region if border mode is 
    235: set to VX_BORDER_UNDEFINED and remove the if 0 and endif lines. 
    236: Else, remove this entire if 0 ... endif block  

/home/ubuntu/Saicmotor/documents/gitLocal/ti-processor-sdk-rtos-j721e-evm-08_02_00_05/vision_apps/kernels/dehaze_float/host/vx_dehaze_get_atmo_float_host.c
    164: Replace <Add type here with correct data type  

    172: Replace <Add type here with correct data type  

    183: (Optional) Add any custom parameter type or range checking not 
    184: covered by the code-generation script.)  

    186: (Optional) If intending to use a virtual data object, set metas using appropriate TI API. 
    187: For a code example, please refer to the validate callback of the follow file: 
    188: tiovx/kernels/openvx-core/host/vx_absdiff_host.c. For further information regarding metas, 
    189: please refer to the OpenVX 1.1 spec p. 260, or search for vx_kernel_validate_f.  

    223: (Optional) Set padding values based on valid region if border mode is 
    224: set to VX_BORDER_UNDEFINED and remove the if 0 and endif lines. 
    225: Else, remove this entire if 0 ... endif block  

