/*
 *
 * Copyright (c) 2022 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "TI/tivx.h"
#include "TI/tivx_dehaze_float.h"
#include "VX/vx.h"
#include "tivx_dehaze_float_kernels_priv.h"
#include "tivx_kernel_dehaze_disptest_float.h"
#include "TI/tivx_target_kernel.h"
#include "tivx_kernels_target_utils.h"

/* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
#if 0
typedef struct
{
} tivxDehazeDisptestFloatParams;

#endif
static tivx_target_kernel vx_dehaze_disptest_float_target_kernel = NULL;

static vx_status VX_CALLBACK tivxDehazeDisptestFloatProcess(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);
static vx_status VX_CALLBACK tivxDehazeDisptestFloatCreate(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);
static vx_status VX_CALLBACK tivxDehazeDisptestFloatDelete(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);
static vx_status VX_CALLBACK tivxDehazeDisptestFloatControl(
       tivx_target_kernel_instance kernel,
       uint32_t node_cmd_id, tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);

void boxFilter(vx_float32 *src, uint32_t imageWidth, uint32_t imageHeight, uint32_t r, vx_float32 *dst);
//void boxFilterNonPadding(vx_float32 *src, uint32_t imageWidth, uint32_t imageHeight, uint32_t r, vx_float32 *dst);

static vx_status VX_CALLBACK tivxDehazeDisptestFloatProcess(
    tivx_target_kernel_instance kernel,
    tivx_obj_desc_t *obj_desc[],
    uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
    #if 0
    tivxDehazeDisptestFloatParams *prms = NULL;
    #endif
    tivx_obj_desc_matrix_t *matrix_trans_desc;
    tivx_obj_desc_matrix_t *matrix_gray_desc;
    tivx_obj_desc_image_t *trans_desc;
    tivx_obj_desc_image_t *gray_desc;

    if ( (num_params != TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_MAX_PARAMS)
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_MATRIX_TRANS_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_MATRIX_GRAY_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_TRANS_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_GRAY_IDX])
    )
    {
        status = (vx_status)VX_FAILURE;
    }

    if((vx_status)VX_SUCCESS == status)
    {
        /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
        #if 0
        uint32_t size;
        #endif
        matrix_trans_desc = (tivx_obj_desc_matrix_t *)obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_MATRIX_TRANS_IDX];
        matrix_gray_desc = (tivx_obj_desc_matrix_t *)obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_MATRIX_GRAY_IDX];
        trans_desc = (tivx_obj_desc_image_t *)obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_TRANS_IDX];
        gray_desc = (tivx_obj_desc_image_t *)obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_GRAY_IDX];

        /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
        #if 0
        status = tivxGetTargetKernelInstanceContext(kernel,
            (void **)&prms, &size);
        if (((vx_status)VX_SUCCESS != status) || (NULL == prms) ||
            (sizeof(tivxDehazeDisptestFloatParams) != size))
        {
            status = (vx_status)VX_FAILURE;
        }
        #endif
    }

    if((vx_status)VX_SUCCESS == status)
    {

        void *matrix_trans_target_ptr;
        void *matrix_gray_target_ptr;
        void *trans_target_ptr;
        void *gray_target_ptr;

        matrix_trans_target_ptr = tivxMemShared2TargetPtr(&matrix_trans_desc->mem_ptr);
        tivxCheckStatus(&status, tivxMemBufferMap(matrix_trans_target_ptr,
                                                  matrix_trans_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
                                                  (vx_enum)VX_READ_AND_WRITE));

        matrix_gray_target_ptr = tivxMemShared2TargetPtr(&matrix_gray_desc->mem_ptr);
        tivxCheckStatus(&status, tivxMemBufferMap(matrix_gray_target_ptr,
           matrix_gray_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_READ_ONLY));

        trans_target_ptr = tivxMemShared2TargetPtr(&trans_desc->mem_ptr[0]);
        tivxCheckStatus(&status, tivxMemBufferMap(trans_target_ptr,
           trans_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_WRITE_ONLY));

        gray_target_ptr = tivxMemShared2TargetPtr(&gray_desc->mem_ptr[0]);
        tivxCheckStatus(&status, tivxMemBufferMap(gray_target_ptr,
           gray_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_WRITE_ONLY));



        {
            VXLIB_bufParams2D_t vxlib_trans;
            uint8_t *trans_addr = NULL;
            VXLIB_bufParams2D_t vxlib_gray;
            uint8_t *gray_addr = NULL;

            tivxInitBufParams(trans_desc, &vxlib_trans);
            tivxSetPointerLocation(trans_desc, &trans_target_ptr, &trans_addr);

            tivxInitBufParams(gray_desc, &vxlib_gray);
            tivxSetPointerLocation(gray_desc, &gray_target_ptr, &gray_addr);

            /* call kernel processing function */

            vx_float32 *matrixGray = (vx_float32 *)matrix_gray_target_ptr;
            vx_float32 *matrixTrans = (vx_float32 *)matrix_trans_target_ptr;

            uint32_t x, y, w, h;
            uint32_t posy, pos;

            w = matrix_trans_desc->columns;
            h = matrix_trans_desc->rows;

            //boxFilterNonPadding(matrixGray, w, h, 20, matrixTrans);

            for (y = 0; y < h; y++)
            {   //测试程序，将矩阵内的float32数据，转换成Uint8 数据，用于显示图像观察
                posy = y * w;
                for (x = 0; x < w; x++)
                {
                    pos = posy + x;

                    *(gray_addr + pos) = (uint8_t)((*(matrixGray + pos)) * 255);
                    *(trans_addr + pos) = (uint8_t)((*(matrixTrans + pos)));
                }
            }



            /* < DEVELOPER_TODO: Add target kernel processing code here > */
            /* kernel processing function complete */
        }
        tivxCheckStatus(&status, tivxMemBufferUnmap(matrix_trans_target_ptr,
                                                    matrix_trans_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
                                                    (vx_enum)VX_READ_AND_WRITE));

        tivxCheckStatus(&status, tivxMemBufferUnmap(matrix_gray_target_ptr,
           matrix_gray_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_READ_ONLY));

        tivxCheckStatus(&status, tivxMemBufferUnmap(trans_target_ptr,
           trans_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_WRITE_ONLY));

        tivxCheckStatus(&status, tivxMemBufferUnmap(gray_target_ptr,
           gray_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_WRITE_ONLY));



    }

    return status;
}

static vx_status VX_CALLBACK tivxDehazeDisptestFloatCreate(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
    #if 0
    tivxDehazeDisptestFloatParams *prms = NULL;
    #endif

    /* < DEVELOPER_TODO: (Optional) Add any target kernel create code here (e.g. allocating */
    /*                   local memory buffers, one time initialization, etc) > */
    if ( (num_params != TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_MAX_PARAMS)
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_MATRIX_TRANS_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_MATRIX_GRAY_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_TRANS_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_GRAY_IDX])
    )
    {
        status = (vx_status)VX_FAILURE;
    }
    else
    {


        /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
        #if 0
        prms = tivxMemAlloc(sizeof(tivxDehazeDisptestFloatParams), (vx_enum)TIVX_MEM_EXTERNAL);
        if (NULL != prms)
        {

        }
        else
        {
            status = (vx_status)VX_ERROR_NO_MEMORY;
            VX_PRINT(VX_ZONE_ERROR, "Unable to allocate local memory\n");
        }

        if ((vx_status)VX_SUCCESS == status)
        {
            tivxSetTargetKernelInstanceContext(kernel, prms,
                sizeof(tivxDehazeDisptestFloatParams));
        }
        else
        {
            status = (vx_status)VX_ERROR_NO_MEMORY;
            VX_PRINT(VX_ZONE_ERROR, "Unable to allocate local memory\n");
        }
        #endif
    }

    return status;
}

static vx_status VX_CALLBACK tivxDehazeDisptestFloatDelete(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
    #if 0
    tivxDehazeDisptestFloatParams *prms = NULL;
    uint32_t size;
    #endif

    /* < DEVELOPER_TODO: (Optional) Add any target kernel delete code here (e.g. freeing */
    /*                   local memory buffers, etc) > */
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
    #if 0
    if ( (num_params != TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_MAX_PARAMS)
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_MATRIX_TRANS_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_MATRIX_GRAY_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_TRANS_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_GRAY_IDX])
    )
    {
        status = (vx_status)VX_FAILURE;
    }
    else
    {
        tivxGetTargetKernelInstanceContext(kernel, (void **)&prms, &size);

        if ((NULL != prms) &&
            (sizeof(tivxDehazeDisptestFloatParams) == size))
        {
            tivxMemFree(prms, size, (vx_enum)TIVX_MEM_EXTERNAL);
        }
    }
    #endif

    return status;
}

static vx_status VX_CALLBACK tivxDehazeDisptestFloatControl(
       tivx_target_kernel_instance kernel,
       uint32_t node_cmd_id, tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;

    /* < DEVELOPER_TODO: (Optional) Add any target kernel control code here (e.g. commands */
    /*                   the user can call to modify the processing of the kernel at run-time) > */

    return status;
}

void tivxAddTargetKernelDehazeDisptestFloat(void)
{
    vx_status status = (vx_status)VX_FAILURE;
    char target_name[TIVX_TARGET_MAX_NAME];
    vx_enum self_cpu;

    self_cpu = tivxGetSelfCpuId();

    if ( self_cpu == (vx_enum)TIVX_CPU_ID_DSP1 )
    {
        strncpy(target_name, TIVX_TARGET_DSP1, TIVX_TARGET_MAX_NAME);
        status = (vx_status)VX_SUCCESS;
    }
    else
    if ( self_cpu == (vx_enum)TIVX_CPU_ID_DSP2 )
    {
        strncpy(target_name, TIVX_TARGET_DSP2, TIVX_TARGET_MAX_NAME);
        status = (vx_status)VX_SUCCESS;
    }
    else
    {
        status = (vx_status)VX_FAILURE;
    }

    if (status == (vx_status)VX_SUCCESS)
    {
        vx_dehaze_disptest_float_target_kernel = tivxAddTargetKernelByName(
                            TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_NAME,
                            target_name,
                            tivxDehazeDisptestFloatProcess,
                            tivxDehazeDisptestFloatCreate,
                            tivxDehazeDisptestFloatDelete,
                            tivxDehazeDisptestFloatControl,
                            NULL);
    }
}

void tivxRemoveTargetKernelDehazeDisptestFloat(void)
{
    vx_status status = (vx_status)VX_SUCCESS;

    status = tivxRemoveTargetKernel(vx_dehaze_disptest_float_target_kernel);
    if (status == (vx_status)VX_SUCCESS)
    {
        vx_dehaze_disptest_float_target_kernel = NULL;
    }
}

void boxFilter(vx_float32 *src, uint32_t imageWidth, uint32_t imageHeight, uint32_t r, vx_float32 *dst)
{
    vx_float32 *paddingImage = tivxMemAlloc((imageWidth + r) * (imageHeight + r) * sizeof(vx_float32), (vx_enum)TIVX_MEM_EXTERNAL);

    memset(paddingImage, 0, (imageWidth + r) * (imageHeight + r) * sizeof(vx_float32));

    uint32_t paddingWidth;
    uint32_t height;
    uint32_t paddingPos, pos;

    paddingWidth = imageWidth + (2 * r);
   // paddingHight = imageHeight + (2 * r);

    for (height = 0; height < imageHeight; height++)
    {
        paddingPos = (height + r) * paddingWidth + r;
        pos = imageWidth * height;
        memcpy((paddingImage + paddingPos), (src + pos), imageWidth);
    }

    vx_float32 *tempSum = tivxMemAlloc(paddingWidth * sizeof(vx_float32), (vx_enum)TIVX_MEM_EXTERNAL);

    uint32_t w, h;

    for (h = 0; h < imageHeight; h++)
    {
        uint32_t i = 0;
        uint32_t kernelR;
        for (i = 0; i < paddingWidth; i++)
        {
            *(tempSum + i) = 0;
            for (kernelR = 0; kernelR < 2 * r; kernelR++)
            {
                paddingPos = kernelR * paddingWidth + i;
                *(tempSum + i) += *(paddingImage + paddingPos);
            }
        }

        for (w = 0; w < imageWidth; w++)
        {
            uint32_t m;
            vx_float32 tempValue = 0.0;

            for (m = 0; m < 2 * r; m++)
            {
                tempValue += *(tempSum + w + m);
            }
            *(dst + h * imageWidth + w) = tempValue / r / r;
        }
    }

    if (NULL != paddingImage)
    {
        tivxMemFree(paddingImage, paddingWidth * sizeof(vx_float32), (vx_enum)TIVX_MEM_EXTERNAL);
    }

    if (NULL != tempSum)
    {
        tivxMemFree(tempSum, paddingWidth * sizeof(vx_float32), (vx_enum)TIVX_MEM_EXTERNAL);
    }
}

// void boxFilterNonPadding(vx_float32 *src, uint32_t imageWidth, uint32_t imageHeight, uint32_t r, vx_float32 *dst)
// {

//     uint32_t pos;

//     vx_float32 *tempSum = tivxMemAlloc((imageWidth) * sizeof(vx_float32), (vx_enum)TIVX_MEM_EXTERNAL);

//     uint32_t w, h;

//     for (h = r; h < (imageHeight - r); h++)
//     {
//         uint32_t i = 0;
//         uint32_t kernelR;
//         for (i = 0; i < imageWidth; i++)
//         {
//             *(tempSum + i) = 0;
//             for (kernelR = 0; kernelR < 2 * r; kernelR++)
//             {
//                 pos = kernelR * imageWidth + i;
//                 *(tempSum + i) += *(src + pos);
//             }
//         }

//         for (w = r; w < (imageWidth-r); w++)
//         {
//             uint32_t m;
//             vx_float32 tempValue = 0.0;

//             for (m = 0; m < 2 * r; m++)
//             {
//                 tempValue += *(tempSum + w + m);
//             }
//             *(dst + h * imageWidth + w) = tempValue / r / r;
//         }
//     }

//     if (NULL != tempSum)
//     {
//         tivxMemFree(tempSum, imageWidth * sizeof(vx_float32), (vx_enum)TIVX_MEM_EXTERNAL);
//     }
// }
// void boxFilterNonPadding(vx_float32 *src, uint32_t imageWidth, uint32_t imageHeight, uint32_t r, vx_float32 *dst)
// {

//     uint32_t pos;

//     vx_float32 *tempSum = tivxMemAlloc((imageWidth) * sizeof(vx_float32), (vx_enum)TIVX_MEM_EXTERNAL);

//     uint32_t w, h;

//     for (h = r; h < (imageHeight - r); h++)
//     {
//         uint32_t i = 0;
//         uint32_t kernelR;
//         for (i = 0; i < imageWidth; i++)
//         {
//             *(tempSum + i) = 0;
//             for (kernelR = 0; kernelR < 2 * r; kernelR++)
//             {
//                 pos = (kernelR + h - r) * imageWidth + i;
//                 *(tempSum + i) += *(src + pos);

//             }
//         }

//         for (w = r; w < (imageWidth - r); w++)
//         {
//             uint32_t m;
//             vx_float32 tempValue = 0.0;

//             for (m = 0; m < 2 * r; m++)
//             {
//                 tempValue += *(tempSum + w - r + m);
//             }
//             *(dst + h * imageWidth + w) = tempValue / (4*r*r);

//             if (h == r)
//             {
//                 uint32_t j;
//                 for (j = 0; j < 2 * r;j++)
//                 {
//                     pos = j * imageWidth ;
//                 }
//             }
//         }
//     }

//     if (NULL != tempSum)
//     {
//         tivxMemFree(tempSum, imageWidth * sizeof(vx_float32), (vx_enum)TIVX_MEM_EXTERNAL);
//     }
// }

