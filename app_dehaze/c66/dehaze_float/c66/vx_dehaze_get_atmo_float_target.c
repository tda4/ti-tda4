/*
 *
 * Copyright (c) 2022 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "TI/tivx.h"
#include "TI/tivx_dehaze_float.h"
#include "VX/vx.h"
#include "tivx_dehaze_float_kernels_priv.h"
#include "tivx_kernel_dehaze_get_atmo_float.h"
#include "TI/tivx_target_kernel.h"
#include "tivx_kernels_target_utils.h"

/* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
#if 0
typedef struct
{
} tivxDehazeGetAtmoFloatParams;

#endif
static tivx_target_kernel vx_dehaze_get_atmo_float_target_kernel = NULL;

static vx_status VX_CALLBACK tivxDehazeGetAtmoFloatProcess(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);
static vx_status VX_CALLBACK tivxDehazeGetAtmoFloatCreate(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);
static vx_status VX_CALLBACK tivxDehazeGetAtmoFloatDelete(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);
static vx_status VX_CALLBACK tivxDehazeGetAtmoFloatControl(
       tivx_target_kernel_instance kernel,
       uint32_t node_cmd_id, tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);

static vx_status VX_CALLBACK tivxDehazeGetAtmoFloatProcess(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
    #if 1
    tivxDehazeGetAtmoFloatParams *prms = NULL;
    #endif
    tivx_obj_desc_image_t *channelr_desc;
    tivx_obj_desc_image_t *channelg_desc;
    tivx_obj_desc_image_t *channelb_desc;
    tivx_obj_desc_user_data_object_t *configuration_desc;
    tivx_obj_desc_scalar_t *atom_desc;

    if ( (num_params != TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_MAX_PARAMS)
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CHANNELR_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CHANNELG_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CHANNELB_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CONFIGURATION_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_ATOM_IDX])
    )
    {
        status = (vx_status)VX_FAILURE;
    }

    if((vx_status)VX_SUCCESS == status)
    {
        /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
#if 1
        uint32_t size;
        #endif
        channelr_desc = (tivx_obj_desc_image_t *)obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CHANNELR_IDX];
        channelg_desc = (tivx_obj_desc_image_t *)obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CHANNELG_IDX];
        channelb_desc = (tivx_obj_desc_image_t *)obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CHANNELB_IDX];
        configuration_desc = (tivx_obj_desc_user_data_object_t *)obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CONFIGURATION_IDX];
        atom_desc = (tivx_obj_desc_scalar_t *)obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_ATOM_IDX];

        /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
#if 1
        status = tivxGetTargetKernelInstanceContext(kernel,
            (void **)&prms, &size);
        if (((vx_status)VX_SUCCESS != status) || (NULL == prms) ||
            (sizeof(tivxDehazeGetAtmoFloatParams) != size))
        {
            status = (vx_status)VX_FAILURE;
        }
        #endif
    }

    if((vx_status)VX_SUCCESS == status)
    {
        vx_float32 atom_value;

        void *channelr_target_ptr;
        void *channelg_target_ptr;
        void *channelb_target_ptr;
        void *configuration_target_ptr;
        //void *atom_target_ptr;

        channelr_target_ptr = tivxMemShared2TargetPtr(&channelr_desc->mem_ptr[0]);
        tivxCheckStatus(&status, tivxMemBufferMap(channelr_target_ptr,
           channelr_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_READ_ONLY));

        channelg_target_ptr = tivxMemShared2TargetPtr(&channelg_desc->mem_ptr[0]);
        tivxCheckStatus(&status, tivxMemBufferMap(channelg_target_ptr,
           channelg_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_READ_ONLY));

        channelb_target_ptr = tivxMemShared2TargetPtr(&channelb_desc->mem_ptr[0]);
        tivxCheckStatus(&status, tivxMemBufferMap(channelb_target_ptr,
           channelb_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_READ_ONLY));

        configuration_target_ptr = tivxMemShared2TargetPtr(&configuration_desc->mem_ptr);
        tivxCheckStatus(&status, tivxMemBufferMap(configuration_target_ptr,
           configuration_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_READ_ONLY));



        {
            VXLIB_bufParams2D_t vxlib_channelr;
            uint8_t *channelr_addr = NULL;
            VXLIB_bufParams2D_t vxlib_channelg;
            uint8_t *channelg_addr = NULL;
            VXLIB_bufParams2D_t vxlib_channelb;
            uint8_t *channelb_addr = NULL;

            tivxInitBufParams(channelr_desc, &vxlib_channelr);
            tivxSetPointerLocation(channelr_desc, &channelr_target_ptr, &channelr_addr);

            tivxInitBufParams(channelg_desc, &vxlib_channelg);
            tivxSetPointerLocation(channelg_desc, &channelg_target_ptr, &channelg_addr);

            tivxInitBufParams(channelb_desc, &vxlib_channelb);
            tivxSetPointerLocation(channelb_desc, &channelb_target_ptr, &channelb_addr);

            /* call kernel processing function */

            /* < DEVELOPER_TODO: Add target kernel processing code here > */
            uint32_t x, y, w, h;
            uint32_t posy, pos;
            uint32_t tempValue = 0, max = 0;
            vx_rectangle_t rect = channelr_desc->valid_roi;
            w = rect.end_x - rect.start_x;
            h = rect.end_y - rect.start_y;
            VX_PRINT(VX_ZONE_ERROR, "Atom Begin\n");
            for (y = 0; y < h; y++)
            {
                posy = y * w;
                for (x = 0; x < w; x++)
                {
                    pos = posy + x;
                    
                    tempValue = *(channelr_addr + pos);     //第一个数据直接赋值，不清零，减少一步操作
                    tempValue += *(channelg_addr + pos);
                    tempValue += *(channelb_addr + pos);

                    //获取亮度最大值
                    if (tempValue > max)
                    {
                        max = tempValue;
                    }
                }
            }
            atom_value = ((vx_float32)max) / 3.0;

            //VX_PRINT(VX_ZONE_ERROR, "max = %f atom_value = %f\n", max, atom_value);
            VX_PRINT(VX_ZONE_ERROR, "Atom end\n");
            /* kernel processing function complete */
        }
        tivxCheckStatus(&status, tivxMemBufferUnmap(channelr_target_ptr,
           channelr_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_READ_ONLY));

        tivxCheckStatus(&status, tivxMemBufferUnmap(channelg_target_ptr,
           channelg_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_READ_ONLY));

        tivxCheckStatus(&status, tivxMemBufferUnmap(channelb_target_ptr,
           channelb_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_READ_ONLY));

        tivxCheckStatus(&status, tivxMemBufferUnmap(configuration_target_ptr,
           configuration_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_READ_ONLY));


        atom_desc->data.f32 = atom_value;
    }

    return status;
}

static vx_status VX_CALLBACK tivxDehazeGetAtmoFloatCreate(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
#if 1
    tivxDehazeGetAtmoFloatParams *prms = NULL;
    #endif

    /* < DEVELOPER_TODO: (Optional) Add any target kernel create code here (e.g. allocating */
    /*                   local memory buffers, one time initialization, etc) > */
    if ( (num_params != TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_MAX_PARAMS)
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CHANNELR_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CHANNELG_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CHANNELB_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CONFIGURATION_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_ATOM_IDX])
    )
    {
        status = (vx_status)VX_FAILURE;
    }
    else
    {
        tivx_obj_desc_user_data_object_t *configuration_desc;

        configuration_desc = (tivx_obj_desc_user_data_object_t *)obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CONFIGURATION_IDX];

        /* < DEVELOPER_TODO: Replace <Add type here> with correct data type > */
        if (configuration_desc->mem_size != sizeof(tivxDehazeGetAtmoFloatParams))
        {
            VX_PRINT(VX_ZONE_ERROR, "User data object size on target does not match the size on host, possibly due to misalignment in data structure\n");
            status = (vx_status)VX_FAILURE;
        }
        /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
#if 1
        prms = tivxMemAlloc(sizeof(tivxDehazeGetAtmoFloatParams), (vx_enum)TIVX_MEM_EXTERNAL);
        if (NULL != prms)
        {

        }
        else
        {
            status = (vx_status)VX_ERROR_NO_MEMORY;
            VX_PRINT(VX_ZONE_ERROR, "Unable to allocate local memory\n");
        }

        if ((vx_status)VX_SUCCESS == status)
        {
            tivxSetTargetKernelInstanceContext(kernel, prms,
                sizeof(tivxDehazeGetAtmoFloatParams));
        }
        else
        {
            status = (vx_status)VX_ERROR_NO_MEMORY;
            VX_PRINT(VX_ZONE_ERROR, "Unable to allocate local memory\n");
        }
        #endif
    }

    return status;
}

static vx_status VX_CALLBACK tivxDehazeGetAtmoFloatDelete(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
    #if 1
    tivxDehazeGetAtmoFloatParams *prms = NULL;
    uint32_t size;
    #endif

    /* < DEVELOPER_TODO: (Optional) Add any target kernel delete code here (e.g. freeing */
    /*                   local memory buffers, etc) > */
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
#if 1
    if ( (num_params != TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_MAX_PARAMS)
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CHANNELR_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CHANNELG_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CHANNELB_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_CONFIGURATION_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_ATOM_IDX])
    )
    {
        status = (vx_status)VX_FAILURE;
    }
    else
    {
        tivxGetTargetKernelInstanceContext(kernel, (void **)&prms, &size);

        if ((NULL != prms) &&
            (sizeof(tivxDehazeGetAtmoFloatParams) == size))
        {
            tivxMemFree(prms, size, (vx_enum)TIVX_MEM_EXTERNAL);
        }
    }
    #endif

    return status;
}

static vx_status VX_CALLBACK tivxDehazeGetAtmoFloatControl(
       tivx_target_kernel_instance kernel,
       uint32_t node_cmd_id, tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;

    /* < DEVELOPER_TODO: (Optional) Add any target kernel control code here (e.g. commands */
    /*                   the user can call to modify the processing of the kernel at run-time) > */

    return status;
}

void tivxAddTargetKernelDehazeGetAtmoFloat(void)
{
    vx_status status = (vx_status)VX_FAILURE;
    char target_name[TIVX_TARGET_MAX_NAME];
    vx_enum self_cpu;

    self_cpu = tivxGetSelfCpuId();

    if ( self_cpu == (vx_enum)TIVX_CPU_ID_DSP1 )
    {
        strncpy(target_name, TIVX_TARGET_DSP1, TIVX_TARGET_MAX_NAME);
        status = (vx_status)VX_SUCCESS;
    }
    else
    if ( self_cpu == (vx_enum)TIVX_CPU_ID_DSP2 )
    {
        strncpy(target_name, TIVX_TARGET_DSP2, TIVX_TARGET_MAX_NAME);
        status = (vx_status)VX_SUCCESS;
    }
    else
    {
        status = (vx_status)VX_FAILURE;
    }

    if (status == (vx_status)VX_SUCCESS)
    {
        vx_dehaze_get_atmo_float_target_kernel = tivxAddTargetKernelByName(
                            TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_NAME,
                            target_name,
                            tivxDehazeGetAtmoFloatProcess,
                            tivxDehazeGetAtmoFloatCreate,
                            tivxDehazeGetAtmoFloatDelete,
                            tivxDehazeGetAtmoFloatControl,
                            NULL);
    }
}

void tivxRemoveTargetKernelDehazeGetAtmoFloat(void)
{
    vx_status status = (vx_status)VX_SUCCESS;

    status = tivxRemoveTargetKernel(vx_dehaze_get_atmo_float_target_kernel);
    if (status == (vx_status)VX_SUCCESS)
    {
        vx_dehaze_get_atmo_float_target_kernel = NULL;
    }
}


