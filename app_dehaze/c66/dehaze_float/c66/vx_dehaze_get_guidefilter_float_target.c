/*
 *
 * Copyright (c) 2022 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "TI/tivx.h"
#include "TI/tivx_dehaze_float.h"
#include "VX/vx.h"
#include "tivx_dehaze_float_kernels_priv.h"
#include "tivx_kernel_dehaze_get_guidefilter_float.h"
#include "TI/tivx_target_kernel.h"
#include "tivx_kernels_target_utils.h"

#include "assert.h"                      // intrinsics prototypes
#include "ti/vxlib/src/common/c6xsim/C6xSimulator.h" // intrinsics prototypes
#include "ti/vxlib/src/common/c6xsim/C6xSimulator_type_modifiers.h" // define/undefine typing keywords

//#include <math.h>


/* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
#if 0
typedef struct
{
} tivxDehazeGetGuidefilterFloatParams;

#endif
                                                        static tivx_target_kernel vx_dehaze_get_guidefilter_float_target_kernel = NULL;

static vx_status VX_CALLBACK tivxDehazeGetGuidefilterFloatProcess(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);
static vx_status VX_CALLBACK tivxDehazeGetGuidefilterFloatCreate(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);
static vx_status VX_CALLBACK tivxDehazeGetGuidefilterFloatDelete(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);
static vx_status VX_CALLBACK tivxDehazeGetGuidefilterFloatControl(
       tivx_target_kernel_instance kernel,
       uint32_t node_cmd_id, tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);

void boxFilterNonPadding(vx_float32 *src, uint32_t imageWidth, uint32_t imageHeight, uint32_t r, vx_float32 *dst);

static vx_status VX_CALLBACK tivxDehazeGetGuidefilterFloatProcess(
    tivx_target_kernel_instance kernel,
    tivx_obj_desc_t *obj_desc[],
    uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
    #if 0
    tivxDehazeGetGuidefilterFloatParams *prms = NULL;
    #endif
    tivx_obj_desc_matrix_t *matrix_trans_desc;
    tivx_obj_desc_matrix_t *matrix_gray_desc;
    tivx_obj_desc_user_data_object_t *configuration_desc;
    tivx_obj_desc_matrix_t *trans_guide_desc;

    if ( (num_params != TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_MAX_PARAMS)
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_MATRIX_TRANS_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_MATRIX_GRAY_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_CONFIGURATION_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_TRANS_GUIDE_IDX])
    )
    {
        status = (vx_status)VX_FAILURE;
    }

    if((vx_status)VX_SUCCESS == status)
    {
        /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
        #if 0
        uint32_t size;
        #endif
        matrix_trans_desc = (tivx_obj_desc_matrix_t *)obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_MATRIX_TRANS_IDX];
        matrix_gray_desc = (tivx_obj_desc_matrix_t *)obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_MATRIX_GRAY_IDX];
        configuration_desc = (tivx_obj_desc_user_data_object_t *)obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_CONFIGURATION_IDX];
        trans_guide_desc = (tivx_obj_desc_matrix_t *)obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_TRANS_GUIDE_IDX];

        /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
        #if 0
        status = tivxGetTargetKernelInstanceContext(kernel,
            (void **)&prms, &size);
        if (((vx_status)VX_SUCCESS != status) || (NULL == prms) ||
            (sizeof(tivxDehazeGetGuidefilterFloatParams) != size))
        {
            status = (vx_status)VX_FAILURE;
        }
        #endif
    }

    if((vx_status)VX_SUCCESS == status)
    {

        void *matrix_trans_target_ptr;
        void *matrix_gray_target_ptr;
        void *configuration_target_ptr;
        void *trans_guide_target_ptr;

        matrix_trans_target_ptr = tivxMemShared2TargetPtr(&matrix_trans_desc->mem_ptr);
        tivxCheckStatus(&status, tivxMemBufferMap(matrix_trans_target_ptr,
           matrix_trans_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_READ_ONLY));

        matrix_gray_target_ptr = tivxMemShared2TargetPtr(&matrix_gray_desc->mem_ptr);
        tivxCheckStatus(&status, tivxMemBufferMap(matrix_gray_target_ptr,
           matrix_gray_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_READ_ONLY));

        configuration_target_ptr = tivxMemShared2TargetPtr(&configuration_desc->mem_ptr);
        tivxCheckStatus(&status, tivxMemBufferMap(configuration_target_ptr,
           configuration_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_READ_ONLY));

        trans_guide_target_ptr = tivxMemShared2TargetPtr(&trans_guide_desc->mem_ptr);
        tivxCheckStatus(&status, tivxMemBufferMap(trans_guide_target_ptr,
           trans_guide_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_WRITE_ONLY));

        {

            /* call kernel processing function */

            /* < DEVELOPER_TODO: Add target kernel processing code here > */

            vx_float32 *matrixGray = (vx_float32 *)matrix_gray_target_ptr;
            vx_float32 *matrixTrans = (vx_float32 *)matrix_trans_target_ptr;

            uint32_t width, height;
            uint32_t totalMatrixSize;
            uint32_t x, y, pos;
            
            width = matrix_trans_desc->columns;
            height = matrix_trans_desc->rows;
            totalMatrixSize = width * height * sizeof(vx_float32);

            tivxDehazeGetGuidefilterFloatParams *param = (tivxDehazeGetGuidefilterFloatParams *)configuration_target_ptr;

            if (0 == param->r)
            {
                param->r = 20;
            }
            if (0.0 == param->e)
            {
                param->e = 0.0001;
            }
            if (0.0 == param->t0)
            {
                param->t0 = 0.25;
            }
//#1
            void *mean_I, *mean_p;

            mean_I = tivxMemAlloc(totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
            mean_p = tivxMemAlloc(totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);



            //memcpy(mean_I, matrixGray, totalMatrixSize);
            //memcpy(mean_p, matrixTrans, totalMatrixSize);
            // boxFilterNonPadding(matrixGray, width, height, param->r, (vx_float32 *)mean_I);
            // boxFilterNonPadding(matrixTrans, width, height, param->r, (vx_float32 *)mean_p);



            vx_float32 *mean_I_FloatTemp = (vx_float32 *) mean_I;
            vx_float32 *mean_p_FloatTemp = (vx_float32 *)mean_p;

            VX_PRINT(VX_ZONE_ERROR, " mean_I_FloatTemp Begin\n");
            //Test
            for (y = 0; y < height; y++)
            {
                pos = y * width;
                for (x = 0; x < width; x++)
                {
                    *(mean_I_FloatTemp + pos) = *(matrixGray + pos);
                    *(mean_p_FloatTemp + pos) = *(matrixTrans + pos);
                    pos++;
                }
            }
            VX_PRINT(VX_ZONE_ERROR, " mean_I_FloatTemp end\n");
            // VX_PRINT(VX_ZONE_ERROR, "[Guide filter infor]  matrixGray %f  , mean_I80  %f\n", *(matrixGray + 640 * 80 + 80), *(mean_I_FloatTemp + 640 * 80 + 80));
            // VX_PRINT(VX_ZONE_ERROR, "[Guide filter infor]  matrixTrans %f  , mean_p80  %f\n",*(matrixTrans + 640*80+80),*(mean_p_FloatTemp + 640*80+80));



            void *temp_II, *temp_Ip;
            void *corr_I, *corr_Ip;
            temp_II = tivxMemAlloc(totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
            temp_Ip = tivxMemAlloc(totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
            corr_I = tivxMemAlloc(totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
            corr_Ip = tivxMemAlloc(totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);

            vx_float32 *temp_II_Float = (vx_float32 *)temp_II;
            vx_float32 *temp_Ip_Float = (vx_float32 *)temp_Ip;
            VX_PRINT(VX_ZONE_ERROR, " temp_II_Float Begin\n");

            for (y = 0; y < height; y++)
            {
                pos = y * width;
                for (x = 0; x < width; x++)
                {
                    *(temp_II_Float + pos) = (*(matrixGray + pos)) * (*(matrixGray + pos));
                    *(temp_Ip_Float + pos) = (*(matrixGray + pos)) * (*(matrixTrans + pos));
                    pos++;
                }
            }
            VX_PRINT(VX_ZONE_ERROR, " temp_II_Float end\n");

            // __float2_t dsp_matrixGray, dsp_matrixTrans;
            // __float2_t dsp_temp_II_Float, dsp_temp_Ip_Float;
            // VX_PRINT(VX_ZONE_ERROR, " temp_II_Float_Test Begin\n");
            // for (y = 0; y < height; y++)
            // {
            //     pos = y * width;
            //     for (x = 0; x < width; x++)
            //     {
            //        
            //         _amem8_f2(&dsp_matrixGray) = _amem8_f2_const(matrixGray + pos);   //加载8字节数据
            //         _amem8_f2(&dsp_matrixTrans) = _amem8_f2_const(matrixTrans + pos); //加载8字节数据
            //         dsp_temp_II_Float = _dmpysp(dsp_matrixGray, dsp_matrixGray);
            //         dsp_temp_Ip_Float = _dmpysp(dsp_matrixGray, dsp_matrixTrans);
            //         _amem8_f2(temp_II_Float + pos) = _ftof2(_hif2(dsp_temp_II_Float), _lof2(dsp_temp_II_Float));
            //         _amem8_f2(temp_Ip_Float + pos) = _ftof2(_hif2(dsp_temp_Ip_Float), _lof2(dsp_temp_Ip_Float));
            // pos++;
            //     }
            // }
            VX_PRINT(VX_ZONE_ERROR, " temp_II_Float_Test end\n");
            //memcpy(corr_I, temp_II_Float, totalMatrixSize);
            //memcpy(corr_Ip, temp_Ip_Float, totalMatrixSize);
            // boxFilterNonPadding(temp_II_Float, width, height, param->r, (vx_float32 *)corr_I);
            // boxFilterNonPadding(temp_Ip_Float, width, height, param->r, (vx_float32 *)corr_Ip);

            vx_float32 *corr_I_FloatTemp = (vx_float32 *) corr_I;
            vx_float32 *corr_Ip_FloatTemp = (vx_float32 *)corr_Ip;
            VX_PRINT(VX_ZONE_ERROR, " corr_I_FloatTemp Begin\n");
            //Test
            for (y = 0; y < height; y++)
            {
                pos = y * width;
                for (x = 0; x < width; x++)
                {
                    *(corr_I_FloatTemp + pos) = *(temp_II_Float + pos) ;
                    *(corr_Ip_FloatTemp + pos) = *(temp_Ip_Float + pos) ;
                    pos++;
                }
            }
            VX_PRINT(VX_ZONE_ERROR, " corr_I_FloatTemp End\n");

            // VX_PRINT(VX_ZONE_ERROR, "[Guide filter infor]matrixGray %f , matrixTrans  %f\n", *(matrixGray + 640 * 80 + 80), *(matrixTrans + 640 * 80 + 80));
            // VX_PRINT(VX_ZONE_ERROR, "[Guide filter infor]temp_II_Float %f , corr_I80  %f\n", *(temp_II_Float + 640 * 80 + 80), *(corr_I_FloatTemp + 640 * 80 + 80));
            // VX_PRINT(VX_ZONE_ERROR, "[Guide filter infor]temp_Ip_Float %f,  corr_Ip80  %f\n",*(temp_Ip_Float+ 640*80+80),*(corr_Ip_FloatTemp + 640*80+80));




//#2
            void *var_I, *cov_Ip;
            var_I = tivxMemAlloc(totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
            cov_Ip = tivxMemAlloc(totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);

            vx_float32 *mean_I_Float = (vx_float32 *)mean_I;
            vx_float32 *mean_p_Float = (vx_float32 *)mean_p;
            vx_float32 *corr_I_Float = (vx_float32 *)corr_I;
            vx_float32 *corr_Ip_Float = (vx_float32 *)corr_Ip;
            vx_float32 *var_I_Float = (vx_float32 *)var_I;
            vx_float32 *cov_Ip_Float = (vx_float32 *)cov_Ip;

            VX_PRINT(VX_ZONE_ERROR, " var_I_Float Begin\n");
            for (y = 0; y < height; y++)
            {
                pos = y * width;
                for (x = 0; x < width; x++)
                {
                    * (var_I_Float + pos) = *(corr_I_Float + pos) - ((*(mean_I_Float + pos) * (*(mean_I_Float + pos))));
                    *(cov_Ip_Float + pos) = *(corr_Ip_Float + pos) - ((*(mean_I_Float+pos)*(*(mean_p_Float+pos))));
                    pos++;
                }
            }
            VX_PRINT(VX_ZONE_ERROR, " var_I_Float end\n");

            // __float2_t mean1, meanP;
            // __float2_t corrI, corrIp;
            // //, varI, covIP;

            // VX_PRINT(VX_ZONE_ERROR, " test begin\n");
            // //优化测试
            // for (y = 0; y < height; y++)
            // {
            //     pos = y * width;
            //     for (x = 0; x < width; x++)
            //     {
            //         
            //         _amem8_f2(&mean1) = _amem8_f2_const(mean_I_Float + pos); //加载8字节数据
                    
            //         _amem8_f2(&meanP) = _amem8_f2_const(mean_p_Float + pos); //加载8字节数据
            //         _amem8_f2(&corrI) = _amem8_f2_const(corr_I_Float + pos);  //加载8字节数据
            //         _amem8_f2(&corrIp) = _amem8_f2_const(corr_Ip_Float + pos); //加载8字节数据

            //         _amem8_f2(var_I_Float + pos) = _dsubsp(corrI, _dmpysp(mean1, mean1)); //先执行乘法，然后在执行减法
            //         _amem8_f2(cov_Ip_Float + pos) = _dsubsp(corrIp, _dmpysp(mean1, meanP)); //先执行乘法，然后在执行减法

            //         //*(var_I_Float + pos) = varI;
            //         //*(cov_Ip_Float + pos) = covIP;
            //         // _amem8_f2(var_I_Float + pos) = _ftof2(_hif2(varI), _lof2(varI));        //将运算后的数据取出
            //         // _amem8_f2(cov_Ip_Float + pos) = _ftof2(_hif2(covIP), _lof2(covIP));     //将运算后的数据取出
            //          pos++;
            //     }
            // }

            VX_PRINT(VX_ZONE_ERROR, " test end\n");

            //#3
            void *matrix_a, *matrix_b;
            matrix_a = tivxMemAlloc(totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
            matrix_b = tivxMemAlloc(totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);

            vx_float32 *matrix_a_Float = (vx_float32 *)matrix_a;
            vx_float32 *matrix_b_Float = (vx_float32 *)matrix_b;
            VX_PRINT(VX_ZONE_ERROR, " matrix_a_Float Begin\n");
            for (y = 0; y < height; y++)
            {
                pos = y * width;
                for (x = 0; x < width; x++)
                {

                    //    a = cov_Ip / (var_I + e)
                    *(matrix_a_Float + pos) = *(cov_Ip_Float + pos) / (*(var_I_Float + pos) + param->e);
                    //    b = mean_p - a * mean_I
                    *(matrix_b_Float + pos) = *(mean_p_Float + pos) - ((*(matrix_a_Float + pos)) * (*(mean_I_Float + pos)));
                    pos++;
                }
            }
            VX_PRINT(VX_ZONE_ERROR, " matrix_a_Float End\n");

            // __float2_t dsp_cov_Ip_Float, dsp_var_I_Float,dsp_paramE;
            // __float2_t dsp_mean_p_Float, dsp_matrix_a_Float, dsp_mean_I_Float;
            // __float2_t dsp_matrix_a_Float, dsp_matrix_b_Float;

            // _amem8_f2(&dsp_paramE) = _amem8_f2_const(&param->e); //加载8字节数据

            // VX_PRINT(VX_ZONE_ERROR, " matrix_a_Float Begin\n");
            // for (y = 0; y < height; y++)
            // {
            //     pos = y * width;
            //     for (x = 0; x < width; x++)
            //     {

            //         _amem8_f2(&dsp_cov_Ip_Float) = _amem8_f2_const(cov_Ip_Float + pos); //加载8字节数据
            //         _amem8_f2(&dsp_var_I_Float) = _amem8_f2_const(var_I_Float + pos);   //加载8字节数据
            //         _amem8_f2(&dsp_mean_p_Float) = _amem8_f2_const(mean_p_Float + pos); //加载8字节数据
            //         _amem8_f2(&dsp_matrix_a_Float) = _amem8_f2_const(matrix_a_Float + pos);             //加载8字节数据
            //         _amem8_f2(&dsp_mean_I_Float) = _amem8_f2_const(mean_I_Float + pos);                 //加载8字节数据

            //         dsp_matrix_a_Float = _daddsp(dsp_var_I_Float, dsp_paramE);

            //         // varI = _dsubsp(corrI, _dmpysp(mean1, mean1));
            //         // covIP = _dsubsp(corrIp, _dmpysp(mean1, meanP));

            //         // _amem8_f2(var_I_Float + pos) = _ftof2(_hif2(varI), _lof2(varI));
            //         // _amem8_f2(cov_Ip_Float + pos) = _ftof2(_hif2(covIP), _lof2(covIP));

            //         //    a = cov_Ip / (var_I + e)
            //         *(matrix_a_Float + pos) = *(cov_Ip_Float + pos) / (*(var_I_Float + pos) + param->e);
            //         //    b = mean_p - a * mean_I
            //         *(matrix_b_Float + pos) = *(mean_p_Float + pos) - ((*(matrix_a_Float + pos)) * (*(mean_I_Float + pos)));
            //          pos++;
            //          }
            // }
            // VX_PRINT(VX_ZONE_ERROR, " matrix_a_Float End\n");

            // VX_PRINT(VX_ZONE_ERROR, "[Guide filter infor]matrix_a_Float %f \n", *(matrix_a_Float + 640*80+80));
            // VX_PRINT(VX_ZONE_ERROR, "[Guide filter infor]matrix_b_Float %f \n", *(matrix_b_Float + 640*80+80));



//#4
            void *matrix_mean_a,*matrix_mean_b;
            matrix_mean_a = tivxMemAlloc(totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
            matrix_mean_b = tivxMemAlloc(totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);

            //memcpy(matrix_mean_a, matrix_a_Float, totalMatrixSize);
            //memcpy(matrix_mean_b, matrix_b_Float, totalMatrixSize);
            // boxFilterNonPadding(matrix_a_Float, width, height, param->r, (vx_float32 *)matrix_mean_a);
            // boxFilterNonPadding(matrix_b_Float, width, height, param->r, (vx_float32 *)matrix_mean_b);

            vx_float32 *matrix_mean_aFloatTemp = (vx_float32 *)matrix_mean_a;
            vx_float32 *matrix_mean_bFloatTemp = (vx_float32 *)matrix_mean_b;
            VX_PRINT(VX_ZONE_ERROR, " matrix_mean_aFloatTemp Begin\n");
            //Test
            for (y = 0; y < height; y++)
            {
                pos = y * width;
                for (x = 0; x < width; x++)
                {
                    *(matrix_mean_aFloatTemp + pos) = *(matrix_a_Float + pos) ;
                    *(matrix_mean_bFloatTemp + pos) = *(matrix_b_Float + pos) ;
                    pos++;
                }
            }
            VX_PRINT(VX_ZONE_ERROR, " matrix_mean_aFloatTemp end\n");

            // VX_PRINT(VX_ZONE_ERROR, "[Guide filter infor]matrix_mean_a %f \n", *(matrix_mean_aFloatTemp + 640*80+80));
            // VX_PRINT(VX_ZONE_ERROR, "[Guide filter infor]matrix_mean_b %f \n", *(matrix_mean_bFloatTemp + 640*80+80));

//#5
            vx_float32 *matrixGuide = (vx_float32 *)trans_guide_target_ptr;
            vx_float32 *matrix_mean_a_Float = (vx_float32 *)matrix_mean_a;
            vx_float32 *matrix_mean_b_Float = (vx_float32 *)matrix_mean_b;
            VX_PRINT(VX_ZONE_ERROR, " matrixGuide Begin\n");
            for (y = 0; y < height; y++)
            {
                pos = y * width;
                for (x = 0; x < width; x++)
                {
                    *(matrixGuide + pos) = ((*(matrix_mean_a_Float + pos)) * (*(matrixGray + pos))) + *(matrix_mean_b_Float + pos);

                    if (*(matrixGuide + pos) < param->t0)
                    {
                        *(matrixGuide + pos) =param->t0;
                    }
                    pos++;
                }
            }
            VX_PRINT(VX_ZONE_ERROR, " matrixGuide End\n");
            // VX_PRINT(VX_ZONE_ERROR, "[Guide filter infor]matrixGuide %f \n", *(matrixGuide + 640*80+80));

            tivxMemFree(temp_II, totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
            tivxMemFree(temp_Ip, totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);

            tivxMemFree(corr_I, totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
            tivxMemFree(corr_Ip, totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);

            tivxMemFree(var_I, totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
            tivxMemFree(cov_Ip, totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);  

            tivxMemFree(matrix_a, totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
            tivxMemFree(matrix_b, totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);   

            tivxMemFree(matrix_mean_a, totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
            tivxMemFree(matrix_mean_b, totalMatrixSize, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);

             /* kernel processing function complete */
        } tivxCheckStatus(&status, tivxMemBufferUnmap(matrix_trans_target_ptr,
                                                      matrix_trans_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
                                                      (vx_enum)VX_READ_ONLY));

        tivxCheckStatus(&status, tivxMemBufferUnmap(matrix_gray_target_ptr,
           matrix_gray_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_READ_ONLY));

        tivxCheckStatus(&status, tivxMemBufferUnmap(configuration_target_ptr,
           configuration_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_READ_ONLY));

        tivxCheckStatus(&status, tivxMemBufferUnmap(trans_guide_target_ptr,
           trans_guide_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_WRITE_ONLY));
    }

    return status;
}

static vx_status VX_CALLBACK tivxDehazeGetGuidefilterFloatCreate(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
    #if 0
    tivxDehazeGetGuidefilterFloatParams *prms = NULL;
    #endif

    /* < DEVELOPER_TODO: (Optional) Add any target kernel create code here (e.g. allocating */
    /*                   local memory buffers, one time initialization, etc) > */
    if ( (num_params != TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_MAX_PARAMS)
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_MATRIX_TRANS_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_MATRIX_GRAY_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_CONFIGURATION_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_TRANS_GUIDE_IDX])
    )
    {
        status = (vx_status)VX_FAILURE;
    }
    else
    {
        tivx_obj_desc_user_data_object_t *configuration_desc;

        configuration_desc = (tivx_obj_desc_user_data_object_t *)obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_CONFIGURATION_IDX];

        /* < DEVELOPER_TODO: Replace <Add type here> with correct data type > */
        if (configuration_desc->mem_size != sizeof(tivxDehazeGetGuidefilterFloatParams))
        {
            VX_PRINT(VX_ZONE_ERROR, "User data object size on target does not match the size on host, possibly due to misalignment in data structure\n");
            status = (vx_status)VX_FAILURE;
        }
        /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
        #if 0
        prms = tivxMemAlloc(sizeof(tivxDehazeGetGuidefilterFloatParams), (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
        if (NULL != prms)
        {

        }
        else
        {
            status = (vx_status)VX_ERROR_NO_MEMORY;
            VX_PRINT(VX_ZONE_ERROR, "Unable to allocate local memory\n");
        }

        if ((vx_status)VX_SUCCESS == status)
        {
            tivxSetTargetKernelInstanceContext(kernel, prms,
                sizeof(tivxDehazeGetGuidefilterFloatParams));
        }
        else
        {
            status = (vx_status)VX_ERROR_NO_MEMORY;
            VX_PRINT(VX_ZONE_ERROR, "Unable to allocate local memory\n");
        }
        #endif
    }

    return status;
}

static vx_status VX_CALLBACK tivxDehazeGetGuidefilterFloatDelete(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
    #if 0
    tivxDehazeGetGuidefilterFloatParams *prms = NULL;
    uint32_t size;
    #endif

    /* < DEVELOPER_TODO: (Optional) Add any target kernel delete code here (e.g. freeing */
    /*                   local memory buffers, etc) > */
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
    #if 0
    if ( (num_params != TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_MAX_PARAMS)
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_MATRIX_TRANS_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_MATRIX_GRAY_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_CONFIGURATION_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_TRANS_GUIDE_IDX])
    )
    {
        status = (vx_status)VX_FAILURE;
    }
    else
    {
        tivxGetTargetKernelInstanceContext(kernel, (void **)&prms, &size);

        if ((NULL != prms) &&
            (sizeof(tivxDehazeGetGuidefilterFloatParams) == size))
        {
            tivxMemFree(prms, size, (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
        }
    }
    #endif

    return status;
}

static vx_status VX_CALLBACK tivxDehazeGetGuidefilterFloatControl(
       tivx_target_kernel_instance kernel,
       uint32_t node_cmd_id, tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;

    /* < DEVELOPER_TODO: (Optional) Add any target kernel control code here (e.g. commands */
    /*                   the user can call to modify the processing of the kernel at run-time) > */

    return status;
}

void tivxAddTargetKernelDehazeGetGuidefilterFloat(void)
{
    vx_status status = (vx_status)VX_FAILURE;
    char target_name[TIVX_TARGET_MAX_NAME];
    vx_enum self_cpu;

    self_cpu = tivxGetSelfCpuId();

    if ( self_cpu == (vx_enum)TIVX_CPU_ID_DSP1 )
    {
        strncpy(target_name, TIVX_TARGET_DSP1, TIVX_TARGET_MAX_NAME);
        status = (vx_status)VX_SUCCESS;
    }
    else
    if ( self_cpu == (vx_enum)TIVX_CPU_ID_DSP2 )
    {
        strncpy(target_name, TIVX_TARGET_DSP2, TIVX_TARGET_MAX_NAME);
        status = (vx_status)VX_SUCCESS;
    }
    else
    {
        status = (vx_status)VX_FAILURE;
    }

    if (status == (vx_status)VX_SUCCESS)
    {
        vx_dehaze_get_guidefilter_float_target_kernel = tivxAddTargetKernelByName(
                            TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_NAME,
                            target_name,
                            tivxDehazeGetGuidefilterFloatProcess,
                            tivxDehazeGetGuidefilterFloatCreate,
                            tivxDehazeGetGuidefilterFloatDelete,
                            tivxDehazeGetGuidefilterFloatControl,
                            NULL);
    }
}

void tivxRemoveTargetKernelDehazeGetGuidefilterFloat(void)
{
    vx_status status = (vx_status)VX_SUCCESS;

    status = tivxRemoveTargetKernel(vx_dehaze_get_guidefilter_float_target_kernel);
    if (status == (vx_status)VX_SUCCESS)
    {
        vx_dehaze_get_guidefilter_float_target_kernel = NULL;
    }
}

void boxFilterNonPadding(vx_float32 *src, uint32_t imageWidth, uint32_t imageHeight, uint32_t r, vx_float32 *dst)
{

    uint32_t pos;

    vx_float32 *tempSum = tivxMemAlloc((imageWidth) * sizeof(vx_float32), (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);

    uint32_t w, h;

    for (h = r; h < (imageHeight - r); h++)
    {
        uint32_t i = 0;
        uint32_t kernelR;
        for (i = 0; i < imageWidth; i++)
        {
            *(tempSum + i) = 0;
            for (kernelR = 0; kernelR < 2 * r; kernelR++)
            {
                pos = (kernelR + h - r) * imageWidth + i;
                *(tempSum + i) += *(src + pos);
            }
        }

        for (w = r; w < (imageWidth - r); w++)
        {
            uint32_t m;
            vx_float32 tempValue = 0.0;

            for (m = 0; m < 2 * r; m++)
            {
                tempValue += *(tempSum + w - r + m);
            }
            *(dst + h * imageWidth + w) = tempValue / (4*r*r);

            if (h == r)
            {
                uint32_t j;
                for (j = 0; j < 2 * r;j++)
                {
                    pos = j * imageWidth ;
                }
            }
        }
    }

    if (NULL != tempSum)
    {
        tivxMemFree(tempSum, imageWidth * sizeof(vx_float32), (vx_enum)TIVX_MEM_EXTERNAL_SCRATCH);
    }
}








