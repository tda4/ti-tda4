/*
 *
 * Copyright (c) 2022 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "TI/tivx.h"
#include "TI/tivx_dehaze_float.h"
#include "VX/vx.h"
#include "tivx_dehaze_float_kernels_priv.h"
#include "tivx_kernel_dehaze_rgb_channels_extract_float.h"
#include "TI/tivx_target_kernel.h"
#include "tivx_kernels_target_utils.h"
#include <ti/vxlib/vxlib.h>

#include "assert.h"                                                 // intrinsics prototypes
#include "ti/vxlib/src/common/c6xsim/C6xSimulator.h"                // intrinsics prototypes
#include "ti/vxlib/src/common/c6xsim/C6xSimulator_type_modifiers.h" // define/undefine typing keywords

/* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
#if 0
typedef struct
{
} tivxDehazeRgbChannelsExtractFloatParams;

#endif
static tivx_target_kernel vx_dehaze_rgb_channels_extract_float_target_kernel = NULL;

static vx_status VX_CALLBACK tivxDehazeRgbChannelsExtractFloatProcess(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);
static vx_status VX_CALLBACK tivxDehazeRgbChannelsExtractFloatCreate(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);
static vx_status VX_CALLBACK tivxDehazeRgbChannelsExtractFloatDelete(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);
static vx_status VX_CALLBACK tivxDehazeRgbChannelsExtractFloatControl(
       tivx_target_kernel_instance kernel,
       uint32_t node_cmd_id, tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg);

static vx_status VX_CALLBACK tivxDehazeRgbChannelsExtractFloatProcess(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
    #if 0
    tivxDehazeRgbChannelsExtractFloatParams *prms = NULL;
    #endif
    tivx_obj_desc_image_t *in_desc;
    tivx_obj_desc_image_t *channelr_desc;
    tivx_obj_desc_image_t *channelg_desc;
    tivx_obj_desc_image_t *channelb_desc;
    tivx_obj_desc_matrix_t *matrix_gray_desc;

    if ( (num_params != TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_MAX_PARAMS)
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_IN_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_CHANNELR_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_CHANNELG_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_CHANNELB_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_MATRIX_GRAY_IDX])
    )
    {
        status = (vx_status)VX_FAILURE;
    }

    if((vx_status)VX_SUCCESS == status)
    {
        /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
        #if 0
        uint32_t size;
        #endif
        in_desc = (tivx_obj_desc_image_t *)obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_IN_IDX];
        channelr_desc = (tivx_obj_desc_image_t *)obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_CHANNELR_IDX];
        channelg_desc = (tivx_obj_desc_image_t *)obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_CHANNELG_IDX];
        channelb_desc = (tivx_obj_desc_image_t *)obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_CHANNELB_IDX];
        matrix_gray_desc = (tivx_obj_desc_matrix_t *)obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_MATRIX_GRAY_IDX];

        /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
        #if 0
        status = tivxGetTargetKernelInstanceContext(kernel,
            (void **)&prms, &size);
        if (((vx_status)VX_SUCCESS != status) || (NULL == prms) ||
            (sizeof(tivxDehazeRgbChannelsExtractFloatParams) != size))
        {
            status = (vx_status)VX_FAILURE;
        }
        #endif
    }

    if((vx_status)VX_SUCCESS == status)
    {

        void *in_target_ptr;
        void *channelr_target_ptr;
        void *channelg_target_ptr;
        void *channelb_target_ptr;
        void *matrix_gray_target_ptr;

        in_target_ptr = tivxMemShared2TargetPtr(&in_desc->mem_ptr[0]);
        tivxCheckStatus(&status, tivxMemBufferMap(in_target_ptr,
           in_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_READ_ONLY));

        channelr_target_ptr = tivxMemShared2TargetPtr(&channelr_desc->mem_ptr[0]);
        tivxCheckStatus(&status, tivxMemBufferMap(channelr_target_ptr,
           channelr_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_WRITE_ONLY));

        channelg_target_ptr = tivxMemShared2TargetPtr(&channelg_desc->mem_ptr[0]);
        tivxCheckStatus(&status, tivxMemBufferMap(channelg_target_ptr,
           channelg_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_WRITE_ONLY));

        channelb_target_ptr = tivxMemShared2TargetPtr(&channelb_desc->mem_ptr[0]);
        tivxCheckStatus(&status, tivxMemBufferMap(channelb_target_ptr,
           channelb_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_WRITE_ONLY));

        matrix_gray_target_ptr = tivxMemShared2TargetPtr(&matrix_gray_desc->mem_ptr);
        tivxCheckStatus(&status, tivxMemBufferMap(matrix_gray_target_ptr,
           matrix_gray_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
           (vx_enum)VX_WRITE_ONLY));



        {
            VXLIB_bufParams2D_t vxlib_in;
            uint8_t *in_addr = NULL;
            VXLIB_bufParams2D_t vxlib_channelr;
            uint8_t *channelr_addr = NULL;
            VXLIB_bufParams2D_t vxlib_channelg;
            uint8_t *channelg_addr = NULL;
            VXLIB_bufParams2D_t vxlib_channelb;
            uint8_t *channelb_addr = NULL;

            tivxInitBufParams(in_desc, &vxlib_in);
            tivxSetPointerLocation(in_desc, &in_target_ptr, &in_addr);

            tivxInitBufParams(channelr_desc, &vxlib_channelr);
            tivxSetPointerLocation(channelr_desc, &channelr_target_ptr, &channelr_addr);

            tivxInitBufParams(channelg_desc, &vxlib_channelg);
            tivxSetPointerLocation(channelg_desc, &channelg_target_ptr, &channelg_addr);

            tivxInitBufParams(channelb_desc, &vxlib_channelb);
            tivxSetPointerLocation(channelb_desc, &channelb_target_ptr, &channelb_addr);

            /* call kernel processing function */

            VX_PRINT(VX_ZONE_ERROR, "VXLIB_channelExtract_1of3_i8u_o8u Begin\n");
            //通道分离
            status = (vx_status)VXLIB_channelExtract_1of3_i8u_o8u(in_addr, &vxlib_in, channelr_addr, &vxlib_channelr, 0);
            status = (vx_status)VXLIB_channelExtract_1of3_i8u_o8u(in_addr, &vxlib_in, channelg_addr, &vxlib_channelg, 1);
            status = (vx_status)VXLIB_channelExtract_1of3_i8u_o8u(in_addr, &vxlib_in, channelb_addr, &vxlib_channelb, 2);
            VX_PRINT(VX_ZONE_ERROR, "VXLIB_channelExtract_1of3_i8u_o8u end\n");

            
            //获取float灰度图
            vx_float32 *matrixGray = (vx_float32 *)matrix_gray_target_ptr;
            vx_rectangle_t rect = in_desc->valid_roi;
            uint32_t w, h;
            w = rect.end_x - rect.start_x;
            h = rect.end_y - rect.start_y;

            uint32_t x, y, pos;

            vx_float32 coeffR = 0.299;
            vx_float32 coeffG = 0.587;
            vx_float32 coeffB = 0.114;

            vx_float32 coeffDivDao = 0.0;
            __float2_t dsp_coeffR, dsp_coeffG, dsp_coeffB;

            _amem8_f2(&dsp_coeffR) = _amem8_f2_const(&coeffR); //加载8字节数据
            _amem8_f2(&dsp_coeffG) = _amem8_f2_const(&coeffG); //加载8字节数据
            _amem8_f2(&dsp_coeffB) = _amem8_f2_const(&coeffB); //加载8字节数据

            coeffDivDao = 1.0/255.0;                           //获取255的倒数，存入dsp_coeffDiv
            //_amem8_f2(&dsp_coeffDivDao) = _amem8_f2_const(&coeffDivDao); //加载8字节数据

            VX_PRINT(VX_ZONE_ERROR, "Gray Image Begin\n");
            uint32_t temp;

            __float2_t dsp_mutilR, dsp_mutilG, dsp_mutilB, dsp_toatl;

            for (y = 0; y < h; y++)
            {
                pos = y * w;
                for (x = 0; x < w; x++)
                {

                    //对gray图、RGB图做归一化处理，等到4个浮点矩阵
                    //_dinthsp 将uint32_t 转换成 __float2_t（double）

                    temp = *(channelr_addr + pos);  //获取R通道值
                    dsp_mutilR = _dmpysp(dsp_coeffR, _dinthsp(temp));

                    temp = *(channelg_addr + pos);
                    dsp_mutilG = _dmpysp(dsp_coeffG, _dinthsp(temp));

                    temp = *(channelb_addr + pos);
                    dsp_mutilB = _dmpysp(dsp_coeffB, _dinthsp(temp));

                    //将三个通道的数值相加
                    dsp_toatl = _daddsp(dsp_mutilB, _daddsp(dsp_mutilR, dsp_mutilG));
                    
                    //将相加后的值和255的倒数相乘，得到最终的灰度值
                    _amem8_f2(matrixGray + pos) = _dmpysp(dsp_toatl, _amem8_f2_const(&coeffDivDao));

                    pos++;

                    // *(matrixGray + pos) = ((0.299 * (*(channelr_addr + pos)) +
                    //                         0.587 * (*(channelg_addr + pos)) +
                    //                         0.114 * (*(channelb_addr + pos))) /
                    //                        255.0);
                }
            }
            VX_PRINT(VX_ZONE_ERROR, "Gray Image end\n");
            /* < DEVELOPER_TODO: Add target kernel processing code here > */

            /* kernel processing function complete */

        }
        tivxCheckStatus(&status, tivxMemBufferUnmap(in_target_ptr,
           in_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_READ_ONLY));

        tivxCheckStatus(&status, tivxMemBufferUnmap(channelr_target_ptr,
           channelr_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_WRITE_ONLY));

        tivxCheckStatus(&status, tivxMemBufferUnmap(channelg_target_ptr,
           channelg_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_WRITE_ONLY));

        tivxCheckStatus(&status, tivxMemBufferUnmap(channelb_target_ptr,
           channelb_desc->mem_size[0], (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_WRITE_ONLY));

        tivxCheckStatus(&status, tivxMemBufferUnmap(matrix_gray_target_ptr,
           matrix_gray_desc->mem_size, (vx_enum)VX_MEMORY_TYPE_HOST,
            (vx_enum)VX_WRITE_ONLY));



    }

    return status;
}

static vx_status VX_CALLBACK tivxDehazeRgbChannelsExtractFloatCreate(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
    #if 0
    tivxDehazeRgbChannelsExtractFloatParams *prms = NULL;
    #endif

    /* < DEVELOPER_TODO: (Optional) Add any target kernel create code here (e.g. allocating */
    /*                   local memory buffers, one time initialization, etc) > */
    if ( (num_params != TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_MAX_PARAMS)
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_IN_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_CHANNELR_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_CHANNELG_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_CHANNELB_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_MATRIX_GRAY_IDX])
    )
    {
        status = (vx_status)VX_FAILURE;
    }
    else
    {


        /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
        #if 0
        prms = tivxMemAlloc(sizeof(tivxDehazeRgbChannelsExtractFloatParams), (vx_enum)TIVX_MEM_EXTERNAL);
        if (NULL != prms)
        {

        }
        else
        {
            status = (vx_status)VX_ERROR_NO_MEMORY;
            VX_PRINT(VX_ZONE_ERROR, "Unable to allocate local memory\n");
        }

        if ((vx_status)VX_SUCCESS == status)
        {
            tivxSetTargetKernelInstanceContext(kernel, prms,
                sizeof(tivxDehazeRgbChannelsExtractFloatParams));
        }
        else
        {
            status = (vx_status)VX_ERROR_NO_MEMORY;
            VX_PRINT(VX_ZONE_ERROR, "Unable to allocate local memory\n");
        }
        #endif
    }

    return status;
}

static vx_status VX_CALLBACK tivxDehazeRgbChannelsExtractFloatDelete(
       tivx_target_kernel_instance kernel,
       tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
    #if 0
    tivxDehazeRgbChannelsExtractFloatParams *prms = NULL;
    uint32_t size;
    #endif

    /* < DEVELOPER_TODO: (Optional) Add any target kernel delete code here (e.g. freeing */
    /*                   local memory buffers, etc) > */
    /* < DEVELOPER_TODO: Uncomment if kernel context is needed > */
    #if 0
    if ( (num_params != TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_MAX_PARAMS)
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_IN_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_CHANNELR_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_CHANNELG_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_CHANNELB_IDX])
        || (NULL == obj_desc[TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_MATRIX_GRAY_IDX])
    )
    {
        status = (vx_status)VX_FAILURE;
    }
    else
    {
        tivxGetTargetKernelInstanceContext(kernel, (void **)&prms, &size);

        if ((NULL != prms) &&
            (sizeof(tivxDehazeRgbChannelsExtractFloatParams) == size))
        {
            tivxMemFree(prms, size, (vx_enum)TIVX_MEM_EXTERNAL);
        }
    }
    #endif

    return status;
}

static vx_status VX_CALLBACK tivxDehazeRgbChannelsExtractFloatControl(
       tivx_target_kernel_instance kernel,
       uint32_t node_cmd_id, tivx_obj_desc_t *obj_desc[],
       uint16_t num_params, void *priv_arg)
{
    vx_status status = (vx_status)VX_SUCCESS;

    /* < DEVELOPER_TODO: (Optional) Add any target kernel control code here (e.g. commands */
    /*                   the user can call to modify the processing of the kernel at run-time) > */

    return status;
}

void tivxAddTargetKernelDehazeRgbChannelsExtractFloat(void)
{
    vx_status status = (vx_status)VX_FAILURE;
    char target_name[TIVX_TARGET_MAX_NAME];
    vx_enum self_cpu;

    self_cpu = tivxGetSelfCpuId();

    if ( self_cpu == (vx_enum)TIVX_CPU_ID_DSP1 )
    {
        strncpy(target_name, TIVX_TARGET_DSP1, TIVX_TARGET_MAX_NAME);
        status = (vx_status)VX_SUCCESS;
    }
    else
    if ( self_cpu == (vx_enum)TIVX_CPU_ID_DSP2 )
    {
        strncpy(target_name, TIVX_TARGET_DSP2, TIVX_TARGET_MAX_NAME);
        status = (vx_status)VX_SUCCESS;
    }
    else
    {
        status = (vx_status)VX_FAILURE;
    }

    if (status == (vx_status)VX_SUCCESS)
    {
        vx_dehaze_rgb_channels_extract_float_target_kernel = tivxAddTargetKernelByName(
                            TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_NAME,
                            target_name,
                            tivxDehazeRgbChannelsExtractFloatProcess,
                            tivxDehazeRgbChannelsExtractFloatCreate,
                            tivxDehazeRgbChannelsExtractFloatDelete,
                            tivxDehazeRgbChannelsExtractFloatControl,
                            NULL);
    }
}

void tivxRemoveTargetKernelDehazeRgbChannelsExtractFloat(void)
{
    vx_status status = (vx_status)VX_SUCCESS;

    status = tivxRemoveTargetKernel(vx_dehaze_rgb_channels_extract_float_target_kernel);
    if (status == (vx_status)VX_SUCCESS)
    {
        vx_dehaze_rgb_channels_extract_float_target_kernel = NULL;
    }
}


