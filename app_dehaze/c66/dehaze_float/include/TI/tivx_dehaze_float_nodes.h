/*
 *
 * Copyright (c) 2022 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef TIVX_DEHAZE_FLOAT_NODES_H_
#define TIVX_DEHAZE_FLOAT_NODES_H_

#include <VX/vx.h>

#ifdef __cplusplus
extern "C" {
#endif

/*! \brief [Graph] Creates a DEHAZE_GET_ATMO_FLOAT Node.
 * \param [in] graph The reference to the graph.
 * \param [in] channelr
 * \param [in] channelg
 * \param [in] channelb
 * \param [in] configuration
 * \param [out] atom
 * \see <tt>TIVX_KERNEL_DEHAZE_GET_ATMO_FLOAT_NAME</tt>
 * \ingroup group_vision_function_dehaze_get_atmo_float
 * \return <tt>\ref vx_node</tt>.
 * \retval vx_node A node reference. Any possible errors preventing a successful creation should be checked using <tt>\ref vxGetStatus</tt>
 */
VX_API_ENTRY vx_node VX_API_CALL tivxDehazeGetAtmoFloatNode(vx_graph graph,
                                                            vx_image channelr,
                                                            vx_image channelg,
                                                            vx_image channelb,
                                                            vx_user_data_object configuration,
                                                            vx_scalar atom);

/*! \brief [Graph] Creates a DEHAZE_DARKANDTRANS_FLOAT Node.
 * \param [in] graph The reference to the graph.
 * \param [in] channelr
 * \param [in] channelg
 * \param [in] channelb
 * \param [in] atom
 * \param [in] mask
 * \param [in] configuration
 * \param [out] matrix_trans
 * \see <tt>TIVX_KERNEL_DEHAZE_DARKANDTRANS_FLOAT_NAME</tt>
 * \ingroup group_vision_function_dehaze_darkandtrans_float
 * \return <tt>\ref vx_node</tt>.
 * \retval vx_node A node reference. Any possible errors preventing a successful creation should be checked using <tt>\ref vxGetStatus</tt>
 */
VX_API_ENTRY vx_node VX_API_CALL tivxDehazeDarkandtransFloatNode(vx_graph graph,
                                      vx_image             channelr,
                                      vx_image             channelg,
                                      vx_image             channelb,
                                      vx_scalar            atom,
                                      vx_matrix            mask,
                                      vx_user_data_object  configuration,
                                      vx_matrix            matrix_trans);

/*! \brief [Graph] Creates a DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT Node.
 * \param [in] graph The reference to the graph.
 * \param [in] in
 * \param [out] channelr
 * \param [out] channelg
 * \param [out] channelb
 * \param [out] matrix_gray
 * \see <tt>TIVX_KERNEL_DEHAZE_RGB_CHANNELS_EXTRACT_FLOAT_NAME</tt>
 * \ingroup group_vision_function_dehaze_rgb_channels_extract_float
 * \return <tt>\ref vx_node</tt>.
 * \retval vx_node A node reference. Any possible errors preventing a successful creation should be checked using <tt>\ref vxGetStatus</tt>
 */
VX_API_ENTRY vx_node VX_API_CALL tivxDehazeRgbChannelsExtractFloatNode(vx_graph graph,
                                      vx_image             in,
                                      vx_image             channelr,
                                      vx_image             channelg,
                                      vx_image             channelb,
                                      vx_matrix            matrix_gray);

/*! \brief [Graph] Creates a DEHAZE_DISPTEST_FLOAT Node.
 * \param [in] graph The reference to the graph.
 * \param [in] matrix_trans
 * \param [in] matrix_gray
 * \param [out] trans
 * \param [out] gray
 * \see <tt>TIVX_KERNEL_DEHAZE_DISPTEST_FLOAT_NAME</tt>
 * \ingroup group_vision_function_dehaze_disptest_float
 * \return <tt>\ref vx_node</tt>.
 * \retval vx_node A node reference. Any possible errors preventing a successful creation should be checked using <tt>\ref vxGetStatus</tt>
 */
VX_API_ENTRY vx_node VX_API_CALL tivxDehazeDisptestFloatNode(vx_graph graph,
                                      vx_matrix            matrix_trans,
                                      vx_matrix            matrix_gray,
                                      vx_image             trans,
                                      vx_image             gray);

/*! \brief [Graph] Creates a DEHAZE_GET_GUIDEFILTER_FLOAT Node.
 * \param [in] graph The reference to the graph.
 * \param [in] matrix_trans
 * \param [in] matrix_gray
 * \param [in] configuration
 * \param [out] trans_guide
 * \see <tt>TIVX_KERNEL_DEHAZE_GET_GUIDEFILTER_FLOAT_NAME</tt>
 * \ingroup group_vision_function_dehaze_get_guidefilter_float
 * \return <tt>\ref vx_node</tt>.
 * \retval vx_node A node reference. Any possible errors preventing a successful creation should be checked using <tt>\ref vxGetStatus</tt>
 */
VX_API_ENTRY vx_node VX_API_CALL tivxDehazeGetGuidefilterFloatNode(vx_graph graph,
                                      vx_matrix            matrix_trans,
                                      vx_matrix            matrix_gray,
                                      vx_user_data_object  configuration,
                                      vx_matrix            trans_guide);

/*! \brief [Graph] Creates a DEHAZE_CHANNELS_COMBINE_FLOAT Node.
 * \param [in] graph The reference to the graph.
 * \param [in] channelr
 * \param [in] channelg
 * \param [in] channelb
 * \param [in] trans_guide
 * \param [in] atom
 * \param [out] out
 * \see <tt>TIVX_KERNEL_DEHAZE_CHANNELS_COMBINE_FLOAT_NAME</tt>
 * \ingroup group_vision_function_dehaze_channels_combine_float
 * \return <tt>\ref vx_node</tt>.
 * \retval vx_node A node reference. Any possible errors preventing a successful creation should be checked using <tt>\ref vxGetStatus</tt>
 */
VX_API_ENTRY vx_node VX_API_CALL tivxDehazeChannelsCombineFloatNode(vx_graph graph,
                                      vx_image             channelr,
                                      vx_image             channelg,
                                      vx_image             channelb,
                                      vx_matrix            trans_guide,
                                      vx_scalar            atom,
                                      vx_image             out);

#ifdef __cplusplus
}
#endif
typedef struct
{
    vx_float32 per; //最高亮度的前百分比
} tivxDehazeGetAtmoFloatParams;

typedef struct
{
    vx_float32 w; //除雾程度，值越大除雾效果越好
} tivxDehazeDarkandtransFloatParams;



typedef struct
{

    uint32_t r;     //滤波半径
    vx_float32 e;   //正则化参数
    vx_float32 t0;  //透射率最小值设置

} tivxDehazeGetGuidefilterFloatParams;








#endif /* TIVX_DEHAZE_FLOAT_NODES_H_ */


