
#include "app_color_converte_module.h"

vx_status app_create_ColorConvert(vx_graph graph, ColorConvertObj *colorConvertobj, vx_image *inImage, vx_image *outImage)
{
    vx_status status = VX_SUCCESS;

    colorConvertobj->node = vxColorConvertNode(graph, *inImage, *outImage); //创建颜色转换node
    if (vxGetStatus((vx_reference)colorConvertobj->node) == (vx_status)VX_SUCCESS)
    {
        status = vxSetNodeTarget(colorConvertobj->node,     //设置目标target为DSP1
                                 VX_TARGET_STRING,
                                 TIVX_TARGET_DSP1);
        return (status);
    }
    else
    {
        status = VX_FAILURE;        //如果检测颜色转换节点创建失败，将标志置为失败标志
    }
    return status;
}
