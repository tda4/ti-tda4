
#ifndef _APP_COLOR_CONVERTE_MODULE
#define _APP_COLOR_CONVERTE_MODULE

#include "app_common.h"

typedef struct {
    //tiovx 相关
    vx_node node;

} ColorConvertObj;



//函数定义声明

vx_status app_create_ColorConvert(vx_graph graph,
                                  ColorConvertObj *colorConvertobj,
                                  vx_image *inImage,
                                  vx_image *outImage);


#endif
