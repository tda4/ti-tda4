/*
 *
 * Copyright (c) 2017 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "app_encode_module_mutil.h"

vx_status app_init_encode_mutil(vx_context context, EncodeObjMutil *encodeObjMutil)
{
    uint32_t max_bitstream_size;

    vx_status status = VX_SUCCESS;

    /* Create object for encode parameters */
    //tivx_video_encoder_params_init(&(encodeObj->encode_params));
    encodeObjMutil->configuration_obj = vxCreateUserDataObject(context,
                                                               "tivx_video_encoder_params_t",
                                                               sizeof(tivx_video_encoder_params_t),
                                                               NULL);

    if (vxGetStatus((vx_reference)encodeObjMutil->configuration_obj) != VX_SUCCESS)
    {
        APP_PRINTF("configuration_obj create failed\n");
        return VX_FAILURE;
    }

    /* Set bitstream format  */
    encodeObjMutil->encode_params.bitstream_format = TIVX_BITSTREAM_FORMAT_H264;

    encodeObjMutil->encode_params.bitstream_format = TIVX_BITSTREAM_FORMAT_H264;
    encodeObjMutil->encode_params.features = TIVX_ENC_FEATURE_CABAC | TIVX_ENC_FEATURE_8x8;
    encodeObjMutil->encode_params.rcmode = TIVX_ENC_SVBR;
    encodeObjMutil->encode_params.idr_period = 30; //
    encodeObjMutil->encode_params.i_period = 30;   //I帧之间的帧数量 GOP数量
    encodeObjMutil->encode_params.bitrate = 10000000; //码率
    encodeObjMutil->encode_params.framerate = 30;     //每秒编码的YUV图像帧数
    encodeObjMutil->encode_params.crop_left = 0;      //裁剪参数
    encodeObjMutil->encode_params.crop_right = 0;
    encodeObjMutil->encode_params.crop_top = 0;
    
#ifdef ENCODE_DECODE_1080P_ENABLE
    encodeObjMutil->encode_params.crop_bottom = 8;    //1088 -8 =1080
#else
    encodeObjMutil->encode_params.crop_bottom = 0;
#endif
    encodeObjMutil->encode_params.nslices = 1;
    encodeObjMutil->encode_params.base_pipe = 0;
    encodeObjMutil->encode_params.initial_qp_i = 0;
    encodeObjMutil->encode_params.initial_qp_p = 0;
    encodeObjMutil->encode_params.initial_qp_b = 0;
    encodeObjMutil->encode_params.min_qp = 0;
    encodeObjMutil->encode_params.max_qp = 0;
    encodeObjMutil->encode_params.min_blk_size = TIVX_ENC_BLK_SZ_DEFAULT;
    encodeObjMutil->encode_params.intra_pred_modes = 0;

    /* Copy object back onto main graph object ??? */
    vxCopyUserDataObject(encodeObjMutil->configuration_obj, 0,
                         sizeof(tivx_video_encoder_params_t),
                         &(encodeObjMutil->encode_params),
                         VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);

    encodeObjMutil->encode_image = vxCreateImage(context, encodeObjMutil->inWidth, encodeObjMutil->inHeight, VX_DF_IMAGE_NV12);

    max_bitstream_size = ((uint32_t)(encodeObjMutil->inWidth / 16) * (uint32_t)(encodeObjMutil->inHeight / 16) * WORST_QP_SIZE) + ((encodeObjMutil->inHeight >> 4) * CODED_BUFFER_INFO_SECTION_SIZE);

    int i = 0;

    for (i = 0; i < ENCODE_MUTIL_NUM; i++) //创建多路编码比特流对象
    {

        encodeObjMutil->bitstream_obj[i] = vxCreateUserDataObject(context,
                                                                  "tivx_video_bitstream_t",
                                                                  sizeof(uint8_t) * max_bitstream_size,
                                                                  NULL);
        status = vxGetStatus((vx_reference)encodeObjMutil->bitstream_obj[i]);
    }



    if(status == VX_SUCCESS)
    {
        for (i = 0; i < ENCODE_MUTIL_NUM ; i++)
        {
                /* Open file for output */
            char *filename = "encode_output.h264";
            sprintf(encodeObjMutil->output_file_path, "/home/root/");       //文件保存在/home/root下
            snprintf(encodeObjMutil->output_file, 256, "%s/%s_%d", encodeObjMutil->output_file_path, filename, i);

            encodeObjMutil->out_fp[i] = fopen(encodeObjMutil->output_file, "wb");
            if (NULL == encodeObjMutil->out_fp[i])
            {
                printf("app_create_graph: encoder: %s: Output file not opened!!!\n", encodeObjMutil->output_file);
                printf("app_create_graph: encoder: Bitstream write to media skipped!\n");
            }            
        }

    }
    return status;
}

void app_deinit_encode_mutil(EncodeObjMutil *encodeObjMutil)
{
    if (NULL != encodeObjMutil)
    {

        if (NULL != encodeObjMutil->configuration_obj)
        {
            vxReleaseUserDataObject(&encodeObjMutil->configuration_obj);
            APP_PRINTF("releasing encode configuration object done\n");
            encodeObjMutil->configuration_obj = NULL;
        }

        int i = 0;
        for (i = 0; i < ENCODE_MUTIL_NUM; i++)
        {
            vxReleaseNode(&encodeObjMutil->node[i]);
            APP_PRINTF("releasing encodeNode done\n");
            encodeObjMutil->node[i] = NULL;

            if (NULL != encodeObjMutil->bitstream_obj[i])
            {
                vxReleaseUserDataObject(&encodeObjMutil->bitstream_obj[i]);
                APP_PRINTF("releasing encode bitstream object done\n");
                encodeObjMutil->bitstream_obj[i] = NULL;
            }
            if (NULL != encodeObjMutil->out_fp[i])
            {
                fclose(encodeObjMutil->out_fp[i]);
                encodeObjMutil->out_fp[i] = NULL;
            }
        }
    }
    return;
}

void app_delete_encode_mutil(EncodeObjMutil *encodeObjMutil)
{
    int i = 0;
    for (i = 0; i < ENCODE_MUTIL_NUM;i++)
    {
        if (encodeObjMutil->node[i] != NULL)
        {
            vxReleaseNode(&encodeObjMutil->node[i]);
        }        
    }

    if (encodeObjMutil->kernel != NULL)
    {
        vxRemoveKernel(encodeObjMutil->kernel);
    }
    return;
}

vx_status app_create_graph_encode_mutil(vx_graph graph, EncodeObjMutil *encodeObjMutil, vx_image *inputImage)
{

    vx_status status = VX_SUCCESS;

    int i = 0;
    char referenceName[128] = {};
    for (i = 0; i < ENCODE_MUTIL_NUM; i++)
    {
        encodeObjMutil->node[i] = tivxVideoEncoderNode(graph,
                                                    encodeObjMutil->configuration_obj, /* Encode parameters          (0) */
                                                    inputImage[0],                     /* From Mosaic                (1) */
                                                    encodeObjMutil->bitstream_obj[i]);    /* Bitstream to write to file (2) */
        APP_ASSERT_VALID_REF(encodeObjMutil->node[i]);

        vxSetNodeTarget(encodeObjMutil->node[i], VX_TARGET_STRING, TIVX_TARGET_VENC1);

        memset(referenceName,0,128);
        sprintf(referenceName, "Encode_node_%d",i);
        vxSetReferenceName((vx_reference)encodeObjMutil->node[i], referenceName);
        status = vxGetStatus((vx_reference)encodeObjMutil->node[i]);        
    }



    return (status);

}

vx_status writeEncodeOutput_mutil(EncodeObjMutil *encodeObjMutil)
{
    vx_status status;
    vx_map_id map_id;
    vx_size   bitstream_size;

    size_t num_read;
    uint8_t *bitstream;

    int i = 0;
    for (i = 0; i < ENCODE_MUTIL_NUM; i++)
    {
        status = vxQueryUserDataObject(encodeObjMutil->bitstream_obj[i],
                                        TIVX_USER_DATA_OBJECT_VALID_SIZE,
                                        &(bitstream_size), sizeof(vx_size));
        APP_ASSERT(status==VX_SUCCESS);

        status = vxMapUserDataObject(encodeObjMutil->bitstream_obj[i], 0,
                                    bitstream_size,
                                    &map_id, (void *)&bitstream,
                                    VX_READ_ONLY, VX_MEMORY_TYPE_HOST, 0);
        APP_ASSERT(status==VX_SUCCESS);

        if (NULL != encodeObjMutil->out_fp[i])
        {
            num_read = fwrite(bitstream, sizeof(uint8_t), bitstream_size, encodeObjMutil->out_fp[i]);
            if (bitstream_size != num_read)
            {
                APP_PRINTF("app_create_graph: encoder: %s: Wrote less than expected (%d < %d)!!!\n", encodeObjMutil->output_file, (uint32_t)num_read, (uint32_t)(bitstream_size));
            }
        }

        vxUnmapUserDataObject(encodeObjMutil->bitstream_obj[i], map_id);       
    }



    return(status);
}



