    
function [] = gen_lut(spec_file, pitch_in_mm,f_in_mm, W, H, hc, vc,s ,m)
    f = f_in_mm/pitch_in_mm ; 
    [h_p , v_p] = meshgrid( 0:W, 0:H);
    [h_d,v_d] = xyz2distorted(h_p,v_p, f/s, hc, vc,spec_file, pitch_in_mm);
    h_delta = round((h_d-h_p) * 8);
    v_delta = round((v_d-v_p) * 8);
    mh = h_delta(1:2^m:end, 1:2^m:end)';
    mv = v_delta(1:2^m:end, 1:2^m:end)';
    dlmwrite('mesh.txt', [mh(:), mv(:)],  'delimiter', ' ')

function [h_d, v_d] = xyz2distorted(x, y, z, hc, vc, spec_file, pitch_in_mm)
[phi, r] = cart2pol(x-hc, y-vc);
theta = atan2(r, z);
lut = read_spec(spec_file, pitch_in_mm);
r = interp1(lut(:,1), lut(:,2), theta);
[h_d, v_d] = pol2cart(phi, r);
h_d = h_d + hc;
v_d = v_d + vc;


function lut = read_spec(spec_file, pitch_in_mm)
lut0 = dlmread(spec_file);
theta = lut0(:,1)/180*pi;
lut = [theta, lut0(:,2)/pitch_in_mm];



