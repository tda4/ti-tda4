
/*************************使用方法及说明*************************/

2022年07月12日

1、运行 Matlab 将spec_file.txt 畸变表文件，转换成 mesh.txt  网格图文件

2、需要下载PROCESSOR_SDK_VISION视觉工具包，具体看这个博客：
    https://blog.csdn.net/AIRKernel/article/details/125676412?spm=1001.2014.3001.5502

3、将mesh.txt 文件转换成 LUT 畸变矫正表，在APP中使用。

4、可以使用DCC tuning tools 实时看到LUT畸变表的矫正效果。然后在软件上微调，以达到最佳效果。

DCC tuning tools 需要找当地的TI 技术支持获取！！！！！
DCC tuning tools 需要找当地的TI 技术支持获取！！！！！
DCC tuning tools 需要找当地的TI 技术支持获取！！！！！