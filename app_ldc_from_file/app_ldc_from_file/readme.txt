
/*************************使用方法及说明*************************/
SDK版本：0802
2022年07月12日

使用方法：
1、将 文件夹 app_ldc_from_file 文件拷贝到 ./vision_apps/apps/basic_demos 路径下

2、执行APP整体编译：sudo make vision_apps -j16 |grep error

3、将文件夹下的 图像 ldc_1280_720_uyvy.yuv 拷贝到 /home/root/ 目录下备用。

4、ldc_lut_1280x720.h 头文件，是通过 mesh.txt 经过PDK 视觉转换工具转换得到的，具体请参考LDC相关博客。
    https://blog.csdn.net/AIRKernel/article/details/125676412?spm=1001.2014.3001.5502

5、将编译生成的  vx_app_ldc_fileio.out 文件拷贝到 SD卡 /opt/vision_apps/目录下

6、将当前目录下的 ./config/app_ldc_from_file.cfg 文件拷贝到 SD卡 /opt/vision_apps/目录下

7、登录开发板，进入 /opt/vision_apps 执行 source ./vision_apps_init.sh

8、运行演示软件  ./vx_app_ldc_fileio.out --cfg app_ldc_from_file.cfg


希望对大家有所帮助！
Good Luck ！


