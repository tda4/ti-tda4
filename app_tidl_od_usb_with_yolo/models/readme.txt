
在ti-processor-sdk-rtos-j721e-evm-08_02_00_05/tidl_j721e_08_02_00_11/ti_dl/utils/tidlModelImport 路径下，执行：
./out/tidl_model_import.out ../../test/testvecs/config/import/public/onnx/tidl_import_yolox_m_ti_lite_45p5_64p2.txt

可以生生对应文件夹下的转换完成的相关文件。如：
        ./yolox/yolox_m_ti_lite_45p5_64p2
文件夹中的网络文件和配置文件，可以导入TDA4 EVM 进行目标检测和推理

网络配置文件：tidl_io_yolox_m_ti_lite_45p5_64p2_1.bin 
网络权重文件：tidl_net_yolox_m_ti_lite_45p5_64p2.bin